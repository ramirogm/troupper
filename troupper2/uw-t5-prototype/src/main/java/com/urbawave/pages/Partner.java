package com.urbawave.pages;

import org.apache.log4j.Logger;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

import com.urbawave.annotations.AnonymousAccess;

@AnonymousAccess
public class Partner {

	@Component
	private Form partnerForm;

	@InjectPage
	private Result resultado;

	@Inject
	private MailSender mailSender;

	@Inject
	private SimpleMailMessage mailMessage;

	@Property
	private String name;
	@Property
	private String email;
	@Property
	private String partnerName;
	@Property
	private String phone;
	@Property
	private String address;
	@Property
	private String details;
	@Property
	private String captcha;

	@Property
	private String error;

	private Logger logger = Logger.getLogger(Partner.class);

	public Object onSuccess() {

		try {
			String[] tos = { "betianad@gmail.com" };
			this.mailMessage.setFrom("Troupper <noreply@troupper.com>");
			this.mailMessage
					.setSubject("Partner contact request from troupper");

			String mailText = "\nName: " + ( this.name == null ? "<No ingresado>" : this.name );
			mailText += "\nEmail: " + ( this.email == null ? "<No ingresado>" : this.email ) ;
			mailText += "\nPartner: " + ( this.partnerName == null ? "<No ingresado>" : this.partnerName );
			mailText += "\nPhone: " + ( this.phone == null ? "<No ingresado>" : this.phone );
			mailText += "\nAddress: " + ( this.address == null ? "<No ingresado>" : this.address );
			mailText += "\nDetails: " + ( this.details == null ? "<No ingresado>" : this.details );

			logger.debug("Enviando mensaje con texto " + mailText);
			this.mailMessage.setText(mailText);
			this.mailMessage.setTo(tos);
			this.mailSender.send(this.mailMessage);
			this.resultado.setMensaje("Message sent successfully");
		} catch (Exception e) {
			e.printStackTrace();
			this.error = e.getMessage();
			logger.error("Couldn't send the email: ", e);
			this.resultado.setMensaje(error);
		}
		return this.resultado;
	}

}
