//
//  Surface.h
//  KClark
//
//  Created by Betiana Darderes on 7/21/11.
//

#import <Foundation/Foundation.h>


@interface Surface : NSObject {
	
	NSString *name;
	NSString *imageName;
	NSMutableDictionary *techniqueDictionary;


	}

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *imageName;
@property (nonatomic, retain) NSMutableDictionary *techniqueDictionary;

- (NSArray *)techniquesForPlace:(NSString*)aPlace;
- (void)setupTechniqueDictionary;



@end
