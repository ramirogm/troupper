package com.urbawave.persistence.dao;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.urbawave.domain.Profile;

public class HibernateDAOTest extends BaseDaoTestCase {
	
	Logger logger = Logger.getLogger(HibernateDAOTest.class);

	@Autowired
	HibernateDAO hibernateDAO;

	public void testPersistProfile() {
		Profile entity = new Profile();
		entity.setName("profileTest");
		entity.setEmail("entity@entity.com");
		hibernateDAO.persist(entity);
//		Profile entity2 = hibernateDAO.load(Profile.class, 0);
//		logger.info(entity2.getName());
		
	}
}
