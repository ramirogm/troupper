
package com.urbawave.ws;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.activation.MimetypesFileTypeMap;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonSyntaxException;


/**
 * 
 * @author ramiro
 *
 */
@Path("/services")
@Component
@Scope("request")
public class DiamanteService {
    
	static Logger logger = Logger.getLogger(DiamanteService.class);
	
    @Autowired
    private MailSender mailSender;

    @Autowired
    private SimpleMailMessage mailMessage;

    @POST 
    @Path("/sendmessage/form/")
    @Consumes("application/x-www-form-urlencoded")
    public String sendMessageFORM(@FormParam("nombre") String nombre,
    		@FormParam("email") String email, @FormParam("mensaje") String mensaje, @FormParam("subject") String subject) {
		try {
			MessageSender ms = new MessageSender();
			ms.setMailSender(mailSender);
			ms.setMailMessage(mailMessage);
			try {
				ms.sendMessage(nombre, email, mensaje, subject);
			} catch (RuntimeException e) {
				throw new WebApplicationException(e, 500);
			}
		} catch (NumberFormatException e) {
			throw new WebApplicationException(e, 500);
		}
    	
    	return "OK";
    }

	private Gson createGSON() {
		GsonBuilder builder = createGsonBuilder();
        Gson gson = builder.create();
		return gson;
	}

	private GsonBuilder createGsonBuilder() {
		GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {

            @Override
            public Date deserialize(JsonElement json, java.lang.reflect.Type typeOfT, JsonDeserializationContext context)
                    throws JsonParseException {

                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                String date = json.getAsJsonPrimitive().getAsString();
                try {
                    return format.parse(date);
                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }
            }
        });
		return builder;
	}
    
    
}
