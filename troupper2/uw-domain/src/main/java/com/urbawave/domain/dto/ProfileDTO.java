package com.urbawave.domain.dto;

public class ProfileDTO {

	public Integer id;

	public String name;

	public String imagePath;

	public byte[] image;

	public String interestsAsString;

	public Boolean active;

	public Boolean isPublic;

	public String bio;

	public String homepage;

	public String email;

	public String otherInterests;
	
	public Double lastSeenLat;
	
	public Double lastSeenLong;



	public boolean isValidForEdit() {
		if ( id == null || id == 0 ) return false;
		return true;
	}

}
