//
//  UISignupController.h
//  troupper.0
//
//  Created by Betiana Darderes on 6/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class UISignupTableViewController;

@interface UISignupController : UIViewController {
	
	UISignupTableViewController *signupTableController;

}
@property (nonatomic, retain) IBOutlet UISignupTableViewController *signupTableController;
- (IBAction)join:(id)sender;
- (IBAction)cancel:(id)sender;

@end
