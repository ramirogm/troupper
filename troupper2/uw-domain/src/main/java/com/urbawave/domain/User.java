/**
 * 
 */
package com.urbawave.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

/**
 * @author ftroya
 * 
 */
@Entity
public class User {

    @Id
    @GeneratedValue
    private Integer id;
    
    private String userName;
    
    private String fullName;
    
    private String password;
    
    private String email;
    
    boolean letOthersFindMeByEmailAddress;
    
    boolean sendMeUpdates;

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    List<Profile> profiles = new ArrayList<Profile>();

    public Profile getActiveProfile() {
	for (Profile iter : this.profiles) {
	    if (iter.getActive()) {
		return iter;
	    }
	}

	return null;
    }

    /**
     * @return the email
     */
    public String getEmail() {
	return this.email;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
	return this.fullName;
    }

    /**
     * @return the id
     */
    public Integer getId() {
	return this.id;
    }

    /**
     * @return the password
     */
    public String getPassword() {
	return this.password;
    }

    /**
     * @return the profiles
     */
    public List<Profile> getProfiles() {
	return this.profiles;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
	return this.userName;
    }

    /**
     * @return the letOthersFindMeByEmailAddress
     */
    public boolean isLetOthersFindMeByEmailAddress() {
	return this.letOthersFindMeByEmailAddress;
    }

    /**
     * @return the sendMeUpdates
     */
    public boolean isSendMeUpdates() {
	return this.sendMeUpdates;
    }

    /**
     * @param email
     *            the email to set
     */
    public void setEmail(String email) {
	this.email = email;
    }

    /**
     * @param fullName
     *            the fullName to set
     */
    public void setFullName(String fullName) {
	this.fullName = fullName;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Integer id) {
	this.id = id;
    }

    /**
     * @param letOthersFindMeByEmailAddress
     *            the letOthersFindMeByEmailAddress to set
     */
    public void setLetOthersFindMeByEmailAddress(boolean letOthersFindMeByEmailAddress) {
	this.letOthersFindMeByEmailAddress = letOthersFindMeByEmailAddress;
    }

    /**
     * @param password
     *            the password to set
     */
    public void setPassword(String password) {
	this.password = password;
    }

    /**
     * @param profiles
     *            the profiles to set
     */
    public void setProfiles(List<Profile> profiles) {
	this.profiles = profiles;
    }

    /**
     * @param sendMeUpdates
     *            the sendMeUpdates to set
     */
    public void setSendMeUpdates(boolean sendMeUpdates) {
	this.sendMeUpdates = sendMeUpdates;
    }

    /**
     * @param userName
     *            the userName to set
     */
    public void setUserName(String userName) {
	this.userName = userName;
    }

}
