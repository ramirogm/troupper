//
//  PlaceViewController.h
//  KClark
//
//  Created by Betiana Darderes on 7/21/11.
//

#import <UIKit/UIKit.h>
#import "DataSourceProtocol.h"
#import "ShadowedTableView.h"
#import "TrouperService.h"


@interface PlaceViewController : UIViewController <UITableViewDelegate, UITableViewDataSource,ServiceDelegate> {

IBOutlet UINavigationController * navController;
ShadowedTableView *theTableView;
id<DataSourceProtocol,UITableViewDataSource> dataSource;
IBOutlet UIActivityIndicatorView * activityIndicator;
	
	NSMutableArray* selectedUsers;
		
}
	
	@property (nonatomic,retain) ShadowedTableView *theTableView;
	@property (nonatomic,retain) id<DataSourceProtocol,UITableViewDataSource> dataSource;
    @property (nonatomic, retain)  UINavigationController * navController;
    @property (nonatomic, retain) UIActivityIndicatorView * activityIndicator;

    @property (nonatomic, retain) NSMutableArray* selectedUsers;
	





@end
