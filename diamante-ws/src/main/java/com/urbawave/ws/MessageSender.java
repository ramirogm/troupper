package com.urbawave.ws;


import org.apache.log4j.Logger;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

public class MessageSender {

    
    private MailSender mailSender;

    private SimpleMailMessage mailMessage;

    private Logger logger = Logger.getLogger(MessageSender.class);

    public MessageSender() {
    	
    }
    
	public MailSender getMailSender() {
		return mailSender;
	}

	public void setMailSender(MailSender mailSender) {
		this.mailSender = mailSender;
	}

	public SimpleMailMessage getMailMessage() {
		return mailMessage;
	}

	public void setMailMessage(SimpleMailMessage mailMessage) {
		this.mailMessage = mailMessage;
	}

	public void sendMessage(String nombre, String email, String mensaje, String subjectData) {
		try {
			String[] bccs = { "ramirogm@gmail.com", "betianad@gmail.com"};
			this.mailMessage.setBcc(bccs);
			this.mailMessage.setFrom("Página Web del Hotel Diamante <noreply@diamantehotel.com.ar>");
			String subject = "[PAGINA WEB]";
			if ( subjectData != null && subjectData.length() > 0) {
				subject = subject + "[" + subjectData + "]";
			}
			this.mailMessage.setSubject(subject);
			String mailText = String.format("Nombre: %s\nemail: %s\nMensaje: %s\n", nombre, email, mensaje); 
			this.mailMessage.setText(mailText);
			this.mailMessage.setTo("info@diamantehotel.com.ar");
			this.mailSender.send(this.mailMessage);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("No pude mandar el mail", e);
			throw new RuntimeException("No puede mandar el mail", e);
		}
	}

}
