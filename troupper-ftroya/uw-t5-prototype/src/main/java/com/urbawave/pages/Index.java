package com.urbawave.pages;

import org.apache.tapestry5.annotations.SessionState;

import com.urbawave.domain.User;

/**
 * Start page of application uw-t5-prototype.
 */
public class Index {

	@SessionState
	private User user;

	private boolean userExists;

	public Object onActivate() {
		return userExists ? Profile.class : Login.class;
	}
}
