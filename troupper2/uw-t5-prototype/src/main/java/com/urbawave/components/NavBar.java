/**
 * 
 */
package com.urbawave.components;

import java.util.HashMap;
import java.util.Map;

import org.apache.tapestry5.Block;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.RequestGlobals;
import org.apache.tapestry5.services.Session;

import com.urbawave.pages.Login;
import com.urbawave.services.PageName;

/**
 * @author ftroya
 * 
 */
public class NavBar {

    @Inject
    private ComponentResources resources;
    
    private String pageName;

    @Inject
    private Block commonNavBar, register, index, profile, login, mysnapshots, partner, mobile, forgot, settings;

    @Inject
    private Block loggedNavBar;

    @Inject
    private Block logoutTab;

    private Map<PageName, Block> matchingBlock = new HashMap<PageName, Block>();

    @Inject
    private RequestGlobals requestGlobals;

    public Object getPageBlock() {
    	String pageUpper = this.pageName.toUpperCase();
		Block block = this.matchingBlock.get(PageName.valueOf(pageUpper));
		return block == null ? this.index : block;
    }

    public Block getCommonNavBar() {
    	return this.commonNavBar;
    }

    public Block getLoggedNavBar() {
    	return this.loggedNavBar;
    }
    
    public Block getLogoutTab() {
    	return this.logoutTab;
    }
    
    @SetupRender
    public void initializeValue() {
		this.pageName = this.resources.getPageName();
		this.matchingBlock.put(PageName.INDEX, this.index);
		this.matchingBlock.put(PageName.PROFILE, this.profile);
		this.matchingBlock.put(PageName.MYSNAPSHOTS, this.mysnapshots);
		this.matchingBlock.put(PageName.PROFILESNAPSHOTS, this.mysnapshots);
		this.matchingBlock.put(PageName.SETTINGS, this.settings);
		this.matchingBlock.put(PageName.LOGIN, this.login);
		this.matchingBlock.put(PageName.MOBILE, this.mobile);
		this.matchingBlock.put(PageName.PARTNER, this.partner);
		this.matchingBlock.put(PageName.FORGOT, this.forgot);
		this.matchingBlock.put(PageName.REGISTER, this.register);
	}

    public Object onActionFromLogout() {
		Session session = this.requestGlobals.getRequest().getSession(false);
	
		if ((session != null) && (!session.isInvalidated())) {
		    session.invalidate();
		}
		return Login.class;
    }

}
