//
//  UIServiceButton.m
//  troupper.0
//
//  Created by Betiana Darderes on 1/22/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "UIServiceButton.h"


@implementation UIServiceButton

@synthesize service;

- (void)dealloc
{
   
	[service release];    
    [super dealloc];
}

@end
