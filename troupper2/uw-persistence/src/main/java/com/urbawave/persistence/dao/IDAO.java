/**
 * 
 */
package com.urbawave.persistence.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @author ftroya
 * 
 */
public interface IDAO {

    public void delete(Collection entitys);

    public void delete(Object entity);

    public <T> List<T> find(Class<T> entityClass);

    public <T> List<T> find(String hql);

    public <T> T load(Class<T> entityClass, Serializable id);

    public void persist(Object entity);

    public void persist(Object[] entities);

    public void refresh(Object entity);

}
