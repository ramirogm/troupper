//
//  UIFriendsController.h
//  troupper.0
//
//  Created by Betiana Darderes on 6/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFriendsTableViewController.h"


@interface UIFriendsController : UIViewController {

	UIFriendsTableViewController *friendsTableController;
	
}
@property (nonatomic, retain) IBOutlet UIFriendsTableViewController *friendsTableController;


@end
