package com.urbawave.components;

import org.apache.tapestry5.Asset;
import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.annotations.AfterRender;
import org.apache.tapestry5.annotations.BeginRender;
import org.apache.tapestry5.annotations.Mixin;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.corelib.mixins.RenderInformals;

public class Image {
	@Parameter(required = true)
	private Asset _imageAsset;

	@Parameter
	private String _description;

	@Mixin
	private RenderInformals _renderInformals;

	@BeginRender
	void renderImageTag(MarkupWriter writer) {
		writer.element("img", "src", _imageAsset.toClientURL(), "alt", _description);

	}

	@AfterRender
	void afterRender(MarkupWriter writer) {
		writer.end();
	}
}
