//
//  UIFriendsTableViewController.h
//  troupper.0
//
//  Created by Betiana Darderes on 6/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIFriendsTableViewController : UITableViewController <UITextFieldDelegate> {
	
	UITableViewCell *tituloCell;
    UITableViewCell *botonesCell;
    UILabel *tituloLabel;
	
	
}

@property (nonatomic, retain) IBOutlet UITableViewCell *tituloCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *botonesCell;
@property (nonatomic, retain) IBOutlet UILabel *tituloLabel;

//- (IBAction)logHello;
//- (IBAction)sliderValueChanged:(UISlider *)slider;

@end
