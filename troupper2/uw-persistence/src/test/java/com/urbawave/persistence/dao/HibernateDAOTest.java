package com.urbawave.persistence.dao;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.urbawave.domain.Profile;
import com.urbawave.domain.Snapshot;
import com.urbawave.domain.Tag;
import com.urbawave.domain.User;

public class HibernateDAOTest extends BaseDaoTestCase {

	Logger logger = Logger.getLogger(HibernateDAOTest.class);

	@Autowired
	HibernateDAO hibernateDAO;

    public void testPersistProfile() {
	Profile entity = new Profile();
	entity.setName("profileTest");
	entity.setEmail("entity@entity.com");
	this.hibernateDAO.persist(entity);
	// Profile entity2 = this.hibernateDAO.load(Profile.class, 0);
	// this.logger.info(entity2.getName());

    }

    public void testPersistSnapshot() {
	User user = new User();
	Profile p = new Profile();
	p.setUser(user);
	
	Tag tag1 = new Tag();
	tag1.setName("reunion de trabajo");

	Snapshot snap1 = new Snapshot();
	snap1.getTags().add(tag1);

	p.getSnapshots().add(snap1);

	this.hibernateDAO.persist(user);

    }

}
