    //
//  UIUserStatusController.m
//  troupper.0
//
//  Created by Betiana Darderes on 6/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "UIUserStatusController.h"
#import "ProfileViewController.h"
#import "ShadowedTableView.h"
#import "TroupperAppDelegate.h"
#import "Components.h"
#import "GradientView.h"
#import "ClearLabelsCellView.h"
#import "User.h"
#import "NewProfileController.h"
#import "TrouperService.h"





@implementation UIUserStatusController


@synthesize navController;
@synthesize theTableView;
@synthesize dataSource;

NSIndexPath* lastIndexSelected;

TrouperService* service;

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	
	UIImage *anImage = [UIImage imageNamed: @"man.png"];
	UIImageView* imageView = [[UIImageView alloc ]initWithImage:anImage];
	[anImage release];
	
	//self.navigationItem.title.text = @"Profiles";
	//self.titleView = imageView;
	
	[imageView release];
	
	
	self.navigationItem.hidesBackButton = YES;	


	
	
	
	
	// create a new table using the full application frame
	// we'll ask the datasource which type of table to use (plain or grouped)
	ShadowedTableView *tableView = [[ShadowedTableView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame] 
																	  style:[dataSource tableViewStyle]];
	
	// set the autoresizing mask so that the table will always fill the view
	tableView.autoresizingMask = (UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight);
	
	// set the cell separator to a single straight line.
	tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	
	tableView.backgroundColor = UIColor.groupTableViewBackgroundColor; 
	
	// set the tableview delegate to this object and the datasource to the datasource which has already been set
	tableView.delegate = self;
	tableView.dataSource = self;
	
	tableView.sectionIndexMinimumDisplayRowCount=2;
	
	// set the tableview as the controller view
    self.theTableView = tableView;
	self.view = tableView;
	[tableView release];
	
	
	
	/*
	// Create cancel button
	UIBarButtonItem *newProfile = [[UIBarButtonItem alloc] 
									initWithTitle:@"New" 
									style:UIBarButtonItemStylePlain 
									target:self 
									action:@selector(newUser)];
	
	self.navigationItem.rightBarButtonItem = newProfile;
	[newProfile release];
	 */
	
	//Sets a profile as public
	[[Components sharedPlacesInstance] publicUserProfile];
	
	service = [[[TrouperService alloc] initWith: nil delegate: nil] retain];
	
	
    [super viewDidLoad];
	
}






- (void)newUser {
	
	NewProfileController * controller = [[NewProfileController alloc] initNewUser];
	[navController pushViewController:controller animated:YES];
	[controller release];
	[super viewDidLoad];
	
	
}

- (void)dealloc {
	
	[service release];
	
	[navController release];
	
	[theTableView release];
	[dataSource release];
	[super dealloc];
}




-(void)viewWillAppear:(BOOL)animated
{
	// force the tableview to load
	
	
	[theTableView reloadData];
}


//
//
// UITableViewDelegate methods
//
//



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 85;
	
}




// return the surface element at the index 
- (User *)userForIndexPath:(NSIndexPath *)indexPath {
	return [[[Components sharedPlacesInstance] userProfiles] objectAtIndex:indexPath.row];
}


// return the surface element at the index 
- (NSIndexPath *)indexForUser:(User *)aUser {
	return [NSIndexPath indexPathWithIndex: [[[Components sharedPlacesInstance] userProfiles] indexOfObject: aUser]];
}


// UITableViewDataSource methods

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	//Gets the surface element for the received indexPath 
	User * user = [self userForIndexPath:indexPath];
	
	//Creates the cell with the surface
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[ClearLabelsCellView alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cell.backgroundView = [[[GradientView alloc] init] autorelease];
    }
	
	if( [user isPublished]){
		
		cell.accessoryType = UITableViewCellAccessoryCheckmark;
	    lastIndexSelected = indexPath;}
	
	else cell.accessoryType = UITableViewCellAccessoryNone;
	
    cell.imageView.image = [user thumbnail];
	
	UIButton *myButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    myButton.frame = CGRectMake(100, 25 , 150, 40); // position in the parent view and set the size of the button
    [myButton setTitle:[user userName] forState:UIControlStateNormal];
    [myButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
	myButton.tag = [user tag];
	
	//Cambia el color en funcion del booleano publicado.
	[cell.contentView addSubview:myButton];
	
	return cell;
	
}

//The user send the form
- (IBAction)buttonClicked:(id)sender {
	
	
		
	if ([sender isKindOfClass:[UIButton class]]) {
		
		
	UIButton *aButton = (UIButton *)sender; // we know the sender is a UIButton object, so cast it
	
	int tag = aButton.tag;
	
	User * selectedUser = [[Components sharedPlacesInstance] userProfileForTag:tag];	
	NewProfileController * profileController = [[NewProfileController alloc] initWithUser: selectedUser];
	[navController pushViewController:profileController animated:YES];
	[profileController release];
	}
	
	
	
}


- (void) putOffline: (User*) aProfile {

	User* user = [[Components sharedPlacesInstance] loggedUser];
	NSString* offlineServiceString = [NSString stringWithFormat: @"users/notifications/%@/%@/offline", [user userName], [aProfile userName] ];
	[service startWithString:offlineServiceString];
	
	

	
	
}


- (void) putOnline: (User*) aProfile {
	
	User* user = [[Components sharedPlacesInstance] loggedUser];
	NSString* onlineServiceString = [NSString stringWithFormat: @"users/notifications/%@/%@/online", [user userName], [aProfile userName] ];
	[service startWithString:onlineServiceString];
		
}


// the user selected a row in the table.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)newIndexPath {
	
	User * userSelected = [self userForIndexPath:newIndexPath];
	User * lastUserSelected  = [self userForIndexPath:lastIndexSelected];
	
	UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:newIndexPath];
	
	UITableViewCell *lastSelectedCell = [tableView cellForRowAtIndexPath:lastIndexSelected];
	
	if ([selectedCell accessoryType] == UITableViewCellAccessoryNone) {
		
        [selectedCell setAccessoryType:UITableViewCellAccessoryCheckmark];
		[lastSelectedCell setAccessoryType: UITableViewCellAccessoryNone];
		
		[userSelected setIsPublished: YES];
		[lastUserSelected setIsPublished: NO];
		
		lastIndexSelected = newIndexPath;
		
		User* user = [[Components sharedPlacesInstance] loggedUser];
		
		if ([user isPublished]) {
			
			[self putOffline: lastUserSelected];
			[self putOnline: userSelected];
		}
		
		
    } 
	
    [tableView deselectRowAtIndexPath:newIndexPath animated:NO];
   	
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView  {
	// this table has only one section
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView  numberOfRowsInSection:(NSInteger)section {
	
	return [[[Components sharedPlacesInstance] userProfiles]count];
}






@end
