/**
 * 
 */
package com.urbawave.components;

import java.util.HashMap;
import java.util.Map;

import org.apache.tapestry5.Block;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.ioc.annotations.Inject;

import com.urbawave.services.PageName;

/**
 * @author ftroya
 * 
 */
public class NavBar {

	@Inject
	private ComponentResources resources;
	private String pageName;

	@Inject
	private Block commonNavBar, register, index, profile, login;
	private Map<PageName, Block> matchingBlock = new HashMap<PageName, Block>();

	@SetupRender
	void initializeValue() {
		pageName = resources.getPageName();
		matchingBlock.put(PageName.INDEX, index);
		matchingBlock.put(PageName.PROFILE, profile);
		matchingBlock.put(PageName.LOGIN, login);
	}

	public Object getCase() {
		Block block = matchingBlock.get(PageName
				.valueOf(pageName.toUpperCase()));
		return block == null ? index : block;
	}

	public Block getCommonNavBar() {
		return commonNavBar;
	}
}
