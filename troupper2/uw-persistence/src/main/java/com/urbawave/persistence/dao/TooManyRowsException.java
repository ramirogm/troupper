package com.urbawave.persistence.dao;

public class TooManyRowsException extends Exception {

	public TooManyRowsException(String string) {
		super(string);
	}

}
