/**
 * 
 */
package com.urbawave.persistence.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.apache.tapestry5.hibernate.annotations.CommitAfter;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import com.urbawave.domain.Profile;
import com.urbawave.domain.User;

/**
 * @author ftroya
 * 
 */
public class ProfileDAO extends HibernateDaoSupport implements IDAO {

	public ProfileDAO() {
	}

	@Autowired
	public ProfileDAO(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.springframework.orm.hibernate3.support.HibernateDaoSupport#
	 * createHibernateTemplate(org.hibernate.SessionFactory)
	 */
	@Override
	protected HibernateTemplate createHibernateTemplate(
			SessionFactory sessionFactory) {
		HibernateTemplate hibernateTemplate = super
				.createHibernateTemplate(sessionFactory);
		hibernateTemplate.setFlushMode(10);  // TODO Que es este 10?
		return hibernateTemplate;
	}
	
	public Profile findByUserAndProfileName(String userName, String profileName) {
		List<Profile> profiles = this.find("from Profile where user.userName = '" + userName + 
				"' and name = '" + profileName + "'");
		if ( profiles.size() == 0 ) return null;
		return (Profile) profiles.get(0);
	}

	public List<Profile> findOnlineProfiles() {
		return this.find("from Profile where online = TRUE ");
	}

	// @Transactional(readOnly = true)
	public <T> List<T> find(Class<T> entityClass) {
		final List<T> entities = getHibernateTemplate().loadAll(entityClass);
		return entities;
	}

	// @Transactional(readOnly = true)
	public <T> List<T> find(String hql) {
		final List<T> entities = getHibernateTemplate().find(hql);
		return entities;
	}

	@Transactional(readOnly = false)
	@CommitAfter
	public void delete(Collection entitys) {
		getHibernateTemplate().deleteAll(entitys);
	}

	@Transactional(readOnly = false)
	@CommitAfter
	public void delete(Object entity) {
		getHibernateTemplate().delete(entity);
	}

    public Profile findById(int id) {
    	List<Profile> profiles = this.find("from Profile p where p.id = " + id);
		if ( profiles.size() == 0 ) return null;
		return (Profile) profiles.get(0);
    }

	//@Transactional(readOnly = true)  // Saco esto xq no es consistente con el uso que le damos
    // ni con los métodos de find
	public <T> T load(Class<T> entityClass, Serializable id) {
		final T entity = getHibernateTemplate().load(entityClass, id);
		return entity;
	}

	@Transactional(readOnly = false)
	@CommitAfter
	public void persist(Object entity) {
		getHibernateTemplate().saveOrUpdate(entity);
	}

	@Transactional
	public void persist(Object[] entities) {
		for (Object entitie : entities) {
			persist(entitie);
		}
	}

	@Transactional(readOnly = true)
	public void refresh(Object entity) {
		getHibernateTemplate().refresh(entity);
	}

    public void saveProfile(Profile aProfile) {
    	this.persist(aProfile);
    }

}
