    //
//  UIFriendsController.m
//  troupper.0
//
//  Created by Betiana Darderes on 6/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "UIFriendsController.h"
#import "TroupperAppDelegate.h"


@implementation UIFriendsController
@synthesize friendsTableController;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


 - (id)init {
 self = [super initWithNibName:@"friendsView" bundle:nil];
 if (self) {
	
 }
 return self;
 }
 
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	// Create cancel button
	UIBarButtonItem *publishButton = [[UIBarButtonItem alloc] 
									  initWithTitle:@"Publish me here" 
									  style:UIBarButtonItemStylePlain 
									  target:self 
									  action:@selector(publish:)];
	
	self.navigationItem.leftBarButtonItem = publishButton;
	[publishButton release];
	

	
	
    [super viewDidLoad];
}

//The user cancels
- (IBAction)publish:(id)sender {
	
	
}
/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
