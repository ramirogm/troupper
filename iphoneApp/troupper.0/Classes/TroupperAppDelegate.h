//
//  TroupperAppDelegate.h
//  troupper.0
//
//  Created by Betiana Darderes on 4/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIInitialController.h"

@interface TroupperAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
	UIInitialController *initController;
	UINavigationController *iniNavController;
	UITabBarController *tabBarController;
	
}


@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) UIInitialController *initController;
@property (nonatomic, retain) IBOutlet UITabBarController *tabBarController;
@property (nonatomic, retain) IBOutlet UINavigationController *iniNavController;

- (void)startTabBarController;

@end

