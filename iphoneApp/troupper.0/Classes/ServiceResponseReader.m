//
//  ServiceResponseReader.m
//  troupper.0
//
//  Created by Betiana Darderes on 1/29/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//


//URLs
//http://localhost:8080/uw-ws/uwservices/
///users
///login/{username}/
///{username}/activeprofile/picture
///{username}/{profilename}/picture
//
//http://192.168.41.100:8080/uw-ws/uwservices/users/bart/Bart%20Simpson/picture

#import "ServiceResponseReader.h"
#import "SBJSON.h"
#import "User.h"


@implementation ServiceResponseReader


UIActivityIndicatorView* activityIndicator;

- (id)initWith:(UIActivityIndicatorView *)anActivityIndicator {
	self = [super init];
	if (self) {
		activityIndicator =anActivityIndicator;
		
	}
	return self;
}

	
	//{ "error" : "WRONG PASSWORD" } 

-(NSString*) errorLoginFrom: (NSData*) responseData{
		
	NSString * errorString = @"";
	NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	NSError *error;
	SBJSON *json = [[SBJSON new] autorelease];
	NSDictionary *luckyNumbers = [json objectWithString:responseString error:&error];
	[responseString release];	
			
	if (luckyNumbers == nil){
		NSLog ( @"%@", [NSString stringWithFormat:@"JSON parsing failed: %@", [error localizedDescription]]);
		[activityIndicator setHidden:  YES];
		[activityIndicator stopAnimating];
				
	}
	
	else {		
		NSMutableString *text = [NSMutableString stringWithString:@"Lucky numbers:\n"];
		 NSLog ( @"%@", luckyNumbers);
		//NSDictionary *registro = [luckyNumbers objectAtIndex:0];
		//[text appendFormat:@"%@\n", registro] ;
	    
		
		errorString = [luckyNumbers objectForKey:@"error"];
		
			}
	
    //retorna el string del error, si lo hubiera
		
	//retorna nil, si no hubo error
	return errorString;	
				
	}


-(NSString*) resultForm: (NSData*) responseData{
	
	
	return [[NSString alloc ] initWithData:responseData encoding:NSUTF8StringEncoding];
		

	
}

	
	//{ "user" : "Bartolomeo Simpson"} 
			
-(User*) loggedUserFrom: (NSData*) responseData{
				
				NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
				
				NSError *error;
				SBJSON *json = [[SBJSON new] autorelease];
				NSDictionary *luckyNumbers = [json objectWithString:responseString error:&error];
				[responseString release];	
				
	if (luckyNumbers == nil){
		NSLog ( @"%@", [NSString stringWithFormat:@"JSON parsing failed: %@", [error localizedDescription]]);
		[activityIndicator stopAnimating];
		[activityIndicator setHidden: YES];
	}
				else {		
					NSMutableString *text = [NSMutableString stringWithString:@"Lucky numbers:\n"];
					
					//llegan los datos de un usuario, por lo tanto es un registro solo;
					NSLog ( @"%@", luckyNumbers);
					
						
					
					
					/*
					 
					 "username" : "homero",
					 "fullname" : "Homero J Simpson",
					 "email" : "homero@gmail.com",
					 "id" : "8"
					 
					 */
					User * user = [[User alloc] init];
					[user setFirstName: [luckyNumbers objectForKey: @"user"]];
					//[user setUserName: [registro objectForKey: @"username"]];
					//[user setEmail: [registro objectForKey: @"email"]];
					//[user setTag: [registro objectForKey: @"id"]];
					return user;
					
					NSLog ( @"%@", text);
					
					
				}
	return nil;		
}			
				

-(NSArray*) userProfilesFrom: (NSData*) responseData{
	
	NSMutableArray *  profiles = [NSMutableArray arrayWithCapacity:10];
	
			
	NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
	
	NSLog(responseString);
	NSError *error;
	SBJSON *json = [[SBJSON new] autorelease];
	NSArray *luckyNumbers = [json objectWithString:responseString error:&error];
	[responseString release];	
	//Ojo que a veces me viene un diccionario..
			
    if (luckyNumbers == nil){
		NSLog ( @"%@", [NSString stringWithFormat:@"JSON parsing failed: %@", [error localizedDescription]]);
		[activityIndicator stopAnimating];
		[activityIndicator setHidden: YES];
	}
			else {	
				
				if ([luckyNumbers isKindOfClass:[NSArray class]]) {
				
				NSMutableString *text = [NSMutableString stringWithString:@"Lucky numbers:\n"];
				
				for (int i = 0; i < [luckyNumbers count]; i++) {
					NSDictionary *registro = [luckyNumbers objectAtIndex:i];
					[text appendFormat:@"%@\n", registro] ;
					/*
					 
					 "name" : "HomeroTrabajo",
					 "id" : "9",
					 "bio" : "Trabajando",
					 "interests" : ["donuts", "tv", "comida en general"]
					 }
					 ,{
					 "name" : "HomeroGordo",
					 "id" : "10",
					 "bio" : "Gordos A",
					 "interests" : ["donuts", "dieta", "vestidos"]
					 
					 */
					User * user = [[User alloc] init];
					//[user setFirstName: [registro objectForKey: @"fullname"]];
					[user setUserName: [registro objectForKey: @"name"]];
					[user setBio: [registro objectForKey: @"bio"]];
					[user setInterests: [registro objectForKey: @"interestsList"]];
					[user setTag: [[registro objectForKey: @"id"]intValue]];
					
					http://192.168.41.100:8080/uw-ws/uwservices/users/bart/Bart%20Simpson/picture
					
				
					
					[profiles addObject: user];
					
				}
				
				NSLog ( @"%@", text);
				
				}
				
}
	return profiles;
}
			
@end
