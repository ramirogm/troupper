//
//  HomePlaceDataSource.m
//  KClark
//
//  Created by Betiana Darderes on 7/21/11.
//


#import "HomePlaceDataSource.h"
#import "Components.h"
#import "User.h"
#import "GradientView.h"
#import "ClearLabelsCellView.h"


@implementation HomePlaceDataSource 

@synthesize name;
@synthesize navigationBarName;

User *user;

//Returns the navigation bar item
- (NSString *)navigationBarName {
	return navigationBarName;
}

//Returns the name
- (NSString *)name {
	return name;
}

- (BOOL)showDisclosureIcon {
	return YES;
}

//Sets the table style
- (UITableViewStyle)tableViewStyle  {
	return UITableViewStylePlain;
}


// return the surface element at the index 
- (User *)userForIndexPath:(NSIndexPath *)indexPath {
	return [[[Components sharedPlacesInstance] connUsers] objectAtIndex:indexPath.row];
}


// UITableViewDataSource methods

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	//Gets the surface element for the received indexPath 
	user = [self userForIndexPath:indexPath];
	
	//Creates the cell with the surface
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[ClearLabelsCellView alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cell.backgroundView = [[[GradientView alloc] init] autorelease];
    }
	if ([self showDisclosureIcon])
		cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	
	//Sets the surface name
	//cell.textLabel.text = [user userName];
	
	   
	//Creates the surface image
	NSString *path = [[NSBundle mainBundle] pathForResource:[user photo] ofType:@"png"];
    UIImage *theImage = [UIImage imageWithContentsOfFile:path];
    cell.imageView.image = theImage;
	
	UIButton *myButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    myButton.frame = CGRectMake(100, 25 , 100, 40); // position in the parent view and set the size of the button
    [myButton setTitle:[user userName] forState:UIControlStateNormal];
    // add targets and actions

	
    [myButton addTarget: tableView.delegate action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
	
	[cell.contentView addSubview:myButton];
	
    return cell;
	

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView  {
	// this table has only one section
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView  numberOfRowsInSection:(NSInteger)section {
	
	return [[[Components sharedPlacesInstance] connUsers]count];
}

- (void)dealloc {
	
	[name release];
    [super dealloc];
}

@end
