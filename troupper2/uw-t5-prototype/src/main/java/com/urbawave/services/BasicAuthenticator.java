package com.urbawave.services;

import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.Session;

import com.urbawave.domain.User;
import com.urbawave.persistence.dao.HibernateDAO;
import com.urbawave.persistence.dao.TooManyRowsException;
import com.urbawave.security.AuthenticationException;

/**
 * Basic Security Realm implementation
 * 
 * @author karesti
 * @version 1.0
 */
public class BasicAuthenticator implements Authenticator {

	public static final String AUTH_TOKEN = "authToken";

	@Inject
	private HibernateDAO crudService;

	@Inject
	private Request request;

	public User getLoggedUser() {
		User user = null;

		if (isLoggedIn()) {
			user = (User) this.request.getSession(true)
					.getAttribute(AUTH_TOKEN);
		} else {
			throw new IllegalStateException("The user is not logged ! ");
		}
		return user;
	}

	public boolean isLoggedIn() {
		Session session = this.request.getSession(false);
		if (session != null) {
			return session.getAttribute(AUTH_TOKEN) != null;
		}
		return false;
	}

	public void login(String username, String password)
			throws AuthenticationException {

		User user;
		try {
			user = this.crudService.findUniqueByUserName(username);
			if (user == null) {
				throw new AuthenticationException("The user doesn't exist");
			}

			if ( !user.getPassword().equals(password)) {
				throw new AuthenticationException("Wrong Password");
			}
			
			this.request.getSession(true).setAttribute(AUTH_TOKEN, user);
		} catch (TooManyRowsException e) {
			throw new AuthenticationException("Internal error, please try a different user");
		}

	}

	public void logout() {
		Session session = this.request.getSession(false);
		if (session != null) {
			session.setAttribute(AUTH_TOKEN, null);
			session.invalidate();
		}
	}

}
