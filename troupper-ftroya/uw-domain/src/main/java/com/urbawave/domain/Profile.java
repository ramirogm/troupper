/**
 * 
 */
package com.urbawave.domain;

import java.util.Arrays;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.urbawave.domain.listeners.ProfileListener;

/**
 * @author ftroya
 * 
 */
@Entity
@EntityListeners({ProfileListener.class})
public class Profile {

	@Id
	@GeneratedValue
	Integer id;

	String name;
	String imagePath;

	@ManyToOne
	@JoinColumn (name="house")
	User user;

	String interestsAsString;

	@Transient
	List<String> interests = Arrays.asList(new String[]{"notPersistent1","notPersistent2"});

	Boolean active = Boolean.FALSE;
	Boolean isPublic = Boolean.FALSE;
	String bio;
	String homepage;
	String email;
	String otherInterests;

	// static constraints = {
	// bio(nullable: true, maxSize: 1000)
	// homepage(url: true, nullable: true)
	// email(email: true, nullable: true)
	// imagePath(nullable: true)
	// }

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the imagePath
	 */
	public String getImagePath() {
		return imagePath;
	}

	/**
	 * @param imagePath
	 *            the imagePath to set
	 */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the active
	 */
	public Boolean getActive() {
		return active;
	}

	/**
	 * @param active
	 *            the active to set
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}

	/**
	 * @return the isPublic
	 */
	public Boolean getIsPublic() {
		return isPublic;
	}

	/**
	 * @param isPublic
	 *            the isPublic to set
	 */
	public void setIsPublic(Boolean isPublic) {
		this.isPublic = isPublic;
	}

	
	/**
	 * @return the bio
	 */
	public String getBio() {
		return bio;
	}

	/**
	 * @param bio
	 *            the bio to set
	 */
	public void setBio(String bio) {
		this.bio = bio;
	}

	/**
	 * @return the homepage
	 */
	public String getHomepage() {
		return homepage;
	}

	/**
	 * @param homepage
	 *            the homepage to set
	 */
	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the otherInterests
	 */
	public String getOtherInterests() {
		return otherInterests;
	}

	/**
	 * @param otherInterests
	 *            the otherInterests to set
	 */
	public void setOtherInterests(String otherInterests) {
		this.otherInterests = otherInterests;
	}

	/**
	 * @return the interests
	 */
	public List<String> getInterests() {
		return interests;
	}

	/**
	 * @param interests
	 *            the interests to set
	 */
	public void setInterests(List<String> interests) {
		this.interests = interests;
	}

	/**
	 * @return the interestsAsString
	 */
	public String getInterestsAsString() {
		return interestsAsString;
	}

	/**
	 * @param interestsAsString the interestsAsString to set
	 */
	public void setInterestsAsString(String interestsAsString) {
		this.interestsAsString = interestsAsString;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}



	
}
