//
//  ProfileDataSource.m
//  troupper.0
//
//  Created by Betiana Darderes on 10/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ProfileDataSource.h"
#import "Components.h"
#import "User.h"
#import "GradientView.h"
#import "ClearLabelsCellView.h"
#import "User.h"

@implementation ProfileDataSource

@synthesize name;
@synthesize navigationBarName;



//Returns the navigation bar item
- (NSString *)navigationBarName {
	return navigationBarName;
}

//Returns the name
- (NSString *)name {
	return name;
}

- (BOOL)showDisclosureIcon {
	return YES;
}

//Sets the table style
- (UITableViewStyle)tableViewStyle  {
	return UITableViewStylePlain;
}


// return the surface element at the index 
- (User *)userForIndexPath:(NSIndexPath *)indexPath {
	return [[[Components sharedPlacesInstance] userProfiles] objectAtIndex:indexPath.row];
}


// UITableViewDataSource methods

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	//Gets the surface element for the received indexPath 
	User * user = [self userForIndexPath:indexPath];
	
	//Creates the cell with the surface
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[ClearLabelsCellView alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cell.backgroundView = [[[GradientView alloc] init] autorelease];
    }
	
	if( [user isPublished])
	
	cell.accessoryType = UITableViewCellAccessoryCheckmark;
	
	else cell.accessoryType = UITableViewCellAccessoryNone;
	
	//Sets the surface name
	//cell.textLabel.text = [user userName];
	
	//Creates the surface image
	
    cell.imageView.image = [user thumbnail];
	
	UIButton *myButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    myButton.frame = CGRectMake(100, 25 , 100, 40); // position in the parent view and set the size of the button
    [myButton setTitle:[user userName] forState:UIControlStateNormal];
    [myButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
	myButton.tag = [user tag];
	
	//Cambia el color en funcion del booleano publicado.
	[cell.contentView addSubview:myButton];
	
	return cell;
	
}



// the user selected a row in the table.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)newIndexPath {
	
	User * userSelected = [self userForIndexPath:newIndexPath];
	User * lastUserSelected  = [self userForIndexPath:lastIndexSelected];

	UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:newIndexPath];
	
	UITableViewCell *lastSelectedCell = [tableView cellForRowAtIndexPath:lastIndexSelected];
	
	if ([selectedCell accessoryType] == UITableViewCellAccessoryNone) {
		
        [selectedCell setAccessoryType:UITableViewCellAccessoryCheckmark];
		[lastSelectedCell setAccessoryType: UITableViewCellAccessoryNone];
		
        [userSelected setIsPublished: YES];
		[lastUserSelected setIsPublished: NO];
		
		lastIndexSelected = newIndexPath;
		
		
    } 
    	
    [tableView deselectRowAtIndexPath:newIndexPath animated:NO];
   	
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView  {
	// this table has only one section
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView  numberOfRowsInSection:(NSInteger)section {
	
	return [[[Components sharedPlacesInstance] userProfiles]count];
}


- (void)dealloc {
	
	[name release];
    [super dealloc];
}

@end