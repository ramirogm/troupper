/**
 * 
 */
package com.urbawave.pages;

import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.annotations.Inject;

import com.urbawave.domain.User;
import com.urbawave.persistence.dao.HibernateDAO;
import com.urbawave.persistence.dao.TooManyRowsException;
import com.urbawave.services.Authenticator;

/**
 * @author ramiro
 * 
 */
public class Settings {

	@Inject
	private Authenticator authenticator;

	@Inject
	private HibernateDAO dao;

	@SessionState
	@Property
	private User user;
	
	private User userInDB;
	
    @Component
    private Form settingsForm;


    /**
     * Valido que no haya usuario con el mismo username
     */
    public void onValidate()
    {
        checkUsernameUsed();
		checkEmailUsed();
    }

    private void checkEmailUsed() {
    	if ( !this.user.getEmail().equals(this.userInDB.getEmail())) {
    		try {
            	User otroUser = dao.findUniqueByEmail(this.user.getEmail());
            	if (otroUser != null ) {
        			settingsForm.recordError( "email already used by another user. Please choose another one.");
            	}
    		} catch (TooManyRowsException e) {
    			settingsForm.recordError( "Internal error, please try again.");
    		}
    		
    	}
	}

    private void checkUsernameUsed() {
    	if ( !this.user.getUserName().equals(this.userInDB.getUserName())) {
    		try {
            	User otroUser = dao.findUniqueByUserName(this.user.getUserName());
            	if (otroUser != null ) {
        			settingsForm.recordError( "username already taken by another user. Please choose another one.");
            	}
    		} catch (TooManyRowsException e) {
    			settingsForm.recordError( "Internal error, please try again.");
    		}
    		
    	}
	}

	public void onActivate() throws TooManyRowsException {
		if ( userInDB == null ) {
			userInDB = dao.findUserByID(user.getId());
		}
	}

	public Object onSuccess() {
	
		try {
			if ( this.user.getPassword().length()!=0) this.userInDB.setPassword(this.user.getPassword());
			this.userInDB.setEmail(this.user.getEmail());
			this.userInDB.setUserName(this.user.getUserName());
			this.userInDB.setFullName(this.user.getFullName());
			this.dao.persist(this.userInDB);
			// Esto no sé si realmente actualiza el valor en la session...
			this.user = this.userInDB;
		} catch (Exception e) {
			e.printStackTrace();
        	settingsForm.recordError("Internal error, please try again.");
        	return null;
		}
		return Index.class;	
	}



}
