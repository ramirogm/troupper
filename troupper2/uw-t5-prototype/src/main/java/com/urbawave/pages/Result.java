package com.urbawave.pages;

import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;

import com.urbawave.annotations.AnonymousAccess;


@AnonymousAccess
public class Result {

    @Persist
    private String mensaje;

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
    
    
    
}
