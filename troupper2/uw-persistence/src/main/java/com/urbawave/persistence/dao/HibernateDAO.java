/**
 * 
 */
package com.urbawave.persistence.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.apache.tapestry5.hibernate.annotations.CommitAfter;
import org.hibernate.HibernateException;
import org.hibernate.NonUniqueResultException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.urbawave.domain.Snapshot;
import com.urbawave.domain.Tag;
import com.urbawave.domain.User;
import com.urbawave.domain.dto.SnapshotDTO;

/**
 * @author ftroya
 * 
 */
@Repository
public class HibernateDAO extends HibernateDaoSupport implements IDAO {

	public HibernateDAO() {
	}

	@Autowired
	public HibernateDAO(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.springframework.orm.hibernate3.support.HibernateDaoSupport#
	 * createHibernateTemplate(org.hibernate.SessionFactory)
	 */
	@Override
	protected HibernateTemplate createHibernateTemplate(
			SessionFactory sessionFactory) {
		HibernateTemplate hibernateTemplate = super
				.createHibernateTemplate(sessionFactory);
		hibernateTemplate.setFlushMode(10);  // Esto de 10 de dónde salió?
		return hibernateTemplate;
	}

	@Transactional(readOnly = false)
	@CommitAfter
	public void delete(Collection entitys) {
		getHibernateTemplate().deleteAll(entitys);
	}

	@Transactional(readOnly = false)
	@CommitAfter
	public void delete(Object entity) {
		getHibernateTemplate().delete(entity);
	}

	// @Transactional(readOnly = true)
	public <T> List<T> find(Class<T> entityClass) {
		final List<T> entities = getHibernateTemplate().loadAll(entityClass);
		return entities;
	}

	// @Transactional(readOnly = true)
	public <T> List<T> find(String hql) {
		final List<T> entities = getHibernateTemplate().find(hql);
		return entities;
	}

	// TODO esto va en UserDAO. Pero ojo que si muevo a subclases tengo que revisar config spring porque va a encontrar dos DAOs
	// ( User e Hibernate que pueden tomarse como implementaciones de HibernateDAO )
	public User findByUserName(String userName) {
		List<Object> users = this.find("from User where userName='" + userName
				+ "'");
		//if ( users.size() > 1) throw new RuntimeException("Hay más de un usuario con ese username");
		return users.isEmpty() ? null : (User) users.get(0);
	}

	// TODO puede haber varios?
	public Snapshot findBySnapshotName(String name) {
		List<Object> snapshots = this.find("from Snapshot where name='" + name
				+ "'");
		return snapshots.isEmpty() ? null : (Snapshot) snapshots.get(0);
	}

	// TODO Puede haber varios?
	public Tag findByTagName(String name) {
		List<Object> tags = this.find("from Tag where name='" + name
				+ "'");
		return tags.isEmpty() ? null : (Tag) tags.get(0);
	}


	//@Transactional(readOnly = true)
	public <T> T load(Class<T> entityClass, Serializable id) {
		final T entity = getHibernateTemplate().load(entityClass, id);
		return entity;
	}

	@Transactional(readOnly = false)
	@CommitAfter
	public void persist(Object entity) {
		getHibernateTemplate().saveOrUpdate(entity);
	}

	@Transactional
	public void persist(Object[] entities) {
		for (Object entitie : entities) {
			persist(entitie);
		}
	}

	@Transactional(readOnly = true)
	public void refresh(Object entity) {
		getHibernateTemplate().refresh(entity);
	}

	public User findUniqueByUserName(String username) throws TooManyRowsException{
		try {
			User found = (User)this.getSessionFactory().getCurrentSession()
				.createQuery("from User where userName= :username ")
				.setString("username", username)
				.uniqueResult();
			return found;
		} catch (NonUniqueResultException e) {
			throw new TooManyRowsException("Hay más de un usuario con ese username");
		}
	}

	public User findUniqueByEmail(String email) throws TooManyRowsException {
		List<Object> users = this.find("from User where email='" + email + "'");
		if ( users.size() > 1) throw new TooManyRowsException("Hay más de un usuario con ese email");
		return users.isEmpty() ? null : (User) users.get(0);
	}

	public User findUserByID(Integer id) throws TooManyRowsException {
		List<Object> users = this.find("from User where id='" + id + "'");
		if ( users.size() > 1) throw new TooManyRowsException("Hay más de un usuario con ese id(??)");
		return users.isEmpty() ? null : (User) users.get(0);
	}
}
