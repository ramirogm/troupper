package com.urbawave.pages;

import java.util.List;

import org.apache.tapestry5.ioc.annotations.Inject;

import com.urbawave.annotations.AnonymousAccess;
import com.urbawave.domain.Profile;
import com.urbawave.domain.User;
import com.urbawave.persistence.dao.HibernateDAO;

@AnonymousAccess
public class Debug {

	@Inject
	private HibernateDAO userDAO;

	/**
	 * @return the userList
	 */
	public List<User> getUserList() {
		return userDAO.find(User.class);
	}

	/**
	 * @return the userList
	 */
	public List<Profile> getProfilesList() {
		return userDAO.find(Profile.class);
	}
}
