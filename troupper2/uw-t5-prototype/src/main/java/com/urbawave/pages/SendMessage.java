package com.urbawave.pages;

import org.apache.log4j.Logger;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

import com.urbawave.annotations.AnonymousAccess;
import com.urbawave.domain.User;
import com.urbawave.persistence.dao.HibernateDAO;
import com.urbawave.services.Authenticator;

@AnonymousAccess
public class SendMessage {

    @Inject
    private Authenticator authenticator;

	@InjectPage
	private Result resultado;
	
    @Inject
    private HibernateDAO dao;

    @Persist
    private int userId;
    
	@SessionState
    @Property
    private User user;

    @Property
    private User recipient;
    
    @Component
    private Form messageForm;

    @Property
    private String texto;

    @Inject
    private MailSender mailSender;

    @Inject
    private SimpleMailMessage mailMessage;

    //@Property
    private String error;

    private Logger logger = Logger.getLogger(SendMessage.class);

    
    public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public void onActivate() {
		this.recipient = this.dao.load(User.class, this.userId);
		int id = this.authenticator.getLoggedUser().getId();
		this.user = this.dao.load(com.urbawave.domain.User.class, id);

	}

	public Object onSuccess() {

		try {
			String[] bccs = { "ramirogm@gmail.com", "betianad@gmail.com"};
			this.mailMessage.setBcc(bccs);
			this.mailMessage.setFrom("Troupper <noreply@troupper.com>");
			this.mailMessage.setSubject("Message from " + user.getFullName() + " at troupper");
			String mailText = "Troupper user " + user.getFullName() + 
			" sent you the following message: " + this.texto; 
			this.mailMessage.setText(mailText);
			this.mailMessage.setTo(this.recipient.getEmail());
			this.mailSender.send(this.mailMessage);
			this.resultado.setMensaje("Message successfully sent");
		} catch (Exception e) {
			e.printStackTrace();
			setError(e.getMessage());
			logger.error("Couldn't send the email: ", e);
			this.resultado.setMensaje(error);
		}
		return this.resultado;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	
	
}
