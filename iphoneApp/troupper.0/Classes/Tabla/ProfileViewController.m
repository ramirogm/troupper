//
//  SurfaceViewController.m
//  KClark
//
//  Created by Betiana Darderes on 7/21/11.
//

#import "ProfileViewController.h"
#import "User.h"
#import "SendMessageController.h"


@implementation ProfileViewController

@synthesize imageView;
//@synthesize name;
@synthesize bio;
@synthesize user;
@synthesize navController;



// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

- (id)initUser:(User *) aUser navController: aNavController{
 self = [super initWithNibName:@"profile" bundle: nil];
 if (self) {
   self.user = aUser;
	 navController = aNavController;
 }
 return self;
 }
 	
	
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)viewWillAppear: (BOOL)animated {
	
	
	
	
	[super viewWillAppear:animated];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	
	self.navigationItem.hidesBackButton = NO;
	self.navigationItem.title = [user userName];
	
	
			UIBarButtonItem * button1 = [[UIBarButtonItem alloc] 
									initWithTitle: @"Send Message" 
									style:UIBarButtonItemStylePlain 
									target:self 
										action:@selector(sendMessage:)];
		
		//self.navigationItem.rightBarButtonItem = button1;
	[button1 release];
	
	UIBarButtonItem * button2 = [[UIBarButtonItem alloc] 
								initWithTitle: @"Poke" 
								style:UIBarButtonItemStylePlain 
								target:self 
								action:@selector(poke:)];
	
	//self.navigationItem.rightBarButtonItem = button2;
	
	
	
    [button2 release];
		
	
	//[self.name setText: [user userName]];
	
	//Creates the surface image
	//NSString *path = [[NSBundle mainBundle] pathForResource:[user photo] ofType:@"png"];
	UIImage *theImage = [user fullImage];
	
	UIImage *cropImage = [theImage imageByScalingAndCroppingForSize: self.imageView.frame.size];
	[self.imageView setImage: cropImage];
	
	[self.bio setText: [user bio]];
	
	[super viewDidLoad];
	
}




- (IBAction)sendMessage:(id)sender {
	
	SendMessageController * controller = [[SendMessageController alloc] initUser: user];
	[navController pushViewController:controller animated:YES];
	[controller release];
}


- (IBAction)poke:(id)sender {
}


- (void)dealloc


{	//[navController release];
	[imageView release];
	[bio release];
	//[user release];
	[super dealloc];
}




@end
