//
//  DataSourceProtocol.h
//  KClark
//
//  Created by Betiana Darderes on 7/21/11.
//

#import <UIKit/UIKit.h>
# import "User.h"



@protocol DataSourceProtocol <NSObject>

@required

// these properties are used by the view controller
// for the navigation and tab bar
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *navigationBarName;


// this property determines the style of table view displayed
@property (readonly) UITableViewStyle tableViewStyle;


// provides a standardized means of asking for the element at the specific
// index path, regardless of the sorting or display technique for the specific
// datasource
- (User *) userForIndexPath:(NSIndexPath *)indexPath;
- (BOOL)showDisclosureIcon;


@optional

// this optional protocol allows us to send the datasource this message, since it has the 
// required information
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section;


@end
