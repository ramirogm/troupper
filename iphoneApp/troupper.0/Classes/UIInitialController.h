//
//  UIInitialController.h
//  troupper.0
//
//  Created by Betiana Darderes on 5/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIInitialController : UIViewController {
	
UINavigationController *navController;

}

@property (nonatomic, retain) UINavigationController *navController; 

- (IBAction)login:(id)sender;
- (IBAction)signup:(id)sender;

@end
