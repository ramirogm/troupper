package com.urbawave.pages;

import java.util.List;

import org.apache.tapestry5.ioc.annotations.Inject;

import com.urbawave.annotations.AnonymousAccess;
import com.urbawave.domain.Profile;
import com.urbawave.domain.Snapshot;
import com.urbawave.domain.User;
import com.urbawave.persistence.dao.HibernateDAO;

@AnonymousAccess
public class Debug {

    @Inject
    private HibernateDAO genericDAO;

    /**
     * @return the userList
     */
    public List<Profile> getProfilesList() {
	return this.genericDAO.find(Profile.class);
    }

    public List<Snapshot> getSnapshotList() {
	return this.genericDAO.find(Snapshot.class);
    }

    /**
     * @return the userList
     */
    public List<User> getUserList() {
	return this.genericDAO.find(User.class);
    }
}
