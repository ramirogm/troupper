//
//  TrouperService.m
//  troupper.0
//
//  Created by Betiana Darderes on 1/28/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "TrouperService.h"


@implementation TrouperService

@synthesize delegate;

UIActivityIndicatorView* activityIndicator;
//NSString * serviceString = @"http://192.168.41.100:8081/uw-ws/uwservices";
//NSString * serviceString = @"http://servicios.troupper.com/uw-ws/uwservices";
NSString * serviceString = @"http://192.168.41.105:8080/uw-ws/uwservices";
NSString * sanpshotString = @"users/savesnapshot/json";
NSString * profileString = @"users/profiles";
NSString * fullString = @"";
NSString *formString;


- (id)initWith:(UIActivityIndicatorView *)anActivityIndicator delegate: (id <ServiceDelegate>) aDelegate{
 self = [super init];
 if (self) {
	 activityIndicator =[anActivityIndicator retain];
	 delegate = aDelegate;
 }
 return self;
 }
 
- (void)post: (NSString*) message from:(int) fromId to: (int)toId {	

// Para mandar un POST

fullString  = [NSString stringWithFormat:@"%@/users/sendmessage/form", serviceString];
fullString = [fullString stringByAddingPercentEscapesUsingEncoding: NSASCIIStringEncoding];
NSURL *url = [NSURL URLWithString:fullString];

NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
													   cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];

NSString *formString  = [NSString stringWithFormat:@"from=%i&to=%i&text=%@", fromId, toId, message];
	NSLog(formString);
//NSString *formString = @"from=FROM&to=TO&text=MENSAJE";

NSData *requestData = [NSData dataWithBytes:[formString UTF8String] length:[formString length]];

[request setHTTPMethod:@"POST"];
[request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
[request setHTTPBody: requestData];

//[request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//[request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

NSURLConnection *connection = [[NSURLConnection alloc]initWithRequest:request delegate:self];
// de acá en adelante no sé que hace
if (connection) {
	responseData = [[NSMutableData data] retain];
}
}

- (void)startWithString: (NSString*) anActionstring {	
	

	
	fullString  = [NSString stringWithFormat:@"%@/%@", serviceString, anActionstring];
	fullString = [fullString stringByAddingPercentEscapesUsingEncoding: NSASCIIStringEncoding];
	
	responseData = [[NSMutableData data] retain];		
	NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:fullString]];
    
    NSLog(fullString);
	

    
	[[NSURLConnection alloc] initWithRequest:request delegate:self];
	
	
	
	//if (activityIndicator)[activityIndicator startAnimating];
}




- (void)postEditedProfile: (NSString*) jasonString profileId: (int) profileId {	
	
	
	
	fullString  = [NSString stringWithFormat:@"%@/%@/%d/edit", serviceString, profileString, profileId];
	
	NSLog(fullString);
	
	fullString = [fullString stringByAddingPercentEscapesUsingEncoding: NSASCIIStringEncoding];
	
	
	// Para mandar un POST
	
	NSURL *url = [NSURL URLWithString:fullString];
	
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
														   cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
	
	
	
	NSData *requestData = [NSData dataWithBytes:[jasonString UTF8String] length:[jasonString length]];
	
	[request setHTTPMethod:@"POST"];
	[request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
	[request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	[request setHTTPBody: requestData];
	
	NSURLConnection *connection = [[NSURLConnection alloc]initWithRequest:request delegate:self];
	// de acá en adelante no sé que hace
	if (connection) {
		responseData = [[NSMutableData data] retain];	
	}
	
		

    
    NSLog(fullString);
	
	
	
	//if (activityIndicator)[activityIndicator startAnimating];
}


- (void)postSnapshot: (NSString*) jasonString {	
	
	
	
	fullString  = [NSString stringWithFormat:@"%@/%@", serviceString, sanpshotString];
	fullString = [fullString stringByAddingPercentEscapesUsingEncoding: NSASCIIStringEncoding];
	
	// Para mandar un POST

	NSURL *url = [NSURL URLWithString:fullString];
	
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
														   cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
	
	
	
	NSData *requestData = [NSData dataWithBytes:[jasonString UTF8String] length:[jasonString length]];
	
	[request setHTTPMethod:@"POST"];
	[request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
	[request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	[request setHTTPBody: requestData];
	
	NSURLConnection *connection = [[NSURLConnection alloc]initWithRequest:request delegate:self];
	// de acá en adelante no sé que hace
	if (connection) {
		responseData = [[NSMutableData data] retain];
	}
	


    
    NSLog(fullString);
	
	
  
	
	//if (activityIndicator)[activityIndicator startAnimating];
}

- (UIImage*)imageForProfile:(NSString*) anImageString {
	
	fullString  = [NSString stringWithFormat:@"%@/%@", serviceString, anImageString];
	
	fullString = [fullString stringByAddingPercentEscapesUsingEncoding: NSASCIIStringEncoding];
	
	NSData* theData = [NSData dataWithContentsOfURL:[NSURL URLWithString:fullString]];
	UIImage *img = [UIImage imageWithData:theData];
	
	return img;
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	NSLog(@"%@",[NSString stringWithFormat:@"Connection failed: %@", [error description]]);
	
	if (activityIndicator) {
	
	[activityIndicator setHidden:  YES];
	[activityIndicator stopAnimating];
	}
	UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Connectio Failed" message: [error description] 
													delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] autorelease];
	
	[alert show];
    
	
	
 }

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {		
	[connection release];
	
	//NSLog(fullString);
	if (delegate) {
		[delegate didFinishLoadingData:responseData string: fullString];
	}
	
	
	//if (activityIndicator) [activityIndicator stopAnimating];
}
	
	
- (void)dealloc {
	[activityIndicator release];
	 [delegate release];	
	[responseData release];
    [super dealloc];
}



@end
