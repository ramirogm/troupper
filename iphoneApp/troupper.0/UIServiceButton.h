//
//  UIServiceButton.h
//  troupper.0
//
//  Created by Betiana Darderes on 1/22/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Service.h"


@interface UIServiceButton : UIButton {
	
	Service * service;

}

@property (nonatomic, retain) Service * service;

@end
