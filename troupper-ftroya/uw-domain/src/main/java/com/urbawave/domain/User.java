/**
 * 
 */
package com.urbawave.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * @author ftroya
 * 
 */
@Entity
public class User {

	@Id
	@GeneratedValue
	private Integer id;
	String userName;
	String fullName;
	String password;
	String email;
	boolean letOthersFindMeByEmailAddress;
	boolean sendMeUpdates;
	@OneToMany(mappedBy="user", fetch=FetchType.EAGER)
	List<Profile> profiles = new ArrayList<Profile>();

	
	
	/**
	 * @return the profiles
	 */
	public List<Profile> getProfiles() {
		return profiles;
	}
	
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * @param fullName
	 *            the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the letOthersFindMeByEmailAddress
	 */
	public boolean isLetOthersFindMeByEmailAddress() {
		return letOthersFindMeByEmailAddress;
	}

	/**
	 * @param letOthersFindMeByEmailAddress
	 *            the letOthersFindMeByEmailAddress to set
	 */
	public void setLetOthersFindMeByEmailAddress(boolean letOthersFindMeByEmailAddress) {
		this.letOthersFindMeByEmailAddress = letOthersFindMeByEmailAddress;
	}

	/**
	 * @return the sendMeUpdates
	 */
	public boolean isSendMeUpdates() {
		return sendMeUpdates;
	}

	/**
	 * @param sendMeUpdates
	 *            the sendMeUpdates to set
	 */
	public void setSendMeUpdates(boolean sendMeUpdates) {
		this.sendMeUpdates = sendMeUpdates;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	

	/**
	 * @param profiles
	 *            the profiles to set
	 */
	public void setProfiles(List<Profile> profiles) {
		this.profiles = profiles;
	}

}
