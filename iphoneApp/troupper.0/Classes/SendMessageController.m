    //
//  SendMessageController.m
//  troupper.0
//
//  Created by Betiana Darderes on 1/28/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "SendMessageController.h"
#import "Components.h"
#import <MessageUI/MessageUI.h>
#import "TrouperService.h"
#import "ServiceResponseReader.h"




@implementation SendMessageController

@synthesize meesageTextView;
@synthesize toUser;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

- (id)initUser: (User*) aUser{
    self = [super initWithNibName:@"SendMessage" bundle:nil];
    if (self) {
        toUser = aUser;
    }
    return self;
}

- (IBAction)send:(id)sender{
	
	User * publicProfile = [[Components sharedPlacesInstance] publicUserProfile];
	
	NSString * subjectString = [NSString stringWithFormat: @"Troupper Message from: %@", [publicProfile userName]];
	
	TrouperService * service = [[TrouperService alloc] initWith: nil delegate: self];
	
	[service post: meesageTextView.text from: [publicProfile tag] to: [toUser tag]];
	
	
	}


- (void)didFinishLoadingData:(NSData *)responseData string: (NSString *) connString{	
	
	ServiceResponseReader * reader = [[ServiceResponseReader alloc] initWith:nil];
	NSString * responseString = [reader resultForm: responseData];
	
	NSLog(responseString);

	
	
	UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Message Sent" message: @"Message Sent Successfully" 
													delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] autorelease];
	[alert show];
	[meesageTextView setText: @""];
	
	/*

	ServiceResponseReader * reader = [[ServiceResponseReader alloc] initWith:nil];
	
	NSString* errorString = [reader resultForm: responseData];
	
	NSRange textRange;
	textRange =[errorString rangeOfString:@"OK"];
	NSLog(errorString);
	
	
	
	if (textRange.location != NSNotFound) {
		UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Message Sent" message: @"Message Sent Successfully" 
														delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] autorelease];
		[alert show];
		[meesageTextView setText: @""];
	} else{
	
		UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Connection Error" message: @"Connection error, try again." 
														delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] autorelease];
		[alert show];
	}
	 */
	
}

-(void) mailComposeController: (MFMailComposeViewController*) controller didFinishWithResult: (MFMailComposeResult) 
result error:(NSError*) error {

	[controller dismissModalViewControllerAnimated: YES];
}

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
	if ( [ text isEqualToString: @"\n" ] ) {
		[ textView resignFirstResponder ];
		return NO;
	}
	return YES;
}

- (void)viewDidLoad {
	
	//self.navigationItem.title = [NSString stringWithFormat: @"To %@", [toUser userName]];
	self.navigationItem.title = @"Message";
	meesageTextView.delegate = self;
	
	    [super viewDidLoad];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
