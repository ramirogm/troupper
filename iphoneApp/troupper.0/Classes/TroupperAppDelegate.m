//
//  TroupperAppDelegate.m
//  troupper.0
//
//  Created by Betiana Darderes on 4/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TroupperAppDelegate.h"
#import "UIInitialController.h"
#import "UILoginController.h"
#import "Components.h"
#import "User.h"
#import "TrouperService.h"

@implementation TroupperAppDelegate

@synthesize window;
@synthesize initController;
@synthesize tabBarController;
@synthesize  iniNavController;



#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    
    // Override point for customization after application launch.
	
	//self.initController =
	//[[UIInitialController alloc] init] ;
	
    
	
    //[window addSubview:[initController view]];
	
	//[initController release];
	
	UILoginController *loginController = [[UILoginController alloc] initWithNibName:@"login" bundle:nil]; 
	
	//TroupperAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    //[self.view addSubview: delegate.iniNavController.view];
	
	[iniNavController pushViewController:loginController animated:YES];
	[loginController release];
	
    [window addSubview: iniNavController.view];
    [window makeKeyAndVisible];

	

    
    return YES;
}


- (void)startTabBarController {    
    
       
    [window addSubview:[tabBarController view]];
	
    [window makeKeyAndVisible];

}

- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
	
		
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}


- (void)applicationWillTerminate:(UIApplication *)application {
	}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}


- (void)dealloc {
	
	TrouperService* service = [[TrouperService alloc] initWith: nil delegate: nil];
	User* user = [[Components sharedPlacesInstance] loggedUser];
	
	User* profile = [[Components sharedPlacesInstance] publicUserProfile];
	NSLog(@"users/notifications/%@/%@", [user userName], [profile userName]);
    NSString* offlineServiceString = [NSString stringWithFormat: @"users/notifications/%@/%@/offline", [user userName], [profile userName] ];
	[service startWithString:offlineServiceString];
	[service release];
	
		
    [window release];
	[initController release];
    [super dealloc];
}


@end
