//
//  ProductController.m
//  KClark
//
//  Created by Betiana Darderes on 7/21/11.
//

#import "ProductController.h"
#import "Technique.h"


@implementation ProductController
@synthesize imageView;
@synthesize technique;
@synthesize scrollView;
@synthesize navController;


- (id)initWithTechnique:(Technique *) aTechnique {
    self = [super initWithNibName:@"Product" bundle:nil];
    if (self) {
		[self setTechnique: aTechnique];
		}
    return self;
}



// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)viewWillAppear: (BOOL)animated {

	[self.imageView setImage: [UIImage imageNamed:technique.productImageName]];
	[super viewWillAppear:animated];
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	
	scrollView.frame = CGRectMake(0,0,320,400);
	//imageView.center = scrollView.center;
	
	[scrollView setBackgroundColor:[UIColor clearColor]];
	 scrollView.pagingEnabled = YES;
	 scrollView.showsVerticalScrollIndicator = YES;
	[scrollView setContentSize: CGSizeMake(320, 500)];
    [super viewDidLoad];
}

- (void)dealloc {
	
	[navController release];
	[imageView release];
	[technique release];
	[scrollView release];
    [super dealloc];
}


@end
