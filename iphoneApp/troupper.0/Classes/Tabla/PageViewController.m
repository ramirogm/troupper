//
//  PageViewController.m
//  KClark
//
//  Created by Betiana Darderes on 7/21/11.
//

#import "PageViewController.h"
#import "KClarkAppDelegate.h"
#import "ProductController.h"
#import "Technique.h"


@implementation PageViewController
@synthesize imageView;
@synthesize technique;

const CGFloat height = 373.0;// Se resto 87 porque es lo que mide el let's clean.
const CGFloat width	= 320.0;


// Load the view nib and initialize the pageNumber ivar.
- (id)initWithTechnique:(Technique *) aTechnique page: (int) page {

    self = [self init];
		
		if (self) {
				
		[self setTechnique: aTechnique];
		
		UIImage *bgImage = [UIImage imageNamed: technique.imageName];
		
		imageView = [[UIImageView alloc] initWithImage:bgImage]; 
	    imageView.userInteractionEnabled= YES;
		imageView.multipleTouchEnabled = YES;
		
		
		UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pageSelected:)];
		[imageView addGestureRecognizer: singleTap];
		[singleTap release];
		
		CGRect rect = self.view.frame;
		rect.size.height =  297; //380; //height// se le resta tambien 87;
		rect.size.width = width;
		rect.origin = CGPointMake(width * page, 0);
		imageView.frame = rect;
		imageView.opaque = NO;
	
	
    }
    return self;
}


- (void)dealloc {
   
	[imageView release];
	[technique release];
    [super dealloc];
}


- (IBAction)pageSelected:(id)sender {
	
    KClarkAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	ProductController *controller = [[ProductController alloc] initWithTechnique: technique]; 
    [delegate.navController pushViewController: controller animated:YES];
	[controller release];
	
	
}




@end
