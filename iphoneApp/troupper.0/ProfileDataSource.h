//
//  ProfileDataSource.h
//  troupper.0
//
//  Created by Betiana Darderes on 10/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "DataSourceProtocol.h"

@interface ProfileDataSource : NSObject <UITableViewDataSource,DataSourceProtocol> {
	
	NSString *name;
	NSString *navigationBarName;
}

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *navigationBarName;

@end