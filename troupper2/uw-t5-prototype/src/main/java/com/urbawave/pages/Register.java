/**
 * 
 */
package com.urbawave.pages;

import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.PasswordField;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

import com.urbawave.annotations.AnonymousAccess;
import com.urbawave.domain.User;
import com.urbawave.persistence.dao.HibernateDAO;
import com.urbawave.persistence.dao.TooManyRowsException;

/**
 * @author ftroya
 * 
 */
@AnonymousAccess
public class Register {

	private static Logger logger = Logger.getLogger(Register.class);

    @Property
    @SessionState
    private User user = new User();

    @Property
    @SessionState
    private com.urbawave.domain.Profile profile = new com.urbawave.domain.Profile();

    @Inject
    private HibernateDAO userDAO;

    @Inject
    private HibernateDAO profileDAO;
    
	@Inject
	private HibernateDAO hibernateDAO;

    @Inject
    private MailSender mailSender;

    @Inject
    private SimpleMailMessage mailMessage;

    @Component
    private Form register;

    @Inject
    private Messages messages;
    
    @Persist
	@Property
	private String fullName;
    
    @Persist
	@Property
	private String userName;
    
	@Component(id = "userName")
	private TextField userNameField;

    
    /**
     * Valido que no haya usuario con el mismo username
     */
    public void onValidate()
    {
        if (yaExisteUsuario()) {
            register.recordError(userNameField, "Name already taken. Please choose another one.");
        }
        try {
			if (yaExisteMail()){
			    register.recordError(userNameField, "email already used in another account. Please choose another one.");
			}
		} catch (TooManyRowsException e) {
			register.recordError(userNameField, "email already used in another account. Please choose another one.");
		}
    }

    private boolean yaExisteMail() throws TooManyRowsException {
		return hibernateDAO.findUniqueByEmail(this.user.getEmail())!= null;
	}

    private boolean yaExisteUsuario() {
		return hibernateDAO.findByUserName(this.user.getUserName())!= null;
	}

	public Object onSuccess() {
	
		// Create a default profile
		EditProfile.setDefaultImageToProfile(profile);
		this.profile.setUser(user);
		this.profile.setActive(Boolean.TRUE);
		this.profile.setEmail(user.getEmail());
		this.profile.setIsPublic(true);
		this.profile.setName(user.getUserName());
		
		this.user.getProfiles().add(this.profile);
		this.userDAO.persist(this.user);
		this.profileDAO.persist(this.profile);
		this.mailMessage.setText("This is your username: " + this.user.getUserName() + " and password: "
			+ this.user.getPassword());
		this.mailMessage.setSubject("Your troupper login information");
		this.mailMessage.setTo(this.user.getEmail());
		try {
			this.mailSender.send(this.mailMessage);
		} catch (MailException e) {
			logger.log(Priority.ERROR, "Error al enviar el mensaje con los datos del user", e);
		}
		//FIXME Acá debería ir directamente a Profiles, prelogueando al usuario
		return Index.class;
    }

}
