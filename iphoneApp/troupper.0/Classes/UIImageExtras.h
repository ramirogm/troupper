//
//  UIImageExtras.h
//  CTM Photo-Video
//
//  Created by Betiana Darderes on 10/21/10.
//  Copyright 2010. All rights reserved.

@interface UIImage (Extras)

- (UIImage*)scaleToSize:(CGSize)size;
- (UIImage*)imageByScalingAndCroppingForSize:(CGSize)targetSize;
- (UIImage*)overlayWith:(UIImage*)overlayImage withOrient:(UIImageOrientation) orient;
- (UIImage*)rotate:(UIImageOrientation)orient;
- (BOOL)isLandscape;

@end

