//
//  Technique.h
//  KClark
//
//  Created by Betiana Darderes on 7/21/11.
//

#import <Foundation/Foundation.h>


@interface Technique : NSObject {

	NSString *name;
	NSString *imageName;
	NSString *productImageName;
	
}

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *imageName;
@property (nonatomic, retain) NSString *productImageName;


@end
