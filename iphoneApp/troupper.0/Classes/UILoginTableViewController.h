//
//  UILoginTableViewController.h
//  troupper.0
//
//  Created by Betiana Darderes on 5/4/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>



@class EditableDetailCell;
@class User;


//  Constants representing the book's fields.
//
enum {
    UserName,
    Password,
    
};


typedef NSUInteger BookAttribute;



@interface UILoginTableViewController : UITableViewController <UITextFieldDelegate> {

	User *user;
    //MyListController *_listController;
    EditableDetailCell * userCell;
    EditableDetailCell * passwordCell;
  
}

@property (nonatomic, retain) User *user;

//  Watch out for retain cycles (objects that retain each other can never
//  be deallocated). If the list controller were to retain the detail controller,
//  we should change 'retain' to 'assign' below to avoid a cycle. (Note that in
//  that case, dealloc shouldn't release the list controller.)
//
//@property (nonatomic, retain) MyListController *listController;

@property (nonatomic, retain) EditableDetailCell *userCell;
@property (nonatomic, retain) EditableDetailCell *passwordCell;

- (BOOL)isModal;

- (EditableDetailCell *)newDetailCellWithTag:(NSInteger)tag;

//  Action Methods
//
- (IBAction)login:(id)sender;
- (IBAction)cancel:(id)sender;

@end

