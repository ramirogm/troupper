/**
 * 
 */
package com.urbawave.pages;

import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

import com.urbawave.annotations.AnonymousAccess;
import com.urbawave.domain.User;
import com.urbawave.persistence.dao.HibernateDAO;
import com.urbawave.services.AppConstants;

/**
 * @author ftroya
 * 
 */
@AnonymousAccess
public class Register {

	@Property
	@SessionState
	private User user = new User();
	
	@Property
	@SessionState
	private com.urbawave.domain.Profile profile = new com.urbawave.domain.Profile();

	@Inject
	private HibernateDAO userDAO;
	
	@Inject
	private MailSender mailSender;
	
	@Inject
	private SimpleMailMessage mailMessage;

	@Component
	private Form register;
	
	 @Inject
	  private Messages messages;

	Object onSuccess() {
		profile.setName("Default profile");
		profile.setImagePath(messages.get(AppConstants.DEFAULT_IMG_PATH)+messages.get(AppConstants.DEFAULT_IMG_FILE_NAME));
		profile.setUser(user);
		user.getProfiles().add(profile);
		userDAO.persist(user);
		userDAO.persist(profile);
		mailMessage.setText("Your username: " + user.getUserName() + "and password: " + user.getPassword());
		mailMessage.setTo(user.getEmail());
		mailSender.send(mailMessage);
		return Profile.class;
	}

}
