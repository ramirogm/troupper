//
//  Components.h
//  KClark
//
//  Created by Betiana Darderes on 7/21/11.
//

#import <Foundation/Foundation.h>
#import "Service.h"
#import "User.h"

@interface Components : NSObject {

	NSMutableArray *connUserArray;
	NSMutableArray *placesDictionary;
	NSMutableArray *services;
	NSMutableDictionary *profileImages;
	User * loggedUser;
	
	
	
}

@property (nonatomic,retain) NSMutableArray *connUserArray;
@property (nonatomic,retain) NSMutableArray *profileUserArray;
@property (nonatomic,retain) NSMutableArray *services;
@property (nonatomic,retain) NSMutableDictionary *profileImages;
@property (nonatomic,retain) User * loggedUser;


- (NSArray *)connUsers;
- (NSArray *)userProfiles; 
- (NSArray *)services; 
- (NSMutableDictionary*) asDictionary: (NSMutableArray*) array;
- (NSMutableDictionary *)servicesDictionary;
- (Service*)serviceForName: (NSString*) name;
- (Service*)serviceForTag: (int) tag;
- (User*)connUserForTag: (int) tag;
- (User*)userProfileForTag: (int) tag;
- (User*)publicUserProfile;
- (void) addImage: (UIImage*) image forUser: (User*) newUser;




@end
