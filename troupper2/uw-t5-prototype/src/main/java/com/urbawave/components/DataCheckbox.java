/**
 * 
 */
package com.urbawave.components;

import org.apache.tapestry5.MarkupWriter;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.corelib.components.Checkbox;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Request;

/**
 * @author ftroya
 *
 * <t:datacheckbox t:value="checked" t:data="data" />
 */
public class DataCheckbox extends Checkbox {

        @Parameter
        private String _data;
        
        @Inject
        private Request _request;
        protected void beforeRenderTemplate(MarkupWriter writer) {
                writer.attributes("value", _data);
        }
        
        @Override
        protected void processSubmission(String elementName) {
                super.processSubmission(elementName);
                String value = _request.getParameter(elementName);
                if ( value != null ) {
                        _data = value;
                }
        }

		public String getData() {
			return _data;
		}
}