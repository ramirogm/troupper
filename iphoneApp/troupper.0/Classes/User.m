//
//  User.m
//  troupper.0
//
//  Created by Betiana Darderes on 5/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "User.h"


@implementation User

@synthesize userName;
@synthesize firstName;
@synthesize lastName;
@synthesize phone;
@synthesize password;
@synthesize gender;
@synthesize birthday;
@synthesize photo;
@synthesize email;
@synthesize bio;
@synthesize interests;
@synthesize tag;
@synthesize isPublished;
@synthesize newCellImage;
@synthesize downloadedImage;


//@dynamic author;



- (void)dealloc{
	
	[interests release];

	[super dealloc];
}


- (UIImage*) maskImage:(UIImage *)image withMask:(UIImage *)maskImage {
	
	CGImageRef maskRef = maskImage.CGImage; 
	
	CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
										CGImageGetHeight(maskRef),
										CGImageGetBitsPerComponent(maskRef),
										CGImageGetBitsPerPixel(maskRef),
										CGImageGetBytesPerRow(maskRef),
										CGImageGetDataProvider(maskRef), NULL, false);
	
	CGImageRef masked = CGImageCreateWithMask([image CGImage], mask);
	return [UIImage imageWithCGImage:masked];
	
}

- (UIImage*) thumbnail{
	
	if (newCellImage) {
		
		croppedImage = [newCellImage imageByScalingAndCroppingForSize: CGSizeMake(65, 60)];
		
		
	}
	else {
		
		if (downloadedImage)
		
		{
		
		croppedImage = [downloadedImage imageByScalingAndCroppingForSize: CGSizeMake(65, 60)];
	
        }
		else {
			NSString *path = [[NSBundle mainBundle] pathForResource: photo ofType:@"png"];
			UIImage *theImage = [UIImage imageWithContentsOfFile:path];
			croppedImage = [theImage imageByScalingAndCroppingForSize: CGSizeMake(65, 60)];
		}

		
}
	
	NSString *path = [[NSBundle mainBundle] pathForResource: @"mascara" ofType:@"png"];
	

	UIImage *mask = [[UIImage imageWithContentsOfFile:path] imageByScalingAndCroppingForSize: CGSizeMake(65, 60)];
	
	
	croppedImage = [croppedImage maskImage:mask];
	
	 return croppedImage;
}
	
- (UIImage*)fullImage{
	
	if (newCellImage) return newCellImage;

	else {
		if (downloadedImage) return downloadedImage;
		 else {
			 

			
		    NSString *path = [[NSBundle mainBundle] pathForResource: photo ofType:@"png"];
			UIImage *theImage = [UIImage imageWithContentsOfFile:path];
			return theImage;
		 }
	}
}

- (BOOL) empty: (NSString *) aString{
	
	return ([[aString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]length] == 0);
}

- (NSString*) jsonRepresentation{
	
	//{"id":13,"name":"Bart Simpson","imagePath":"bart.jpeg","image":
	//"krusty","active":true,"isPublic":true,"bio":"Principal","email":"bart.simpson@gmail.com"}
	
	//[[self fullImage] arrayOfBytesPNG]
	
	NSData *pixelData = UIImageJPEGRepresentation([self fullImage],1);
	//NSData* pixelData = (NSData*) CGDataProviderCopyData(CGImageGetDataProvider(.CGImage));
    unsigned char* pixelBytes = (unsigned char *)[pixelData bytes];

    
    // Redondeo porque si no la división me toma el floor
//    for(int i = 0; i < [pixelData length]; i += IMAGE_BYTES_PER_PIXEL) {

	// No anda xq no son compatibles los tipos.
	//NSArray *a2 = [NSArray arrayWithArray:pixelBytes];
	

	NSMutableArray *imageArray = [[NSMutableArray alloc]
									initWithCapacity: [pixelData length]];
	
	NSNumber *number;
	for (int i = 0; i < [pixelData length]; i++)
	{
		number = [NSNumber numberWithUnsignedChar: pixelBytes[i]];
		[imageArray addObject: number];
	}
	
	
	// NSData* data = UIImagePNGRepresentation([self fullImage]);
	
	//NSString *byteArray  = [data base64Encoding];
	

	
	NSDictionary* snapshotDic = [NSDictionary dictionaryWithObjectsAndKeys:
								 [NSNumber numberWithInt:tag], @"id",
								 userName, @"name",
								 [NSString stringWithFormat: @"%@.jpeg", userName], @"imagePath",	 
								 imageArray, @"image",
								 interests, @"interests",
								 bio, @"bio",
								 email, @"email",
								 nil ];
	

	//NSLog(@"%@",[[self fullImage] arrayOfBytesPNG]);
	
	
	
	
	

	
	return[snapshotDic JSONRepresentation];

}

- (BOOL) sameInterestsThan: (User*) otherUser{
	
	//NSString *bio = @"Norman, Stanley, Fletcher";
	
	
	
	NSMutableArray *evaluatedInterests = [[NSMutableArray alloc] init];
	[evaluatedInterests addObjectsFromArray: [self interests]];
	//[evaluatedInterests addObjectsFromArray: [bio componentsSeparatedByString:@", "]];
	
	NSMutableArray *otherEvaluatedInterests = [[NSMutableArray alloc] init];
	[otherEvaluatedInterests addObjectsFromArray: [otherUser interests]];
	//[otherEvaluatedInterests addObjectsFromArray: [[otherUser bio ]componentsSeparatedByString:@", "]];
	
	
	if ( ([evaluatedInterests count]==0)||([otherEvaluatedInterests count]==0)) {
		return YES;
	}
	
	int i;
	int j;
	
	NSLog(@"interests: %@ ", otherEvaluatedInterests);
	NSLog(@"other interests: %@ ", evaluatedInterests);
	
	
	
	for (i = 0; i < [evaluatedInterests count]; i++) {
		
		
		NSString * anInterest = [[evaluatedInterests objectAtIndex:i] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		
		for (j = 0; j < [otherEvaluatedInterests count]; j++) {
			
			NSString * anOtherInterest = [[otherEvaluatedInterests objectAtIndex:j] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
			
			if ([anInterest isEqualToString: anOtherInterest] ) {
				return YES;
			}		
		}
		
			
	} 
	
	[otherEvaluatedInterests release];
	[evaluatedInterests release];
	
	return NO;

}
	

	
+ (NSArray *)keys
{
    return [NSArray arrayWithObjects:
            @"userName",
            @"password",
			@"firstName",
			@"lastName",
			@"phone",
			@"email",
			@"birthday",
			@"gender",
			@"photo",
			@"bio",
			nil];
}



+ (NSNumberFormatter *)numberFormatter
{
    static NSNumberFormatter *_formatter;
    
    if (_formatter == nil)
    {
        _formatter = [[NSNumberFormatter alloc] init];
    }
    
    return _formatter;
}

- (id)init
{
    self = [super init];
    
    //  Set these to blank string to avoid writing nil to the
    //  plist file if user doesn't set all the values.
    //
     [self setUserName:@""];
     [self setPassword:@""];
	 [self setLastName:@""];
	 [self setFirstName:@""];
	 [self setGender:@""];
	 [self setPhoto:@""];
	 [self setEmail:@""];
	 [self setPhone:@""];
	 [self setBirthday:@""];
	 [self setBio:@""];
	
	isPublished= NO;
	photo = @"logo troupper chico";
	interests = [[NSMutableArray alloc]init];
    
    return self;
}

- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [self init];
    
    //NSString *yearStr = [dictionary valueForKey:@"publicationYear"];
    //NSNumber *year = [[[self class] numberFormatter] numberFromString:yearStr];
    //[self setPublicationYear:year];
    
    //NSMutableDictionary *mutableDict = [dictionary mutableCopy];
    //[mutableDict removeObjectForKey:@"publicationYear"];
    
    //[self setValuesForKeysWithDictionary:mutableDict];
    
    return self;
}

//- (NSString *)author
//{
 //   return [NSString stringWithFormat:@"%@, %@", 
 //           [self lastName], 
 //           [self firstName]];
//}

@end

