    //
//  SendSnapshotController.m
//  troupper.0
//
//  Created by Betiana Darderes on 1/28/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "SendSnapshotController.h"
#import "User.h"
#import "TrouperService.h"
#import "ServiceResponseReader.h"
#import "Components.h"

@implementation SendSnapshotController

@synthesize tag;
@synthesize isPrivate;

NSMutableArray* selectedProfiles;

- (id)initWithProfiles: (NSMutableArray*) profiles {
    self = [super initWithNibName:@"SendSnapshot" bundle:nil];
    if (self) {
        selectedProfiles = [profiles retain];
    }
    return self;
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
	if ( [ text isEqualToString: @"\n" ] ) {
		[ textView resignFirstResponder ];
		return NO;
	}
	return YES;
}

- (void)viewDidLoad {
	
	self.navigationItem.title = @"Snapshot";
	tag.delegate = self;
	
	[super viewDidLoad];
}


- (IBAction)send:(id)sender {


	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:@"dd-MM-yyyy hh:mm:ss"];
	
	NSString *dateString = [dateFormat stringFromDate: [NSDate date]];
	
	
	NSMutableArray* profileIds = [NSMutableArray arrayWithCapacity: [selectedProfiles count]];
	int i;
	User * aUser;
	
	for (i = 0; i < [selectedProfiles count]; i++) {
		
		aUser = [selectedProfiles objectAtIndex:i];
		[profileIds addObject: [NSNumber numberWithInt:[aUser tag]]];
	}
	
	/*
	 "name": "snapshot1", 
	 "isPrivate": "false",
	 "ownerId" : "13", 
	 "date" : "23-11-2010 10:00:00"
	 "profilesIds": [],
	 "tagIds": []
	 */
		
	int loggedUserId= [[[Components sharedPlacesInstance ]publicUserProfile]tag];
	NSDictionary* snapshotDic = [NSDictionary dictionaryWithObjectsAndKeys:
				tag.text, @"name",
				dateString, @"date",
				[NSNumber numberWithInt:loggedUserId], @"ownerId",	 
				[NSNumber numberWithBool: (!isPrivate.on)], @"isPrivate",
				[NSArray arrayWithArray: profileIds], @"profilesIds",
				nil ];
    	
	NSString * snapshotJson = [snapshotDic JSONRepresentation];
	NSLog(snapshotJson);
	
	TrouperService * service = [[TrouperService alloc] initWith: nil delegate: self];
	
	[service postSnapshot: snapshotJson];
	
}

- (void)didFinishLoadingData:(NSData *)responseData string: (NSString *) connString{	
	
	ServiceResponseReader * reader = [[ServiceResponseReader alloc] initWith:nil];
	NSString * responseString = [reader resultForm: responseData];
	
	//{"resultado": "OK"}
	
	NSRange textRange;
	textRange =[responseString rangeOfString:@"OK"];

	
	NSLog(responseString);
	
	if (textRange.location != NSNotFound) {

	{
		UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Snapshot Saved" message: @"The snapshot info is now available in your site account." 
														delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] autorelease];
		[alert show];
		
		
	}
		//analiza si hubo error de login
	}	
}
/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/



- (void)dealloc {
    [super dealloc];
}


@end
