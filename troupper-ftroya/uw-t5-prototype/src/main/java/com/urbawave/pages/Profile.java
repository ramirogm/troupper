/**
 * 
 */
package com.urbawave.pages;

import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.annotations.Inject;

import com.urbawave.domain.User;
import com.urbawave.persistence.dao.HibernateDAO;
import com.urbawave.services.Authenticator;

/**
 * @author ftroya
 *
 */
public class Profile {
	
	@Inject
	private Authenticator authenticator;

	@SessionState
	@Property
	private User user;
	
	
	@Property
	private com.urbawave.domain.Profile profile;
	
	public void onActivate(){
		user = authenticator.getLoggedUser();
	}
	
}
