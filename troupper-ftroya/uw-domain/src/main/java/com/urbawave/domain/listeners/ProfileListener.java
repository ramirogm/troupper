/**
 * 
 */
package com.urbawave.domain.listeners;

import java.util.Arrays;

import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.springframework.util.StringUtils;

import com.urbawave.domain.Profile;

/**
 * @author ftroya
 * 
 */
public class ProfileListener {

	@PrePersist
	public void prePersist(Profile profile) {
		System.out.println("EmployeeDebugListener.Persist called on employee id: " + profile.getName());
	}

	@PreUpdate
	public void populateInterests(Profile profile) {
		profile.getInterests().addAll(
				Arrays.asList(StringUtils.tokenizeToStringArray(profile.getInterestsAsString(), "|")));
	}

	@PostLoad
	public void parseInterests(Profile profile) {
		StringBuffer buffer = new StringBuffer();
		for (String interest : profile.getInterests()) {
			buffer.append(interest);
			buffer.append("|");
		}
		profile.setInterestsAsString(buffer.toString());
	}

}
