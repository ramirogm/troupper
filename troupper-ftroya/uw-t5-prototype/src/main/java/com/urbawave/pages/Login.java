/**
 * 
 */
package com.urbawave.pages;

import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.PasswordField;
import org.apache.tapestry5.ioc.annotations.Inject;

import com.urbawave.annotations.AnonymousAccess;
import com.urbawave.domain.User;
import com.urbawave.security.AuthenticationException;
import com.urbawave.services.Authenticator;

/**
 * @author ftroya
 * 
 */
@AnonymousAccess
public class Login {

	@Inject
	private Authenticator authenticator;

	private String login;

	private String pass;

	@Component(id = "password")
	private PasswordField passwordField;

	@Component
	private Form loginForm;

	@SessionState
	private User user;

	public Object onSuccess() {
		try {
			authenticator.login(login, pass);
		} catch (AuthenticationException e) {
			loginForm.recordError("Auth exception");
		}
		return user != null ? Index.class : Register.class;
	}

	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * @param login
	 *            the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * @return the pass
	 */
	public String getPass() {
		return pass;
	}

	/**
	 * @param pass
	 *            the pass to set
	 */
	public void setPass(String pass) {
		this.pass = pass;
	}

	Object onHit() {
		return Register.class;
	}

}
