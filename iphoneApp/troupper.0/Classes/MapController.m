    //
//  MapController.m
//  troupper.0
//
//  Created by Betiana Darderes on 1/12/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "MapController.h"
#import "Components.h"
#import "Service.h"
#import "UIServiceButton.h"
#import "ServiceController.h"


@implementation MapController

@synthesize mapView;
@synthesize locationManager;
@synthesize navController;

NSMutableArray* addedAnnotations;
BOOL showAll = YES;
BOOL recomended = NO;


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	
	self.locationManager = [[CLLocationManager alloc] init];
	self.locationManager.delegate = self;
	self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
	self.locationManager.distanceFilter = kCLDistanceFilterNone;
	[self.locationManager startUpdatingLocation];
	self.mapView.delegate = self;
	self.mapView.mapType = MKMapTypeHybrid;
	
	MKCoordinateSpan span;
	span.latitudeDelta = .001;
	span.longitudeDelta = .001;
	MKCoordinateRegion region;
	region.center = self.locationManager.location.coordinate;
	region.span = span;
	
	[mapView setRegion:region animated:YES];
	mapView.showsUserLocation = YES;
	
	[self addAnnotations];
	[self setNowAllButton];
	[self setRecomendedAllButton];

	/*UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
	lpgr.minimumPressDuration = 1.0;  //user must hold for 1 second
	[mapView addGestureRecognizer:lpgr];
	[lpgr release];
	*/
	
    [super viewDidLoad];
}

- (void) locationManager: (CLLocationManager *) manager 
	           didUpdateToLocation: (CLLocation *) newLocation
			fromLocation: (CLLocation*) oldLocation{

	
	
	

}


- (void) mapView: (MKMapView *) mv 
	 regionWillChangeAnimated: (BOOL) animated {
	
	
}



- (void)handleLongPress:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
	
    CGPoint touchPoint = [gestureRecognizer locationInView:mapView];    
    CLLocationCoordinate2D touchMapCoordinate = [mapView convertPoint:touchPoint toCoordinateFromView:mapView];

	NSLog(@"Lat:%f. Lng: %f", touchMapCoordinate.latitude, touchMapCoordinate.longitude);
    //add pin where user touched down...
    MKPointAnnotation *pa = [[MKPointAnnotation alloc] init];
    pa.coordinate = touchMapCoordinate;
    pa.title = @"Hello";
	pa.subtitle = @"Hello";
    [mapView addAnnotation:pa];
    [pa release];
	
    //add circle with 5km radius where user touched down...
    //MKCircle *circle = [MKCircle circleWithCenterCoordinate:touchMapCoordinate radius:5000];
    //[mapView addOverlay:circle];
}

-(MKOverlayView *)mapView:(MKMapView *)mv viewForOverlay:(id)overlay 
{
    
}

- (MKAnnotationView *)mapView:(MKMapView *)mv viewForAnnotation:(id <MKAnnotation>)annotation
{

	
	static NSString *AnnotationIdentifier = @"Annotation";
	NSLog(@"El de la annotation en map view");
	NSLog(annotation.title);
	// Tengo que descartar current location y despues el resto si
	if (![annotation isKindOfClass:[MKUserLocation class]]) {
	
    MKPinAnnotationView* pinView = (MKPinAnnotationView *)[mv dequeueReusableAnnotationViewWithIdentifier:AnnotationIdentifier];
    if (!pinView)
    {
		pinView = [[[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationIdentifier] autorelease];
		
		}
		
        pinView.annotation = annotation;
		
		UIButton * detailButton = [UIButton buttonWithType: UIButtonTypeDetailDisclosure];
		detailButton.frame = CGRectMake(0, 0, 23, 23);
		detailButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
		detailButton.contentHorizontalAlignment = UIControlContentVerticalAlignmentCenter;
		
		
		Service * service = [[Components sharedPlacesInstance] serviceForName: annotation.title];	
		detailButton.tag = [service tag];
		
		[detailButton addTarget: self action: @selector(detailButtonTapped:) forControlEvents: UIControlEventTouchUpInside];
		
        if ([service recomended]) {
			pinView.pinColor = MKPinAnnotationColorRed;
		} else
			pinView.pinColor = MKPinAnnotationColorGreen;
		
		pinView.animatesDrop = YES;
		pinView.canShowCallout = YES;
		pinView.enabled = YES;
		pinView.rightCalloutAccessoryView = detailButton;
		
		return pinView;
	}
	else {
		MKAnnotationView  *ann =  (MKAnnotationView *)[mv  dequeueReusableAnnotationViewWithIdentifier:AnnotationIdentifier];
		ann.annotation = annotation;
		return ann;
	}

}
	-(void) detailButtonTapped: (id) sender {
	
		
		NSLog(@"El tag del boton es:");
		UIButton *aButton = (UIButton *)sender; // we know the sender is a UIButton object, so cast it
	
		NSLog(@"Buton tag = %d", aButton.tag);
		
		Service * selectedService = [[Components sharedPlacesInstance] serviceForTag: aButton.tag];	
		
		ServiceController * controller = [[ServiceController alloc] initWithService: selectedService];
			
		[navController pushViewController:controller animated:YES];
		[controller release];
	
	}



- (IBAction)nowButtonTap:(id)sender{
	
	if(showAll) showAll = NO;
	   else showAll = YES;
	[self removeAllAnnotations];
	[self addAnnotations];
	[self setNowAllButton];

}


- (IBAction)recomendedButtonTap:(id)sender{
	
	if(recomended) recomended = NO;
	else recomended = YES;
	[self removeAllAnnotations];
	[self addAnnotations];
	[self setRecomendedAllButton];
	
}

-(void)removeAllAnnotations
{
	for (id annotation in [mapView annotations]) {
		NSLog(@"annotation %@", annotation);
		
		if (![annotation isKindOfClass:[MKUserLocation class]]){
			
			[mapView removeAnnotation:annotation];
		}
    }
	}
- (void)addAnnotations{

	
	
	NSArray* services = [[Components sharedPlacesInstance] services];
	Service * service;
	int i;
	
	for (i = 0; i < [services count]; i++) {
		
		service = [services objectAtIndex:i];
		
		if (( showAll)||([service now])) {
			if ((recomended)||([service recomended]))  {
			
			
			MKPointAnnotation *pa = [[MKPointAnnotation alloc] init];
			pa.title = [service name];
			pa.subtitle = [service subtitle];
			pa.coordinate = [service coordinate];
			
			
			[mapView addAnnotation:pa];
			
			[pa release];		 
		}
		
		}
	}
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL) setNowAllButton {
	
	NSString * title;
	
	if (showAll) {
		title = @"Available Now";
		
	} else {title = @"All"; }
	
	// Create cancel button
	UIBarButtonItem * button = [[UIBarButtonItem alloc] 
				  initWithTitle: title 
				  style:UIBarButtonItemStylePlain 
				  target:self 
			  action:@selector(nowButtonTap:)];
	
	self.navigationItem.rightBarButtonItem = button;
	
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL) setRecomendedAllButton {
	
	NSString * title;
	
	if (!recomended) {
		title = @"All";
		
	} else {title = @"Recomended"; }
	
	// Create cancel button
	UIBarButtonItem * button = [[UIBarButtonItem alloc] 
								initWithTitle: title 
								style:UIBarButtonItemStylePlain 
								target:self 
								action:@selector(recomendedButtonTap:)];
	
	self.navigationItem.leftBarButtonItem = button;
	
}



- (void)dealloc {
	[mapView release];
	[locationManager release];
    [super dealloc];
}


@end
