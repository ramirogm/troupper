    //
//  UISignupTableViewController.m
//  troupper.0
//
//  Created by Betiana Darderes on 6/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "UISignupTableViewController.h"
#import "EditableDetailCell.h"
#import "User.h"


@implementation UISignupTableViewController


@synthesize firstNameCell;
@synthesize lastNameCell;
@synthesize phoneCell;
@synthesize emailCell;
@synthesize passwordCell;
@synthesize genderCell;
@synthesize birthdayCell;
@synthesize photoCell;

@synthesize user;

#pragma mark -


- (UISignupTableViewController*)init
{		
	self.user = [[User alloc] init];	
	
	return[super init];
}
- (void)dealloc
{
	[user release];
	[firstNameCell release];
    [lastNameCell release];
	[phoneCell release];
	[emailCell release];
	[passwordCell release];
	[genderCell release];
	[birthdayCell release];
	[photoCell release];
    [super dealloc];
}


//  Convenience method that returns a fully configured new instance of 
//  EditableDetailCell. Note that methods whose names begin with 'alloc' or
//  'new', or whose names contain 'copy', should return a non-autoreleased
//  instance with a retain count of one, as we do here.
//
- (EditableDetailCell *)newDetailCellWithTag:(NSInteger)tag
{
    EditableDetailCell *cell = [[EditableDetailCell alloc] initWithFrame:CGRectZero 
                                                         reuseIdentifier:nil];
    [[cell textField] setDelegate:self];
    [[cell textField] setTag:tag];
    
    return cell;
}

#pragma mark -
#pragma mark Action Methods

- (void)signup
{
    //[[self listController] addObject:[self book]];
    
    [self dismissModalViewControllerAnimated:YES];
}

- (void)cancel
{
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark UIViewController Methods

- (void)viewDidLoad
{            
    [self setFirstNameCell:    [self newDetailCellWithTag:FirstName ]];
	[self setLastNameCell:    [self newDetailCellWithTag:LastName ]];
	[self setPhoneCell:    [self newDetailCellWithTag:Phone]];
	[self setEmailCell:    [self newDetailCellWithTag:Email]];
    [self setPasswordCell: [self newDetailCellWithTag:Password ]];
	[self setGenderCell:    [self newDetailCellWithTag:Gender]];
	[self setBirthdayCell:    [self newDetailCellWithTag:Birthday]];
	[self setPhotoCell:    [self newDetailCellWithTag:Photo]];
    
}

//  Override this method to automatically place the insertion point in the
//  first field.
//
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
	//    NSUInteger indexes[] = { 0, 0 };
	//    NSIndexPath *indexPath = [NSIndexPath indexPathWithIndexes:indexes length:2];
	// Más fácil así:
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
	
    EditableDetailCell *cell = (EditableDetailCell *)[[self tableView]
                                                      cellForRowAtIndexPath:indexPath];
    
    [[cell textField] becomeFirstResponder];
}

//  Force textfields to resign firstResponder so that our implementation of
//  -textFieldDidEndEditing: gets called. That will ensure that the current
//  UI values are flushed to our model object before we return to the list view.
//
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    for (NSUInteger section = 0; section < [[self tableView] numberOfSections]; section++)
    {
        for (NSUInteger row = 0; row < [[self tableView] numberOfRowsInSection:section]; row++)
        {
            NSUInteger indexes[] = { section, row };
            NSIndexPath *indexPath = [NSIndexPath indexPathWithIndexes:indexes
                                                                length:7];
            
            EditableDetailCell *cell = (EditableDetailCell *)[[self tableView]
                                                              cellForRowAtIndexPath:indexPath];
            if ([[cell textField] isFirstResponder])
            {
                [[cell textField] resignFirstResponder];
            }
        }
    }
}

#pragma mark -
#pragma mark UITextFieldDelegate Protocol

//  Sets the label of the keyboard's return key to 'Done' when the insertion
//  point moves to the table view's last field.
//
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    
    return YES;
}

//  UITextField sends this message to its delegate after resigning
//  firstResponder status. Use this as a hook to save the text field's
//  value to the corresponding property of the model object.
//
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    static NSNumberFormatter *_formatter;
    
    if (_formatter == nil)
    {
        _formatter = [[NSNumberFormatter alloc] init];
    }
    
    NSString *text = [textField text];
    
    switch ([textField tag])
    {
        case FirstName:  [user setFirstName: text];          break;
	    case LastName:  [user setLastName: text];          break;
        case Phone:  [user setPhone:text];       break;
		case Password:  [user setPassword:text];       break;
		case Gender:  [user setGender:text];       break;
		case Birthday:  [user setBirthday:text];       break;
		case Photo:  [user setPhoto:text];       break;
			
	}
}

//  UITextField sends this message to its delegate when the return key
//  is pressed. Use this as a hook to navigate back to the list view 
//  (by 'popping' the current view controller, or dismissing a modal nav
//  controller, as the case may be).
//
//  If the user is adding a new item rather than editing an existing one,
//  respond to the return key by moving the insertion point to the next cell's
//  textField, unless we're already at the last cell.
//
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField returnKeyType] != UIReturnKeyDone)
    {
        //  If this is not the last field (in which case the keyboard's
        //  return key label will currently be 'Next' rather than 'Done'), 
        //  just move the insertion point to the next field.
        //
        //  (See the implementation of -textFieldShouldBeginEditing: above.)
        //
        NSInteger nextTag = [textField tag] + 1;
        UIView *nextTextField = [[self tableView] viewWithTag:nextTag];
        
        [nextTextField becomeFirstResponder];
    }
    else if ([self isModal])
    {
        //  We're in a modal navigation controller, which means the user is
        //  adding a new book rather than editing an existing one.
        //
        [self login];
    }
    else
    {
        [[self navigationController] popViewControllerAnimated:YES];
    }
    
    return YES;
}

#pragma mark -
#pragma mark UITableViewDataSource Protocol

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return 8;
}


/*
 - (NSString *)tableView:(UITableView *)tableView
 titleForHeaderInSection:(NSInteger)section
 {
 return @"Login";
 
 }
 */

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EditableDetailCell *cell = nil;
    NSInteger tag = INT_MIN;
    NSString *text = nil;
    NSString *placeholder = nil;
    
    //  Pick the editable cell and the values for its textField
    //
    NSUInteger index = [indexPath row];
    switch (index) 
    {
        case 0:
        {
            cell = [self firstNameCell];
            text = [user firstName];
            tag = FirstName;
            placeholder = @"First name";
            break;
        }
        case 1:
        {
            cell = [self lastNameCell];
			text = [user lastName];
			tag = LastName;
			placeholder = @"Last name";
			break;
        }
		case 2:
        {
            cell = [self phoneCell];
			text = [user Phone];
			tag = Phone;
			placeholder = @"Phone";
			break;
        }
		case 3:
        {
            cell = [self emailCell];
			text = [user email];
			tag = Email;
			placeholder = @"Email";
			break;
        }
		case 4:
        {
            cell = [self passwordCell];
			text = [user password];
			tag = Password;
			placeholder = @"Password";
			break;
        }
		
		case 5:
        {
            cell = [self genderCell];
			text = [user gender];
			tag = Gender;
			placeholder = @"Gender";
			break;
        }
		
		case 6:
        {
            cell = [self birthdayCell];
			text = [user birthday];
			tag = Birthday;
			placeholder = @"Birthday";
			break;
        }
		
		case 7:
        {
            cell = [self photoCell];
			text = [user photo];
			tag = Photo;
			placeholder = @"Photo";
			break;
        }
			
    }
    
    UITextField *textField = [cell textField];
    [textField setTag:tag];
    [textField setText:text];
    [textField setPlaceholder:placeholder];
    
    return cell;
}



// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}




@end
