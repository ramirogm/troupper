//
//  ActivityProgress.h
//  CTM Photo-Video
//
//  Created by Betiana Darderes on 11/21/10.
//  Copyright 2010. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ActivityProgress : UIAlertView {
    UIActivityIndicatorView *activityIndicator;
    UILabel *progressMessage;
	UIImageView *backgroundImageView;

}

@property (nonatomic, assign) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, retain) UILabel *progressMessage;
@property (nonatomic, assign) UIImageView *backgroundImageView;


- (id)initWithLabel:(NSString *)text;
- (void)dismiss;

@end
