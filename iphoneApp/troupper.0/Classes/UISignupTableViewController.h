//
//  UISignupTableViewController.h
//  troupper.0
//
//  Created by Betiana Darderes on 6/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EditableDetailCell;
@class User;


//  Constants representing the book's fields.
//
enum {
    FirstName,
	LastName,
	Phone,
	Email,
    Password,
	Gender,
	Birthday,
	Photo
    
};


typedef NSUInteger BookAttribute;
@interface UISignupTableViewController : UITableViewController <UITextFieldDelegate> {
	
	User *user;
    //MyListController *_listController;
    EditableDetailCell * firstNameCell;
    EditableDetailCell * lastNameCell;
	EditableDetailCell * phoneCell;
	EditableDetailCell * emailCell;
	EditableDetailCell * passwordCell;
	EditableDetailCell * genderCell;
	EditableDetailCell * birthdayCell;
	EditableDetailCell * photoCell;
	
}
@property (nonatomic, retain) User *user;

//  Watch out for retain cycles (objects that retain each other can never
//  be deallocated). If the list controller were to retain the detail controller,
//  we should change 'retain' to 'assign' below to avoid a cycle. (Note that in
//  that case, dealloc shouldn't release the list controller.)
//
//@property (nonatomic, retain) MyListController *listController;

@property (nonatomic, retain) EditableDetailCell *userCell;
@property (nonatomic, retain) EditableDetailCell * firstNameCell;
@property (nonatomic, retain) EditableDetailCell * lastNameCell;
@property (nonatomic, retain) EditableDetailCell * phoneCell;
@property (nonatomic, retain) EditableDetailCell * emailCell;
@property (nonatomic, retain) EditableDetailCell * passwordCell;
@property (nonatomic, retain) EditableDetailCell * genderCell;
@property (nonatomic, retain) EditableDetailCell * birthdayCell;
@property (nonatomic, retain) EditableDetailCell * photoCell;

- (BOOL)isModal;

- (EditableDetailCell *)newDetailCellWithTag:(NSInteger)tag;

//  Action Methods
//
- (void)login;
- (void)cancel;


@end
