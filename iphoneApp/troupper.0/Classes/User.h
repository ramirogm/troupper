//
//  User.h
//  troupper.0
//
//  Created by Betiana Darderes on 5/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface User : NSObject {

	NSString *userName;
    NSString *password;
	NSString *lastName;
	NSString *firstName;
	NSString *photo;
	NSString *phone;
	NSString *email;
	NSString *gender;
	NSString *birthday;
	NSString *bio;
	int tag;
	NSMutableArray * interests;
	UIImage * croppedImage;
	UIImage * newCellImage;
	BOOL isPublished;
	UIImage * downloadedImage;
  }

@property (nonatomic, retain) NSString *userName;
@property (nonatomic, retain) NSString *password;
@property (nonatomic, retain) NSString *firstName;
@property (nonatomic, retain) NSString *lastName;
@property (nonatomic, retain) NSString *gender;
@property (nonatomic, retain) NSString *birthday;
@property (nonatomic, retain) NSString *photo;
@property (nonatomic, retain) NSString *phone;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSString *bio;
@property (nonatomic, retain) NSMutableArray * interests;
@property (nonatomic, retain) UIImage * croppedImage;
@property (nonatomic, retain) UIImage * newCellImage;
@property (nonatomic, retain) UIImage * downloadedImage;
@property int tag;
@property BOOL isPublished;


+ (NSArray *)keys;
- (id)initWithDictionary:(NSDictionary *)dictionary;
- (UIImage*)fullImage;

@end