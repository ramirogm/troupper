package com.urbawave.services;

public enum PageName {

	REGISTER, LOGIN, INDEX, PROFILE, EDITPROFILE, DEBUG
}
