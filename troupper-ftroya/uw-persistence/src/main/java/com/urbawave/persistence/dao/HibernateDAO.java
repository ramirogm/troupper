/**
 * 
 */
package com.urbawave.persistence.dao;

import java.io.Serializable;
import java.util.List;

import org.apache.tapestry5.hibernate.annotations.CommitAfter;
import org.hibernate.FlushMode;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.urbawave.domain.User;

/**
 * @author ftroya
 * 
 */
@Repository
public class HibernateDAO extends HibernateDaoSupport implements IDAO {

	public HibernateDAO() {
	}

	@Autowired
	public HibernateDAO(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Transactional(readOnly = false)
	@CommitAfter
	public void persist(Object entity) {
		getHibernateTemplate().saveOrUpdate(entity);
	}

	// @Transactional
	public void persist(Object[] entities) {
		for (int i = 0; i < entities.length; i++) {
			persist(entities[i]);
		}
	}

	// @Transactional(readOnly = true)
	public <T> List<T> find(Class<T> entityClass) {
		final List<T> entities = getHibernateTemplate().loadAll(entityClass);
		return entities;
	}

	@Transactional(readOnly = true)
	public <T> T load(Class<T> entityClass, Serializable id) {
		final T entity = (T) getHibernateTemplate().load(entityClass, id);
		return entity;
	}

	// @Transactional(readOnly = true)
	public <T> List<T> find(String hql) {
		final List<T> entities = getHibernateTemplate().find(hql);
		return entities;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.springframework.orm.hibernate3.support.HibernateDaoSupport#
	 * createHibernateTemplate(org.hibernate.SessionFactory)
	 */
	@Override
	protected HibernateTemplate createHibernateTemplate(SessionFactory sessionFactory) {
		HibernateTemplate hibernateTemplate = super.createHibernateTemplate(sessionFactory);
		hibernateTemplate.setFlushMode(10);
		return hibernateTemplate;
	}

	public User findByUserName(String userName) {
		List<Object> users = this.find("from User where userName='" + userName + "'");
		return users.isEmpty() ? null : (User) users.get(0);
	}
}
