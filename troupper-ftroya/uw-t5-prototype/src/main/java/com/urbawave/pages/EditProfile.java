/**
 * 
 */
package com.urbawave.pages;

import java.io.File;
import java.util.Arrays;

import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.upload.services.UploadedFile;

import com.urbawave.components.DataCheckbox;
import com.urbawave.domain.Profile;
import com.urbawave.persistence.dao.HibernateDAO;
import com.urbawave.services.AppConstants;

/**
 * @author ftroya
 * 
 */
public class EditProfile {

	@Inject
	private HibernateDAO hibernateDAO;

	@SessionState
	@Property
	private com.urbawave.domain.Profile profile = new Profile();

	@Property
	private UploadedFile file;

	private int profileID;

	@Component
	private Form form;

	private String interest;

	@Inject
	private Messages messages;

	@Component
	private DataCheckbox dcheckbox;

	void onActivate(int id) {
		if (id == 0) {
			id++;
		}
		;
		profileID = id;
		profile = hibernateDAO.load(com.urbawave.domain.Profile.class, id);
	}

	int onPassivate() {
		return profileID;
	}

	public void onSuccess() {
		if (file != null) {
			File copied = new File(messages.get(AppConstants.DEFAULT_IMG_PATH) + file.getFileName());
			file.write(copied);
			profile.setImagePath(file.getFilePath());
		}
		profile.setInterests(Arrays.asList(new String[] { "persistedValue1", "persistedValue2" }));
		profile.setBio(dcheckbox.getData());
		hibernateDAO.persist(profile);
	}

	/**
	 * @return the interest
	 */
	public String getInterest() {
		return interest;
	}

	/**
	 * @param interest
	 *            the interest to set
	 */
	public void setInterest(String interest) {
		this.interest = interest;
	}

}
