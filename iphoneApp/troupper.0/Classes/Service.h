//
//  Service.h
//  troupper.0
//
//  Created by Betiana Darderes on 1/21/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>


@interface Service : NSObject {
	
	NSString * name;
	NSString * photoName;
	NSString * subtitle;
	BOOL like;
	BOOL recomended;//Los usuarios hicieron muchos likes
	BOOL now;//Si ahora esta disponible la oferta, aca tiene que ponerse una hora.
	double lat;
	double lng;
	int tag;

}

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * photoName;
@property (nonatomic, retain) NSString * subtitle;
@property BOOL like;
@property BOOL recomended;
@property BOOL now;
@property int tag;

@property double lat;
@property double lng;

- (CLLocationCoordinate2D) coordinate;


@end
