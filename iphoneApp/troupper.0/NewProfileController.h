//
//  NewProfileController.h
//  troupper.0
//
//  Created by Betiana Darderes on 1/26/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"


@interface NewProfileController : UIViewController <UITextViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate> {
	
	
	IBOutlet UILabel * nameLabel;
	IBOutlet UITextView* bioTV;
	IBOutlet UIImageView* imageView;
	UIImagePickerController *imagePicker;
	IBOutlet UIImage* image;
	UIImage * profileImage;
	User * newUser;
	User * editedUser;
	

}


@property (nonatomic, retain) UILabel * nameLabel;
@property (nonatomic, retain) UITextView* bioTV;
@property (nonatomic, retain) UIImageView* imageView;
@property (nonatomic, retain) UIImage* image;
@property (nonatomic, retain) UIImage * profileImage;
@property (nonatomic, retain) User * newUser;
@property (nonatomic, retain) User * editedUser;

- (IBAction)uploadFoto:(id)sender;

@end
