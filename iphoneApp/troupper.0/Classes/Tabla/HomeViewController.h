//
//  HomeViewController.h
//  KClark
//
//  Created by Betiana Darderes on 7/21/11.
//

#import <UIKit/UIKit.h>
#import "HomePlaceDataSource.h"


@interface HomeViewController : UIViewController {
	
	
}


- (IBAction)selectHome:(id)sender;

- (IBAction)selectHotel:(id)sender;

- (IBAction)selectSchool:(id)sender;

- (IBAction)selectRestaurant:(id)sender;

- (IBAction)selectOffice:(id)sender;

- (IBAction)selectPublic:(id)sender;

- (IBAction)selectRetail:(id)sender;

- (IBAction)selectGym:(id)sender;


- (void)selectPlace: (HomePlaceDataSource *) dataSource;

@end
