//
//  ProductController.h
//  KClark
//
//  Created by Betiana Darderes on 7/21/11.
//

#import <UIKit/UIKit.h>
#import "Technique.h"


@interface ProductController : UIViewController {

	UINavigationController * navController;
	IBOutlet UIScrollView * scrollView;
	UIImageView *imageView;
	NSString * imageName;
	Technique * technique;
	
}


@property (nonatomic, retain) IBOutlet UINavigationController * navController;
@property (nonatomic, retain) IBOutlet UIImageView *imageView;
@property (nonatomic, retain) Technique * technique;
@property (nonatomic, retain) UIScrollView * scrollView;

- (id)initWithTechnique:(Technique *) aTechnique;

@end
