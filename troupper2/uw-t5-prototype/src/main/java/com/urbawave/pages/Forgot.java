package com.urbawave.pages;

import org.apache.log4j.Logger;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

import com.urbawave.annotations.AnonymousAccess;
import com.urbawave.domain.User;
import com.urbawave.persistence.dao.HibernateDAO;
import com.urbawave.services.Authenticator;

@AnonymousAccess
public class Forgot {

//    @Inject
//    private Authenticator authenticator;
//
	@InjectPage
	private Result resultado;
	
    @Inject
    private HibernateDAO dao;

//    @Persist
//    private int userId;
//    
//	@SessionState
//    @Property
//    private User user;

//    @Property
//    private User recipient;
    
    @Component
    private Form recoverForm;

    @Property
    private String username;

    @Component(id="username")
    private TextField usernameField;

    @Inject
    private MailSender mailSender;

    @Inject
    private SimpleMailMessage mailMessage;

    //@Property
    private String error;

    private Logger logger = Logger.getLogger(Forgot.class);

    
	public void onActivate() {
//		this.recipient = this.dao.load(User.class, this.userId);
//		int id = this.authenticator.getLoggedUser().getId();
//		this.user = this.dao.load(com.urbawave.domain.User.class, id);
	}

	public Object onSuccess() {

		try {
			if ( this.username == null || this.username.length() == 0) {
				recoverForm.recordError(this.usernameField,"Please enter a username or email address");
				return Forgot.class;
			}
			boolean looksLikeEmail = this.username.indexOf('@')>0;
			User u;
			if ( looksLikeEmail) {
				u = dao.findUniqueByEmail(this.username);
			} else {
				u = dao.findByUserName(this.username);
			}
			
			if ( u==null) {
				recoverForm.recordError(this.usernameField,"Unknown username or email");
				return Forgot.class;
			}
			this.mailMessage.setFrom("Troupper <noreply@troupper.com>");
			this.mailMessage.setSubject("Troupper recover password");
			String mailText = "Somebody at troupper.com ( probably you ) requested a password remainder for your account. Your username is " + u.getUserName() + " and your password is " + u.getPassword(); 
			this.mailMessage.setText(mailText);
			this.mailMessage.setTo(u.getEmail());
			this.mailSender.send(this.mailMessage);
			this.resultado.setMensaje("Message successfully sent");
		} catch (Exception e) {
			e.printStackTrace();
			setError(e.getMessage());
			logger.error("Couldn't send the mail", e);
			this.resultado.setMensaje(error);
		}
		return this.resultado;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	
	
}
