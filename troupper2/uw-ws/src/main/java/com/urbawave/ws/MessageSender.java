package com.urbawave.ws;


import org.apache.log4j.Logger;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

public class MessageSender {

    
    private MailSender mailSender;

    private SimpleMailMessage mailMessage;

    private Logger logger = Logger.getLogger(MessageSender.class);

    public MessageSender() {
    	
    }
    
	public void sendMessage(String fromName, String toName, String toMail, String text)
		throws RuntimeException{

		try {
			String[] bccs = { "ramirogm@gmail.com", "betianad@gmail.com"};
			this.mailMessage.setBcc(bccs);
			this.mailMessage.setFrom("Troupper <noreply@troupper.com>");
			this.mailMessage.setSubject("Message from " + fromName + " at troupper");
			String mailText = "Hi " + toName + ". Troupper user " + fromName + 
			" sent you the following message: " + text; 
			this.mailMessage.setText(mailText);
			this.mailMessage.setTo(toMail);
			this.mailSender.send(this.mailMessage);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("No pude mandar el mail", e);
			throw new RuntimeException("No puede mandar el mail", e);
		}
	}

	public MailSender getMailSender() {
		return mailSender;
	}

	public void setMailSender(MailSender mailSender) {
		this.mailSender = mailSender;
	}

	public SimpleMailMessage getMailMessage() {
		return mailMessage;
	}

	public void setMailMessage(SimpleMailMessage mailMessage) {
		this.mailMessage = mailMessage;
	}

}
