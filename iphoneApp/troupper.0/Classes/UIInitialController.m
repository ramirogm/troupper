    //
//  UIInitialController.m
//  troupper.0
//
//  Created by Betiana Darderes on 5/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "UIInitialController.h"
#import "UILoginController.h"
#import "UISignupController.h"
#import "TroupperAppDelegate.h"


@implementation UIInitialController
@synthesize navController;





//The user logins
- (IBAction)login:(id)sender {
	
	UILoginController *loginController = [[UILoginController alloc] initWithNibName:@"login" bundle:nil]; 
	
	TroupperAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    [self.view addSubview: delegate.iniNavController.view];
	
	[delegate.iniNavController pushViewController:loginController animated:YES];
	[loginController release];
	
	
	
}
//The user signups
- (IBAction)signup:(id)sender {
	

	UISignupController *signupController = [[UISignupController alloc] initWithNibName:@"signup" bundle:nil];
	[self.view addSubview:navController.view];
	[navController pushViewController:signupController animated:YES];
	[signupController release];
	
	

	
}


- (id)init {
    self = [super initWithNibName:@"Initial" bundle:nil];
    if (self) {
        self.navController = [[UINavigationController alloc] initWithRootViewController:self];
		
    }
    return self;
}


/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {

 
    [super viewDidLoad];
}



 // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewWillAppear:(BOOL)animated {
	
	
	[super viewWillAppear:YES];
 }
 



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[navController release];
    [super dealloc];
}


@end
