//
//  Surface.m
//  KClark
//
//  Created by Betiana Darderes on 7/21/11.
//

#import "Surface.h"
#import "Technique.h"


@implementation Surface

@synthesize imageName;
@synthesize name;
@synthesize techniqueDictionary;




//setups the technique collection
-(id)initWithName:(NSString*)aName{
	if (self = [super init]) {
		[self setName: aName];
		[self setupTechniqueDictionary];
	}
	return self;
}


//setups the technique collection
- (void)setupTechniqueDictionary {

	self.techniqueDictionary = [NSMutableDictionary dictionary];
	
	// creates empty array entries in the technique Dictionary for each place
	[techniqueDictionary setObject:[NSMutableArray array] forKey:@"Home"];
	[techniqueDictionary setObject:[NSMutableArray array] forKey:@"Office"];
	[techniqueDictionary setObject:[NSMutableArray array] forKey:@"School"];
	[techniqueDictionary setObject:[NSMutableArray array] forKey:@"Gym"];
	[techniqueDictionary setObject:[NSMutableArray array] forKey:@"Restaurant"];
	[techniqueDictionary setObject:[NSMutableArray array] forKey:@"Hotel"];
	[techniqueDictionary setObject:[NSMutableArray array] forKey:@"Public Transport"];
	[techniqueDictionary setObject:[NSMutableArray array] forKey:@"Retail"];
	
	//creates the techniques
	Technique *cleaningFood = [[Technique alloc] init];
	[cleaningFood setName:@"Cleaning Food"];
	[cleaningFood setImageName:@"03_concept1.png"];
	[cleaningFood setProductImageName:@"04_titlepic4.png"];
	
	Technique *desinfecting = [[Technique alloc] init];
	[desinfecting setName:@"Desinfecting"];
	[desinfecting setImageName:@"03_concept2.png"];
	[desinfecting setProductImageName:@"04_titlepic2.png"];
	
	Technique *odorControl = [[Technique alloc] init];
	[odorControl setName:@"OdorControl"];
	[odorControl setImageName:@"03_concept3.png"];
	[odorControl setProductImageName:@"04_titlepic1.png"];
	
	Technique *cleaningSoap = [[Technique alloc] init];
	[cleaningSoap setName:@"Cleaning Soap"];
	[cleaningSoap setImageName:@"03_concept4.png"];
	[cleaningSoap setProductImageName:@"04_titlepic3.png"];
	
	
	//Depending on the surface name, add each technique in the proper place key
	if ([self.name isEqualToString:@"Door Handles"]) {
		
		[[techniqueDictionary objectForKey:@"Home"] addObject:desinfecting];
		[[techniqueDictionary objectForKey:@"Office"]addObject:desinfecting];
		[[techniqueDictionary objectForKey:@"School"]addObject:desinfecting];
		[[techniqueDictionary objectForKey:@"Gym"]addObject:desinfecting];
		[[techniqueDictionary objectForKey:@"Restaurant"] addObject:desinfecting];
		[[techniqueDictionary objectForKey:@"Hotel"]addObject:desinfecting];
		[[techniqueDictionary objectForKey:@"Public Transport"]addObject:desinfecting];
		[[techniqueDictionary objectForKey:@"Retail"]addObject:desinfecting];
		}
	else {
		if ([self.name isEqualToString:@"Microwave/Cofee Maker"]) {
			
			
			[[techniqueDictionary objectForKey:@"Home"] addObject:desinfecting];
			[[techniqueDictionary objectForKey:@"Office"]addObject:desinfecting];
			[[techniqueDictionary objectForKey:@"School"]addObject:desinfecting];
			[[techniqueDictionary objectForKey:@"Restaurant"] addObject:desinfecting];
			[[techniqueDictionary objectForKey:@"Hotel"]addObject:desinfecting];
			
			[[techniqueDictionary objectForKey:@"Home"] addObject:cleaningFood];
			[[techniqueDictionary objectForKey:@"Office"]addObject:cleaningFood];
			[[techniqueDictionary objectForKey:@"School"]addObject:cleaningFood];
			[[techniqueDictionary objectForKey:@"Restaurant"] addObject:cleaningFood];
			[[techniqueDictionary objectForKey:@"Hotel"]addObject:cleaningFood];

			[[techniqueDictionary objectForKey:@"Home"] addObject:odorControl];
			[[techniqueDictionary objectForKey:@"Office"]addObject:odorControl];
			[[techniqueDictionary objectForKey:@"School"]addObject:odorControl];
			[[techniqueDictionary objectForKey:@"Restaurant"] addObject:odorControl];
			[[techniqueDictionary objectForKey:@"Hotel"]addObject:odorControl];
			
		
			
			
		}
		else {
			if ([self.name isEqualToString:@"Copier"]) {
				
				[[techniqueDictionary objectForKey:@"Office"]addObject:desinfecting];
				[[techniqueDictionary objectForKey:@"School"]addObject:desinfecting];
				[[techniqueDictionary objectForKey:@"Hotel"]addObject:desinfecting];
				
				
		
			}
			else {
				
				if ([self.name isEqualToString:@"Counters"]) {
					
					
					
					[[techniqueDictionary objectForKey:@"Home"] addObject:desinfecting];
					[[techniqueDictionary objectForKey:@"Office"]addObject:desinfecting];
					[[techniqueDictionary objectForKey:@"School"]addObject:desinfecting];
					[[techniqueDictionary objectForKey:@"Restaurant"] addObject:desinfecting];
					[[techniqueDictionary objectForKey:@"Hotel"]addObject:desinfecting];
					[[techniqueDictionary objectForKey:@"Retail"]addObject:desinfecting];
					
					[[techniqueDictionary objectForKey:@"Home"] addObject:cleaningFood];
					[[techniqueDictionary objectForKey:@"Office"]addObject:cleaningFood];
					[[techniqueDictionary objectForKey:@"School"]addObject:cleaningFood];
					[[techniqueDictionary objectForKey:@"Restaurant"] addObject:cleaningFood];
					[[techniqueDictionary objectForKey:@"Hotel"]addObject:cleaningFood];
					[[techniqueDictionary objectForKey:@"Retail"]addObject:cleaningFood];
					
					[[techniqueDictionary objectForKey:@"Home"] addObject:odorControl];
					[[techniqueDictionary objectForKey:@"Office"]addObject:odorControl];
					[[techniqueDictionary objectForKey:@"School"]addObject:odorControl];
					[[techniqueDictionary objectForKey:@"Restaurant"] addObject:odorControl];
					[[techniqueDictionary objectForKey:@"Hotel"]addObject:odorControl];
					[[techniqueDictionary objectForKey:@"Retail"]addObject:odorControl];
					
					
				}
				else {
				if ([self.name isEqualToString:@"Sinks/Faucetts"]) {
					
					
					
					[[techniqueDictionary objectForKey:@"Home"] addObject:desinfecting];
					[[techniqueDictionary objectForKey:@"Office"]addObject:desinfecting];
					[[techniqueDictionary objectForKey:@"School"]addObject:desinfecting];
					[[techniqueDictionary objectForKey:@"Gym"]addObject:desinfecting];
					[[techniqueDictionary objectForKey:@"Restaurant"] addObject:desinfecting];
					[[techniqueDictionary objectForKey:@"Hotel"]addObject:desinfecting];
					[[techniqueDictionary objectForKey:@"Retail"]addObject:desinfecting];
					
					
					
					[[techniqueDictionary objectForKey:@"Home"] addObject:cleaningSoap];
					[[techniqueDictionary objectForKey:@"Office"]addObject:cleaningSoap];
					[[techniqueDictionary objectForKey:@"School"]addObject:cleaningSoap];
					[[techniqueDictionary objectForKey:@"Gym"]addObject:cleaningSoap];
					[[techniqueDictionary objectForKey:@"Restaurant"] addObject:cleaningSoap];
					[[techniqueDictionary objectForKey:@"Hotel"]addObject:cleaningSoap];
					[[techniqueDictionary objectForKey:@"Retail"]addObject:cleaningSoap];
					
					
					[[techniqueDictionary objectForKey:@"Home"] addObject:odorControl];
					[[techniqueDictionary objectForKey:@"Office"]addObject:odorControl];
					[[techniqueDictionary objectForKey:@"School"]addObject:odorControl];
					[[techniqueDictionary objectForKey:@"Gym"]addObject:odorControl];
					[[techniqueDictionary objectForKey:@"Restaurant"] addObject:odorControl];
					[[techniqueDictionary objectForKey:@"Hotel"]addObject:odorControl];
					[[techniqueDictionary objectForKey:@"Retail"]addObject:odorControl];
					
					
				} else
				{
				if ([self.name isEqualToString:@"Floors"]) {
								
					[[techniqueDictionary objectForKey:@"Home"] addObject:cleaningSoap];
					[[techniqueDictionary objectForKey:@"Office"]addObject:cleaningSoap];
					[[techniqueDictionary objectForKey:@"School"]addObject:cleaningSoap];
					[[techniqueDictionary objectForKey:@"Gym"]addObject:cleaningSoap];
					[[techniqueDictionary objectForKey:@"Restaurant"] addObject:cleaningSoap];
					[[techniqueDictionary objectForKey:@"Hotel"]addObject:cleaningSoap];
					[[techniqueDictionary objectForKey:@"Public Transport"]addObject:cleaningSoap];
					[[techniqueDictionary objectForKey:@"Retail"]addObject:cleaningSoap];
					
					[[techniqueDictionary objectForKey:@"Home"] addObject:cleaningFood];
					[[techniqueDictionary objectForKey:@"Office"]addObject:cleaningFood];
					[[techniqueDictionary objectForKey:@"School"]addObject:cleaningFood];
					[[techniqueDictionary objectForKey:@"Gym"]addObject:cleaningFood];
					[[techniqueDictionary objectForKey:@"Restaurant"] addObject:cleaningFood];
					[[techniqueDictionary objectForKey:@"Hotel"]addObject:cleaningFood];
					[[techniqueDictionary objectForKey:@"Public Transport"]addObject:cleaningFood];
					[[techniqueDictionary objectForKey:@"Retail"]addObject:cleaningFood];
					
				} else
				{
					if ([self.name isEqualToString:@"Phone/Remote/Keyboard/Pencils/Mouse"]) {
						
						[[techniqueDictionary objectForKey:@"Home"] addObject:cleaningFood];
						[[techniqueDictionary objectForKey:@"Office"]addObject:cleaningFood];
						[[techniqueDictionary objectForKey:@"School"]addObject:cleaningFood];
						[[techniqueDictionary objectForKey:@"Hotel"]addObject:cleaningFood];
						[[techniqueDictionary objectForKey:@"Retail"]addObject:cleaningFood];
						
						
						[[techniqueDictionary objectForKey:@"Home"] addObject:desinfecting];
						[[techniqueDictionary objectForKey:@"Office"]addObject:desinfecting];
						[[techniqueDictionary objectForKey:@"School"]addObject:desinfecting];
						[[techniqueDictionary objectForKey:@"Hotel"]addObject:desinfecting];
						[[techniqueDictionary objectForKey:@"Retail"]addObject:desinfecting];
						
					}else {
					
					if ([self.name isEqualToString:@"Light Switches/Elevator Buttons"]) {
				
										
						[[techniqueDictionary objectForKey:@"Home"] addObject:cleaningFood];
						[[techniqueDictionary objectForKey:@"Office"]addObject:cleaningFood];
						[[techniqueDictionary objectForKey:@"School"]addObject:cleaningFood];
						[[techniqueDictionary objectForKey:@"Gym"]addObject:cleaningFood];
						[[techniqueDictionary objectForKey:@"Restaurant"] addObject:cleaningFood];
						[[techniqueDictionary objectForKey:@"Hotel"]addObject:cleaningFood];
						[[techniqueDictionary objectForKey:@"Public Transport"]addObject:cleaningFood];
						[[techniqueDictionary objectForKey:@"Retail"]addObject:cleaningFood];
						
						[[techniqueDictionary objectForKey:@"Home"] addObject:desinfecting];
						[[techniqueDictionary objectForKey:@"Office"]addObject:desinfecting];
						[[techniqueDictionary objectForKey:@"School"]addObject:desinfecting];
						[[techniqueDictionary objectForKey:@"Gym"]addObject:desinfecting];
						[[techniqueDictionary objectForKey:@"Restaurant"] addObject:desinfecting];
						[[techniqueDictionary objectForKey:@"Hotel"]addObject:desinfecting];
						[[techniqueDictionary objectForKey:@"Public Transport"]addObject:desinfecting];
						[[techniqueDictionary objectForKey:@"Retail"]addObject:desinfecting];
				
				
					}
					else {
						if ([self.name isEqualToString:@"Sports Equipment"]) {
							
							[[techniqueDictionary objectForKey:@"Home"] addObject:cleaningFood];
							[[techniqueDictionary objectForKey:@"School"]addObject:cleaningFood];
							[[techniqueDictionary objectForKey:@"Gym"]addObject:cleaningFood];
							
							[[techniqueDictionary objectForKey:@"Home"] addObject:desinfecting];
							[[techniqueDictionary objectForKey:@"School"]addObject:desinfecting];
							[[techniqueDictionary objectForKey:@"Gym"]addObject:desinfecting];
							
							
						}
						
						else {
							if ([self.name isEqualToString:@"Tub/Shower"]) {
								
								[[techniqueDictionary objectForKey:@"Home"] addObject:cleaningSoap];
								[[techniqueDictionary objectForKey:@"Gym"]addObject:cleaningSoap];
								[[techniqueDictionary objectForKey:@"Hotel"]addObject:cleaningSoap];
								
								[[techniqueDictionary objectForKey:@"Home"] addObject:odorControl];
								[[techniqueDictionary objectForKey:@"Gym"]addObject:odorControl];
								[[techniqueDictionary objectForKey:@"Hotel"]addObject:odorControl];
								
								
							}
							
							else {
								if ([self.name isEqualToString:@"Garbage Cans"]) {
									
									[[techniqueDictionary objectForKey:@"Home"] addObject:cleaningFood];
									[[techniqueDictionary objectForKey:@"Office"]addObject:cleaningFood];
									[[techniqueDictionary objectForKey:@"School"]addObject:cleaningFood];
									[[techniqueDictionary objectForKey:@"Gym"]addObject:cleaningFood];
									[[techniqueDictionary objectForKey:@"Restaurant"] addObject:cleaningFood];
									[[techniqueDictionary objectForKey:@"Hotel"]addObject:cleaningFood];
									[[techniqueDictionary objectForKey:@"Public Transport"]addObject:cleaningFood];
									[[techniqueDictionary objectForKey:@"Retail"]addObject:cleaningFood];
									
									
									[[techniqueDictionary objectForKey:@"Home"] addObject:odorControl];
									[[techniqueDictionary objectForKey:@"Office"]addObject:odorControl];
									[[techniqueDictionary objectForKey:@"School"]addObject:odorControl];
									[[techniqueDictionary objectForKey:@"Gym"]addObject:odorControl];
									[[techniqueDictionary objectForKey:@"Restaurant"] addObject:odorControl];
									[[techniqueDictionary objectForKey:@"Hotel"]addObject:odorControl];
									[[techniqueDictionary objectForKey:@"Public Transport"]addObject:odorControl];
									[[techniqueDictionary objectForKey:@"Retail"]addObject:odorControl];
									
									
								}}}}}}}}}}}
				
	[desinfecting release];
	[odorControl release];
	[cleaningFood release];
	[cleaningSoap release];
	
		}

//Retrieves the techniques for a place key
- (NSArray *)techniquesForPlace:(NSString*)aPlace{
	
	return [self.techniqueDictionary objectForKey:aPlace];
}


- (void)dealloc {
	
	[name release];
	[imageName release];
	[techniqueDictionary release];
    [super dealloc];
}


@end
