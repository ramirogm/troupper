//
//  ActivityProgress.m
//  CTM Photo-Video
//
//  Created by Betiana Darderes on 11/21/10.
//  Copyright 2010. All rights reserved.
//

#import "ActivityProgress.h"

@implementation ActivityProgress

@synthesize backgroundImageView, activityIndicator, progressMessage;

- (id)initWithLabel:(NSString *)text {
    if (self = [super init]) {
       		
		backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"progressBackground.png"]];
		backgroundImageView.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin);
		backgroundImageView.alpha = 0.5;
		[self addSubview:backgroundImageView];

        progressMessage = [[UILabel alloc] initWithFrame:CGRectZero];
        progressMessage.textColor = [UIColor whiteColor];
        progressMessage.backgroundColor = [UIColor clearColor];
		progressMessage.font = [UIFont fontWithName:@"Helvetica" size:(14.0)];
        progressMessage.text = text;
        [self addSubview:progressMessage];

        activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [activityIndicator startAnimating];
        [self addSubview:activityIndicator];
    }

    return self;
}

- (void)drawRect:(CGRect)rect {
}

- (void)layoutSubviews {
    [progressMessage sizeToFit];
	
	progressMessage.center = backgroundImageView.center;
	activityIndicator.center = backgroundImageView.center;

    CGRect textRect = progressMessage.frame;
    textRect.origin.y += 30.0;
    progressMessage.frame = textRect;

    CGRect activityRect = activityIndicator.frame;
    activityRect.origin.y -= 10.0;
    activityIndicator.frame = activityRect;
	
    [self bringSubviewToFront:activityIndicator];
    [self bringSubviewToFront:progressMessage];
}

- (void)show {
    [super show];
    CGSize backGroundImageSize = self.backgroundImageView.image.size;
    self.bounds = CGRectMake(0, 0, backGroundImageSize.width, backGroundImageSize.height);
	[self layoutSubviews];
    [self bringSubviewToFront:activityIndicator];
    [self bringSubviewToFront:progressMessage];
}

- (void)dismiss {
    [super dismissWithClickedButtonIndex:0 animated:YES];
  
}

- (void)dismissWithClickedButtonIndex:(NSInteger)buttonIndex animated:(BOOL)animated {
    [super dismissWithClickedButtonIndex:buttonIndex animated:animated];

}

- (void)dealloc {
    [activityIndicator release];
    [progressMessage release];
    [super dealloc];
}

@end
