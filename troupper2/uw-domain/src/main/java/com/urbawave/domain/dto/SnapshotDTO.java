package com.urbawave.domain.dto;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

public class SnapshotDTO {

	public Date date ;
	public String name;
	public Boolean isPrivate;
	public Collection<Integer> profilesIds = new HashSet<Integer>();
	public Integer ownerId;
	public Collection<Integer> tagIds = new HashSet<Integer>();
	public Double lati;
	public Double longi;
	
	/**
	 * Retorna true si el DTO es válido, y completo, como para crear un Snapshot. Es decir, que tiene todos
	 * las properties necesarias seteadas
	 * ( en XSD esto se pondría con required ) 
	 * @return
	 */
	public boolean isValidForCreate() {
		if ( date == null ) return false;
		if ( name == null || name.length()==0) return false; 
		if ( ownerId == null || ownerId == 0 ) return false;
		return true;
	}
}
