//
//  PageViewController.h
//  KClark
//
//  Created by Betiana Darderes on 7/21/11.
//

#import <UIKit/UIKit.h>
#import "Technique.h"


@interface PageViewController : UIViewController <UIScrollViewDelegate> {

	UIImageView *imageView;
	Technique *technique;
}

@property (nonatomic, retain) IBOutlet UIImageView *imageView;
@property (nonatomic, retain)Technique *technique;


- (id)initWithTechnique:(Technique *) aTechnique page: (int) page;


@end
