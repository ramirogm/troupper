/**
 * 
 */
package com.urbawave.pages;

import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.PasswordField;
import org.apache.tapestry5.ioc.annotations.Inject;

import com.urbawave.annotations.AnonymousAccess;
import com.urbawave.domain.User;
import com.urbawave.persistence.dao.TooManyRowsException;
import com.urbawave.security.AuthenticationException;
import com.urbawave.services.Authenticator;

/**
 * 
 * Ver http://tapestry.apache.org/forms-and-validation.html
 * 
 * @author ftroya
 * 
 */
@AnonymousAccess
public class Login {

	@Inject
	private Authenticator authenticator;

	@Persist
	@Property
	private String login;

	@Property
	private String pass;

	@Component(id = "password")
	private PasswordField passwordField;

	@Component
	private Form loginForm;

	@SessionState
	private User user;

	public Object onSuccess() {
		try {
			authenticator.login(login, pass);
		} catch (AuthenticationException e) {
			loginForm.recordError(passwordField,e.getMessage());
		}
		return user != null ? Index.class : Register.class;
	}

	Object onHit() {
		return Register.class;
	}

}
