//
//  Technique.m
//  KClark
//
//  Created by Betiana Darderes on 7/21/11.
//

#import "Technique.h"


@implementation Technique
@synthesize imageName;
@synthesize name;
@synthesize productImageName;


- (void)dealloc {
	
	[name release];
	[imageName release];
	[productImageName release];
	
	[super dealloc];
}
@end
