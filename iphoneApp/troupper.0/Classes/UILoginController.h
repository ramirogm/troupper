//
//  UILoginController.h
//  troupper.0
//
//  Created by Betiana Darderes on 5/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrouperService.h"


@class UILoginTableViewController;


@interface UILoginController : UIViewController <ServiceDelegate>{

	//UILoginTableViewController *loginTableController;
	IBOutlet UIActivityIndicatorView * activityIndicator;
	IBOutlet UITextField * userNameField;
	IBOutlet UITextField * passwordField;
	

}

//@property (nonatomic, retain) IBOutlet UILoginTableViewController *loginTableController;
@property (nonatomic, retain) UIActivityIndicatorView * activityIndicator;
@property (nonatomic, retain) UITextField * passwordField;
@property (nonatomic, retain) UITextField * userNameField;
@end


