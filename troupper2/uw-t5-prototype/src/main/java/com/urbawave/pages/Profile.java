/**
 * 
 */
package com.urbawave.pages;

import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.annotations.Inject;

import com.urbawave.domain.User;
import com.urbawave.persistence.dao.HibernateDAO;
import com.urbawave.services.Authenticator;

/**
 * @author ftroya
 * 
 */
public class Profile {

	@Inject
	private Authenticator authenticator;

	@Inject
	private HibernateDAO dao;

	@SessionState
	@Property
	private User user;

	@Property
	private com.urbawave.domain.Profile profile;

	@Property
	private String imagePath;

	@InjectPage
	private EditProfile editProfile;

    @InjectPage
    private ProfileSnapshots profileSnapshots;
	
	@Property
	private String interest;

	public String getActive() {
		if (this.profile != null) {
			return this.profile.getActive() ? "active" : "";
		}
		return "";
	}

	public EditProfile getEditProfile() {
		return this.editProfile;
	}

	public Object onActionFromDeleteProfile(int id) {
		com.urbawave.domain.Profile profile2 = this.dao.load(
				com.urbawave.domain.Profile.class, id);
		if (!profile2.getId().equals(1)) {
			this.dao.delete(profile2);
		}
		this.dao.refresh(this.user);
		return Profile.class;
	}

	public Object onActionFromEditProfile(int id) {
		com.urbawave.domain.Profile profile2 = this.dao.load(
				com.urbawave.domain.Profile.class, id);
    	this.editProfile.onActivate(profile2.getId());
    	return this.editProfile;
	}

    public Object onActionFromShowSnapshots(int id) {
	    this.profileSnapshots.setProfileId(id);
	    return this.profileSnapshots;
        }
 	
	
	public void onActivate() {
		this.user = this.authenticator.getLoggedUser();
		this.dao.refresh(this.user);
	}

	public void setActive(String active) {
	}

	public void setEditProfile(EditProfile editProfile) {
		this.editProfile = editProfile;
	}

}
