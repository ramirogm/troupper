// Para mandar un POST
NSURL *url = [NSURL URLWithString:@"https://xxxxxxx.com/users/sendmessage/form"];

NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
             cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];

NSString *formString = @"from=FROM&to=TO&text=MENSAJE";

NSData *requestData = [NSData dataWithBytes:[formString UTF8String] length:[formString length]];

[request setHTTPMethod:@"POST"];
[request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
[request setHTTPBody: requestData];

//[request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//[request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

NSURLConnection *connection = [[NSURLConnection alloc]initWithRequest:request delegate:self];
// de acá en adelante no sé que hace
if (connection) {
 receivedData = [[NSMutableData data] retain];
}



----------- JSON


// Así se puede armar una estructura, para después generar el JSON:

// un arreglo de 3 elementos + nil
NSArray *values = [NSArray arrayWithObjects:@"pepe", @"xxxxxxx", @"fruta", nil];

NSArray *keys = [NSArray arrayWithObjects:@"nick_name", @"UDID", @"user_question", nil];

// El diccionario
NSDictionary *dict = [NSDictionary dictionaryWithObjects:objects forKeys:keys];

NSString *jsonRequest = [dict JSONRepresentation];

NSLog(@"jsonRequest is %@", jsonRequest);

NSURL *url = [NSURL URLWithString:@"https://xxxxxxx.com/questions"];

NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
             cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];


NSData *requestData = [NSData dataWithBytes:[jsonRequest UTF8String] length:[jsonRequest length]];

[request setHTTPMethod:@"POST"];
[request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
[request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
[request setValue:[NSString stringWithFormat:@"%d", [requestData length]] forHTTPHeaderField:@"Content-Length"];
[request setHTTPBody: requestData];

NSURLConnection *connection = [[NSURLConnection alloc]initWithRequest:request delegate:self];
if (connection) {
 receivedData = [[NSMutableData data] retain];
}








The receivedData is then handled by:

NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
NSDictionary *jsonDict = [jsonString JSONValue];
NSDictionary *question = [jsonDict objectForKey:@"question"];


