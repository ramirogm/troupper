    //
//  ServiceController.m
//  troupper.0
//
//  Created by Betiana Darderes on 1/21/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ServiceController.h"
#import "Service.h"


@implementation ServiceController

@synthesize likeButton;
@synthesize serviceImage;
@synthesize service;


- (id)initWithService: (Service *) aService {
    self = [super initWithNibName:@"Service" bundle:nil];
    if (self) {
		
		service = aService;
      
		
    }
    return self;
}


/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	
	
	
	self.navigationItem.title = [service name];
	
	[self setLikeButton];
		
	NSString *path = [[NSBundle mainBundle] pathForResource:[service photoName] ofType:@"png"];
    UIImage *theImage = [UIImage imageWithContentsOfFile:path];
	
	[self.serviceImage setImage: theImage];
    [super viewDidLoad];
}



// Override to allow orientations other than the default portrait orientation.
- (BOOL) setLikeButton {
	
	NSString * title;
	
 if ([service like] == YES) {
      title = @"Unlike";
 
 } else {title = @"Like"; }
 
 // Create cancel button
 likeButton = [[UIBarButtonItem alloc] 
			   initWithTitle: title 
 style:UIBarButtonItemStylePlain 
 target:self 
 action:@selector(likeButtonTap)];
 
 self.navigationItem.rightBarButtonItem = likeButton;

}





- (void)likeButtonTap {
	
	if ([service like] ==YES) {
		[service setLike: NO];
		//[likeButton setTitle: @"Like" forState:UIControlStateNormal];
	} else {
		[service setLike: YES];
		//[likeButton setTitle: @"Unlike" forState:UIControlStateNormal];

	}
	[self setLikeButton];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}




- (void)dealloc {
	[likeButton release];
	[serviceImage release];
	
    [super dealloc];
}


@end
