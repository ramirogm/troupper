package com.troupper;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class SeleniumTroupper {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://www.troupper.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testPepe() throws Exception {
	  for (int i = 0; i < 20; i++) {
		    doScript();
	}
  }

private void doScript() {
	driver.get(baseUrl + "/uw-t5-prototype/login");
    driver.findElement(By.id("login")).clear();
    driver.findElement(By.id("login")).sendKeys("betianademo");
    driver.findElement(By.id("password")).clear();
    driver.findElement(By.id("password")).sendKeys("demo");
    driver.findElement(By.cssSelector("input[type=\"submit\"]")).click();
    driver.findElement(By.linkText("Capturas")).click();
    driver.findElement(By.linkText("Partnership")).click();
    driver.findElement(By.linkText("Acerca de...")).click();
    driver.findElement(By.linkText("Inicio")).click();
    driver.findElement(By.linkText("Editar")).click();
    driver.findElement(By.linkText("Acerca de...")).click();
    driver.findElement(By.linkText("Inicio")).click();
    driver.findElement(By.linkText("Logout")).click();
}

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}