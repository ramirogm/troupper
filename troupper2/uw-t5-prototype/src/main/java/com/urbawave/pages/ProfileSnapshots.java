package com.urbawave.pages;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.RequestGlobals;

import com.urbawave.domain.Profile;
import com.urbawave.domain.Snapshot;
import com.urbawave.domain.User;
import com.urbawave.persistence.dao.HibernateDAO;
import com.urbawave.services.AppConstants;
import com.urbawave.services.Authenticator;

public class ProfileSnapshots {

    static Logger logger = Logger.getLogger(ProfileSnapshots.class);

    @Inject
    private Authenticator authenticator;

    @Inject
    private HibernateDAO dao;

    @Property
    private Profile profile;

    @Inject
    private Messages messages;	

    @InjectPage
    private ProfileSnapshots profileSnapshots;

    @InjectPage
    private SendMessage sendMessage;

    @Persist
    private int profileId;
    
    @Property
    private boolean profileNotFromLoggedUser;
    
    // Usados para la iteración
    @Property
    private Snapshot eachSnapshot;

    @Property
    private Profile eachSnapshotProfile;
    
    // FIN usados para la iteración
    
	public String getDateFormatted() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		return sdf.format(this.eachSnapshot.getFecha());
	}

    public Object onActionFromSendMessage(int id) {
	    this.sendMessage.setUserId(id);
	    return this.sendMessage;
        }
 
    public void onActivate() {
    	this.profile = this.dao.load(com.urbawave.domain.Profile.class, this.profileId);
		setProfileIsFromLoggedUser();
        }

	private void setProfileIsFromLoggedUser() {
		int loggedUserId = this.authenticator.getLoggedUser().getId();
		this.profileNotFromLoggedUser = this.profile.getUser().getId() != loggedUserId;
	}

    public Object onActionFromShowProfile(int id) {
	    this.profileSnapshots.setProfileId(id);
	    return this.profileSnapshots;
        }
 

    public int getProfileId() {
		return profileId;
	}

	public void setProfileId(int profileId) {
		this.profileId = profileId;
	}

	public Boolean getProfileIsFromLoggedUser() {
		return !profileNotFromLoggedUser;
	}
	
	/**
	 * Hecho así porque no encuentro un AND en Tapestry 
	 * 
	 * @return
	 */
	public Boolean getPrivateSnapshotAndNotFromLoggedUser() {
		return eachSnapshot.getPrivateSnapshot() && profileNotFromLoggedUser;
	}
	
	public List<Snapshot> getSnapshotsOrderedByDateDesc() {
		List<Snapshot> snapshots = new ArrayList<Snapshot>(this.profile.getSnapshots());
		Collections.sort(snapshots, new Comparator<Snapshot>() {

	        public int compare(Snapshot s1, Snapshot s2) {
	            return s1.getFecha().compareTo(s2.getFecha()) * -1;
	        }
	    });
		return snapshots;
	}
	
	public List<Snapshot> getPublicSnapshots() {
		List<Snapshot> result = new ArrayList<Snapshot>();
		for (Snapshot snapshot : this.profile.getSnapshots()) {
			if (!( snapshot.getPrivateSnapshot() && profileNotFromLoggedUser)) {
				result.add(snapshot);
			}
		}
		return result;
	}

}
