/**
 * 
 */
package com.urbawave.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.urbawave.domain.dto.SnapshotDTO;

/**
 * @author ftroya
 * 
 */
@Entity
public class Snapshot {

    @Id
    @GeneratedValue
    private Integer id;

    @ManyToMany(mappedBy = "snapshot", cascade = CascadeType.ALL)
    private Set<Tag> tags = new HashSet<Tag>();

    private String nombre;

    private Date fecha;

    private Boolean privateSnapshot;

    @ManyToMany(mappedBy = "otherSnapshots", cascade = { CascadeType.ALL })
    private Set<Profile> profiles = new HashSet<Profile>();

    /**
     * Profile que sacó el snapshot
     */
    @ManyToOne
    private Profile ownerProfile;
    
    private Double lati;
    
    private Double longi;
    
    
    
    public Double getLati() {
		return lati;
	}

	public void setLati(Double lati) {
		this.lati = lati;
	}

	public Double getLongi() {
		return longi;
	}

	public void setLongi(Double longi) {
		this.longi = longi;
	}

	public Profile getOwnerProfile() {
		return ownerProfile;
	}

	public void setOwnerProfile(Profile ownerProfile) {
		this.ownerProfile = ownerProfile;
	}

	public Date getFecha() {
	return this.fecha;
    }

    public Integer getId() {
	return this.id;
    }

    public String getNombre() {
	return this.nombre;
    }

    public Boolean getPrivateSnapshot() {
	return this.privateSnapshot;
    }

    public Set<Tag> getTags() {
	return this.tags;
    }

    public Set<Profile> getProfiles() {
	return this.profiles;
    }

    public void setFecha(Date fecha) {
	this.fecha = fecha;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public void setNombre(String nombre) {
	this.nombre = nombre;
    }

    public void setPrivateSnapshot(Boolean privateSnapshot) {
	this.privateSnapshot = privateSnapshot;
    }

    public void setTags(Set<Tag> tags) {
	this.tags = tags;
    }

    public void setProfiles(Set<Profile> profiles) {
	this.profiles = profiles;
    }
}
