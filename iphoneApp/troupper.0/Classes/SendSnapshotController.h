//
//  SendSnapshotController.h
//  troupper.0
//
//  Created by Betiana Darderes on 1/28/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SendSnapshotController : UIViewController <UITextViewDelegate> {

	IBOutlet UITextView * tag;
	IBOutlet UISwitch * isPrivate;
	
	
	
}

@property (nonatomic,retain) UITextView * tag;
@property (nonatomic,retain) UISwitch * isPrivate;



- (IBAction)send:(id)sender;

@end


