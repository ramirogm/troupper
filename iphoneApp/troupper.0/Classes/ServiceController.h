//
//  ServiceController.h
//  troupper.0
//
//  Created by Betiana Darderes on 1/21/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"


@interface ServiceController : UIViewController {
	
	IBOutlet UIButton *likeButton;
	IBOutlet UIImageView * serviceImage;
	 Service * service;

}

@property (retain, nonatomic) UIButton *likeButton;
@property (retain, nonatomic) UIImageView * serviceImage;
@property (retain, nonatomic) Service * service;
@end
