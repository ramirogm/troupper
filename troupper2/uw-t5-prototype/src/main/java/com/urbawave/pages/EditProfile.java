/**
 * 
 */
package com.urbawave.pages;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Vector;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.RequestGlobals;
import org.apache.tapestry5.upload.services.UploadedFile;

import com.urbawave.domain.Profile;
import com.urbawave.domain.User;
import com.urbawave.persistence.dao.HibernateDAO;
import com.urbawave.services.AppConstants;
import com.urbawave.services.Authenticator;

/**
 * @author ftroya
 * 
 */
public class EditProfile {

	@Inject
	private HibernateDAO hibernateDAO;

	@Inject
	private Authenticator authenticator;

	@SessionState
	@Property
	private com.urbawave.domain.Profile profile = new Profile();

	@Property
	private String interest;

	@Property
	private UploadedFile file;

	private int profileID;

	@Component
	private Form form;

	@Inject
	private Messages messages;

	// @Component
	// private DataCheckbox dcheckbox;

	@Inject
	private RequestGlobals requestGlobals;

    private static Logger logger = Logger.getLogger(EditProfile.class);
	
	void onActivate(int id) {
		if (id > 0) {
			this.profile = this.hibernateDAO.load(
					com.urbawave.domain.Profile.class, id);
		} else {
			this.profile = new Profile();
			setDefaultImageToProfile(this.profile);
		}
		this.profileID = id;
	}

	public static void setDefaultImageToProfile(Profile p) {
		String defaultFile = "usuario_default.png";
		p.setImagePath(defaultFile);
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(defaultFile);
		try {
			byte[] bytes = IOUtils.toByteArray(is);
			p.setImage(bytes);
		} catch (IOException e) {
			logger.log(Priority.FATAL, "Al leer el archivo imagen por default", e);
		}
	}

	int onPassivate() {
		return this.profileID;
	}

	public Object onSuccess() {
		if (this.file != null) {
			this.profile.setImagePath(this.file.getFileName());

			byte[] bFile = new byte[(int) file.getSize()];
			try {
				InputStream inputStream = file.getStream();
				// convert file into array of bytes
				inputStream.read(bFile);
			} catch (Exception e) {
				e.printStackTrace();
			}
			this.profile.setImage(bFile);
		}

		User user = this.authenticator.getLoggedUser();

		if (!this.profile.getActive()) {
			Profile profileActivo = null;
			for (Profile profileIter : user.getProfiles()) {
				if (profileIter.getActive()) {
					profileActivo = profileIter;
					break;
				}
			}
			if (profileActivo == null) {
				this.profile.setActive(Boolean.TRUE);
			}
		}

		this.profile.setUser(user);
		// this.profile.setInterestsAsString(this.interest);
		// this.profile.setName(form);
		this.hibernateDAO.persist(this.profile);
		this.hibernateDAO.refresh(user);
		return com.urbawave.pages.Profile.class;
	}
}
