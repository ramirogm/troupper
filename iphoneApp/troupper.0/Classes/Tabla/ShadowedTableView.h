//
//  ShadowedTableView.h
//  KClark
//
//  Created by Betiana Darderes on 7/21/11.
//
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface ShadowedTableView : UITableView
{
	CAGradientLayer *originShadow;
	CAGradientLayer *topShadow;
	CAGradientLayer *bottomShadow;
}

@end
