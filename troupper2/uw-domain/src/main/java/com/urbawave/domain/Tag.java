package com.urbawave.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Tag {

    @Id
    @GeneratedValue
    private Integer id;

    private String name;

    @ManyToMany
    private Set<Snapshot> snapshot = new HashSet<Snapshot>();

    public Integer getId() {
	return this.id;
    }

    public String getName() {
	return this.name;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public void setName(String name) {
	this.name = name;
    }

}
