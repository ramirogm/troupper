/**
 * 
 */
package com.urbawave.domain;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.util.StringUtils;

import com.urbawave.domain.dto.ProfileDTO;
import com.urbawave.domain.listeners.ProfileListener;

/**
 * @author ftroya
 * 
 */
@Entity
@EntityListeners({ ProfileListener.class })
public class Profile {

	@Id
	@GeneratedValue
	private Integer id;

	private String name;

	private String imagePath;

	@Lob
	@Column(name = "PROFILE_IMAGE")
	private byte[] image;

	@ManyToOne
	private User user;

	@OneToMany(mappedBy = "ownerProfile", cascade = CascadeType.ALL)
	private Set<Snapshot> snapshots = new HashSet<Snapshot>();

	@ManyToMany
	private Set<Snapshot> otherSnapshots = new HashSet<Snapshot>();

	private String interestsAsString;

	private Boolean active = Boolean.FALSE;

	private Boolean online = Boolean.FALSE;

	private Boolean isPublic = Boolean.FALSE;

	private String bio;

	private String homepage;

	private String email;

	private String otherInterests;
	
	private Double lastSeenLat = Double.valueOf(-1);
	
	private Double lastSeenLong = Double.valueOf(-1);

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (obj instanceof Profile) {
			Profile newObj = (Profile) obj;
			if (this.id != null) {
				return (this.id.compareTo(newObj.getId()) == 0);
			} else {
				return newObj.getId() == null;
			}

		}

		return super.equals(obj);
	}

	public Set<Snapshot> getSnapshots() {
		return snapshots;
	}

	public void setSnapshots(Set<Snapshot> snapshots) {
		this.snapshots = snapshots;
	}

	public Set<Snapshot> getOtherSnapshots() {
		return otherSnapshots;
	}

	public void setOtherSnapshots(Set<Snapshot> otherSnapshots) {
		this.otherSnapshots = otherSnapshots;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	/**
	 * @return the active
	 */
	public Boolean getActive() {
		return this.active;
	}

	/**
	 * @return the bio
	 */
	public String getBio() {
		return this.bio;
	}

	// static constraints = {
	// bio(nullable: true, maxSize: 1000)
	// homepage(url: true, nullable: true)
	// email(email: true, nullable: true)
	// imagePath(nullable: true)
	// }

	/**
	 * @return the email
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * @return the homepage
	 */
	public String getHomepage() {
		return this.homepage;
	}

	public Integer getId() {
		return this.id;
	}

	/**
	 * @return the imagePath
	 */
	public String getImagePath() {
		return this.imagePath;
	}

	/**
	 * @return the interests
	 */
	public List<String> getInterests() {

		String[] commaDelimitedListToStringArray = StringUtils
				.commaDelimitedListToStringArray(this.interestsAsString);

		return Arrays.asList(commaDelimitedListToStringArray);

	}

	/**
	 * @return the interestsAsString
	 */
	public String getInterestsAsString() {
		return this.interestsAsString;
	}

	/**
	 * @return the isPublic
	 */
	public Boolean getIsPublic() {
		return this.isPublic;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @return the otherInterests
	 */
	public String getOtherInterests() {
		return this.otherInterests;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return this.user;
	}

	@Override
	public int hashCode() {

		if (this.id != null) {
			return this.id.hashCode() + super.hashCode();
		}

		return super.hashCode();
	}

	/**
	 * @param active
	 *            the active to set
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}

	/**
	 * @param bio
	 *            the bio to set
	 */
	public void setBio(String bio) {
		this.bio = bio;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @param homepage
	 *            the homepage to set
	 */
	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param imagePath
	 *            the imagePath to set
	 */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	/**
	 * @param interests
	 *            the interests to set
	 */
	public void setInterests(List<String> interests) {
		String collectionToDelimitedString = StringUtils
				.collectionToDelimitedString(interests, ",");

		this.interestsAsString = collectionToDelimitedString;
	}

	/**
	 * @param interestsAsString
	 *            the interestsAsString to set
	 */
	public void setInterestsAsString(String interestsAsString) {
		this.interestsAsString = interestsAsString;
	}

	/**
	 * @param isPublic
	 *            the isPublic to set
	 */
	public void setIsPublic(Boolean isPublic) {
		this.isPublic = isPublic;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param otherInterests
	 *            the otherInterests to set
	 */
	public void setOtherInterests(String otherInterests) {
		this.otherInterests = otherInterests;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	public Boolean isOnline() {
		return online;
	}

	public Boolean isOffLine() {
		return !this.isOnline();
	}

	public void beOnline() {
		this.online = true;
	}

	public void beOffline() {
		this.online = false;
	}

	public Boolean getOnline() {
		return online;
	}

	public void setOnline(Boolean online) {
		this.online = online;
	}

	public Double getLastSeenLat() {
		return lastSeenLat;
	}

	public void setLastSeenLat(Double lastSeenLat) {
		this.lastSeenLat = lastSeenLat;
	}

	public Double getLastSeenLong() {
		return lastSeenLong;
	}

	public void setLastSeenLong(Double lastSeenLong) {
		this.lastSeenLong = lastSeenLong;
	}

	public void updateFrom(ProfileDTO dto) {
		// TODO Esto seguro se debería poder hacer automáticamente con alguna
		// library como beanutils
		if ( dto.active != null) this.active = dto.active;
		if ( dto.isPublic != null) this.isPublic = dto.isPublic;
		if ( dto.bio != null) this.bio = dto.bio;
		if ( dto.email != null) this.email = dto.email;
		if ( dto.homepage != null) this.homepage = dto.homepage;
		if ( dto.image != null) this.image = dto.image;
		if ( dto.imagePath != null) this.imagePath = dto.imagePath;
		if ( dto.interestsAsString != null) this.interestsAsString = dto.interestsAsString;
		if ( dto.name != null) this.name = dto.name;
		if ( dto.otherInterests != null) this.otherInterests = dto.otherInterests;
		this.lastSeenLat = dto.lastSeenLat;
		this.lastSeenLong = dto.lastSeenLong;
	}

	public ProfileDTO getDTO() {
		// TODO Esto seguro se debería poder hacer automáticamente con alguna
		// library...
		ProfileDTO dto = new ProfileDTO();
		dto.active = this.active;
		dto.bio = this.bio;
		dto.email = this.email;
		dto.homepage = this.homepage;
		dto.id = this.id;
		dto.image = this.image;
		dto.imagePath = this.imagePath;
		dto.interestsAsString = this.interestsAsString;
		dto.isPublic = this.isPublic;
		dto.name = this.name;
		dto.otherInterests = this.otherInterests;
		dto.lastSeenLat = this.lastSeenLat;
		dto.lastSeenLong = this.lastSeenLong;
		return dto;
	}
	
}
