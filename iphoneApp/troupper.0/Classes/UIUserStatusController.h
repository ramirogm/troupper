//
//  UIUserStatusController.h
//  troupper.0
//
//  Created by Betiana Darderes on 6/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataSourceProtocol.h"
#import "ShadowedTableView.h"

@interface UIUserStatusController : UIViewController {
	

		
		UINavigationController * navController;
		ShadowedTableView *theTableView;
		id<DataSourceProtocol,UITableViewDataSource> dataSource;
		
	}
	
	@property (nonatomic,retain) ShadowedTableView *theTableView;
	@property (nonatomic,retain) id<DataSourceProtocol,UITableViewDataSource> dataSource;
    @property (nonatomic, retain) IBOutlet UINavigationController * navController;
	
	- (id)initWithDataSource:(id<DataSourceProtocol,UITableViewDataSource>)theDataSource;
	
	
	
	
	@end

