package com.urbawave.pages;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.BeanEditForm;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.RequestGlobals;

import com.urbawave.domain.Profile;
import com.urbawave.domain.Snapshot;
import com.urbawave.domain.User;
import com.urbawave.persistence.dao.HibernateDAO;
import com.urbawave.services.AppConstants;
import com.urbawave.services.Authenticator;

public class MySnapshots {

    @Inject
    private Authenticator authenticator;

    @Inject
    private HibernateDAO dao;


	@SessionState
    @Property
    private User user;

    @Property
    private Profile myProfile;

    @Property
    private Set<Snapshot> snapshots;

    @Property
    private Snapshot snapshot;

    @Property
    private Snapshot newSnapshot = new Snapshot();

    @Property
    private Profile profile;

    @Property
    private String imagePath;

    @Inject
    private RequestGlobals requestGlobals;

    @Inject
    private Messages messages;

    @InjectPage
    private ProfileSnapshots profileSnapshots;

	public String getDateFormatted() {

		long currentTimeMillis = System.currentTimeMillis();

		long time = this.snapshot.getFecha().getTime();

		long diff = currentTimeMillis - time;

		Calendar instance = Calendar.getInstance();
		instance.setTimeInMillis(diff);
		SimpleDateFormat sdf = new SimpleDateFormat("mm:ss");

		return sdf.format(instance.getTime());

	}

    
	public Collection<Profile> getProfiles() {

		return this.snapshot.getProfiles();
	}

    public Object onActionFromShowProfile(int id) {
    	System.out.println("onActionFromShowProfileTutorial con id = " + id);
	    this.profileSnapshots.setProfileId(id);
	    return this.profileSnapshots;
        }
    
	public void onActivate() {
		int id = this.authenticator.getLoggedUser().getId();
		this.user = this.dao.load(com.urbawave.domain.User.class, id);
		this.snapshots = this.user.getActiveProfile().getSnapshots();
		this.myProfile = this.user.getActiveProfile();

		String contextRoot = this.requestGlobals.getHTTPServletRequest()
				.getContextPath();

		this.imagePath = contextRoot + "/"
				+ this.messages.get(AppConstants.DEFAULT_IMG_PATH);
	}

	void onPrepareFromsnapshotForm() {
		this.newSnapshot = new Snapshot();
	}

	public Object onSuccessFromsnapshotForm() {
		this.newSnapshot.setFecha(new Date());
		this.newSnapshot.setOwnerProfile(this.user.getActiveProfile());
		this.dao.persist(this.newSnapshot);
		return MySnapshots.class;
	}
 
	public ProfileSnapshots getProfileSnapshots() {
		return profileSnapshots;
	}


	public void setProfileSnapshots(ProfileSnapshots profileSnapshots) {
		this.profileSnapshots = profileSnapshots;
	}



}
