    //
//  UILoginTableViewController.m
//  troupper.0
//
//  Created by Betiana Darderes on 5/4/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "UILoginTableViewController.h"
#import "EditableDetailCell.h"
#import "User.h"


@implementation UILoginTableViewController


@synthesize userCell;
@synthesize passwordCell;
@synthesize user;

#pragma mark -


- (UILoginTableViewController*)init
{		
	self.user = [[User alloc] init];	
	
	return[super init];
}
- (void)dealloc
{
	[user release];
    [userCell release];
    [passwordCell release];
    [super dealloc];
}


//  Convenience method that returns a fully configured new instance of 
//  EditableDetailCell. Note that methods whose names begin with 'alloc' or
//  'new', or whose names contain 'copy', should return a non-autoreleased
//  instance with a retain count of one, as we do here.
//
- (EditableDetailCell *)newDetailCellWithTag:(NSInteger)tag
{
    EditableDetailCell *cell = [[EditableDetailCell alloc] initWithFrame:CGRectZero 
                                                         reuseIdentifier:nil];
    [[cell textField] setDelegate:self];
    [[cell textField] setTag:tag];
    
    return cell;
}

#pragma mark -
#pragma mark Action Methods


//The user logins
- (IBAction)login:(id)sender {
	
	
	//TroupperAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	
	//   [delegate startTabBarController];
	
	
}
//The user cancels
- (IBAction)cancel:(id)sender {
	
	
	//TroupperAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	
	//	[delegate startTabBarController];	
	
	
}

#pragma mark -
#pragma mark UIViewController Methods

- (void)viewDidLoad
{            
    [self setUserCell:    [self newDetailCellWithTag:UserName ]];
    [self setPasswordCell: [self newDetailCellWithTag:Password ]];
    
}

//  Override this method to automatically place the insertion point in the
//  first field.
//
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    NSUInteger indexes[] = { 0, 0 };
//    NSIndexPath *indexPath = [NSIndexPath indexPathWithIndexes:indexes length:2];
// Más fácil así:
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
							  
    EditableDetailCell *cell = (EditableDetailCell *)[[self tableView]
                                                      cellForRowAtIndexPath:indexPath];
    
    [[cell textField] becomeFirstResponder];
	
	
}

//  Force textfields to resign firstResponder so that our implementation of
//  -textFieldDidEndEditing: gets called. That will ensure that the current
//  UI values are flushed to our model object before we return to the list view.
//
- (void)viewWillDisappear:(BOOL)animated
{
    
    [super viewWillDisappear:animated];
    for (NSUInteger section = 0; section < [[self tableView] numberOfSections]; section++)
    {
        for (NSUInteger row = 0; row < [[self tableView] numberOfRowsInSection:section]; row++)
        {
            NSUInteger indexes[] = { section, row };
            NSIndexPath *indexPath = [NSIndexPath indexPathWithIndexes:indexes
                                                                length:2];
            
            EditableDetailCell *cell = (EditableDetailCell *)[[self tableView]
                                                              cellForRowAtIndexPath:indexPath];
            if ([[cell textField] isFirstResponder])
            {
                [[cell textField] resignFirstResponder];
            }
        }
    }
	
	
}

#pragma mark -
#pragma mark UITextFieldDelegate Protocol

//  Sets the label of the keyboard's return key to 'Done' when the insertion
//  point moves to the table view's last field.
//
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    
    return YES;
}

//  UITextField sends this message to its delegate after resigning
//  firstResponder status. Use this as a hook to save the text field's
//  value to the corresponding property of the model object.
//
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    static NSNumberFormatter *_formatter;
    
    if (_formatter == nil)
    {
        _formatter = [[NSNumberFormatter alloc] init];
    }
    
    NSString *text = [textField text];
    
    switch ([textField tag])
    {
        case UserName:     [user setUserName: text];          break;
        case Password:  [user setPassword:text];       break;
	}
}

//  UITextField sends this message to its delegate when the return key
//  is pressed. Use this as a hook to navigate back to the list view 
//  (by 'popping' the current view controller, or dismissing a modal nav
//  controller, as the case may be).
//
//  If the user is adding a new item rather than editing an existing one,
//  respond to the return key by moving the insertion point to the next cell's
//  textField, unless we're already at the last cell.
//
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField returnKeyType] != UIReturnKeyDone)
    {
        //  If this is not the last field (in which case the keyboard's
        //  return key label will currently be 'Next' rather than 'Done'), 
        //  just move the insertion point to the next field.
        //
        //  (See the implementation of -textFieldShouldBeginEditing: above.)
        //
        NSInteger nextTag = [textField tag] + 1;
        UIView *nextTextField = [[self tableView] viewWithTag:nextTag];
        
        [nextTextField becomeFirstResponder];
    }
    else if ([self isModal])
    {
        //  We're in a modal navigation controller, which means the user is
        //  adding a new book rather than editing an existing one.
        //
        [self login];
    }
    else
    {
        [[self navigationController] popViewControllerAnimated:YES];
    }
    
    return YES;
}

#pragma mark -
#pragma mark UITableViewDataSource Protocol

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return 2;
}


/*
- (NSString *)tableView:(UITableView *)tableView
titleForHeaderInSection:(NSInteger)section
{
    return @"Login";

}
*/

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EditableDetailCell *cell = nil;
    NSInteger tag = INT_MIN;
    NSString *text = nil;
    NSString *placeholder = nil;
    
    //  Pick the editable cell and the values for its textField
    //
    NSUInteger index = [indexPath row];
    switch (index) 
    {
        case 0:
        {
            cell = [self userCell];
            text = [user userName];
            tag = UserName;
            placeholder = @"User";
            break;
        }
        case 1:
        {
            cell = [self passwordCell];
			text = [user password];
			tag = Password;
			placeholder = @"Password";
			break;
        }
        
    }
    
    UITextField *textField = [cell textField];
    [textField setTag:tag];
    [textField setText:text];
    [textField setPlaceholder:placeholder];
    
    return cell;
}






@end
