//
//  HomePlaceDataSource.h
//  KClark
//
//  Created by Betiana Darderes on 7/21/11.
//

#import <Foundation/Foundation.h>
#import "DataSourceProtocol.h"

@interface HomePlaceDataSource : NSObject <UITableViewDataSource,DataSourceProtocol> {
	
	NSString *name;
	NSString *navigationBarName;
}

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *navigationBarName;

@end
