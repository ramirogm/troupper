
package com.urbawave.ws;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.activation.MimetypesFileTypeMap;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.apache.tapestry5.hibernate.annotations.CommitAfter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonSyntaxException;
import com.urbawave.domain.Profile;
import com.urbawave.domain.Snapshot;
import com.urbawave.domain.Tag;
import com.urbawave.domain.User;
import com.urbawave.persistence.dao.HibernateDAO;
import com.urbawave.persistence.dao.ProfileDAO;
import com.urbawave.persistence.dao.UserDAO;
import com.urbawave.domain.dto.ProfileDTO;
import com.urbawave.domain.dto.SnapshotDTO;


/**
 * FIXME http://en.wikipedia.org/wiki/SQL_injection
 * 
 * @author ramiro
 *
 */
@Path("/users")
@Component
@Scope("request")
public class UsersService {
    
	static Logger logger = Logger.getLogger(UsersService.class);
	
    @Autowired
    private HibernateDAO hibernateDAO;
	
    @Autowired
    private ProfileDAO profileDAO;

    @Autowired
    private MailSender mailSender;

    @Autowired
    private SimpleMailMessage mailMessage;

    
    
    /** Method processing HTTP GET requests, producing "text/plain" MIME media
     * type.
     * @return String that will be send back as a response of type "text/plain".
     */
    @GET 
    @Produces("text/plain")
    @Path("/login/{username}/")
    public String getIt(@PathParam("username") String username, @QueryParam("password") String password) {

    	User user = this.hibernateDAO.findByUserName(username);
    	if ( user == null) {
    		return "{ \"error\" : \"NOT FOUND\" } ";
    	}

    	if ( password == null ) {
    		return "{ \"error\" : \"No ha ingresado la password\" } ";
    	}

    	if ( !password.equals(user.getPassword())) {
    		return "{ \"error\" : \"WRONG PASSWORD\" } ";
    	}

    	return "{ \"user\" : \"" + user.getFullName() + "\"} ";
    }
    
    @GET 
    @Produces("image/*")
    @Path("/{username}/activeprofile/picture")
    public Response getUserActiveProfilePictureFromDB(@PathParam("username") String username) {
    	User user = this.hibernateDAO.findByUserName(username);
    	if ( user == null) {
            throw new WebApplicationException(404);
    	}

    	Profile profile = user.getActiveProfile();
    	if ( profile == null) {
            throw new WebApplicationException(404);
    	}
    	
    	return getImageResponseFromProfile(profile);
        }

    @GET 
    @Produces("image/*")
    @Path("/{username}/{profilename}/picture")
    public Response getUserProfilePictureFromDB(@PathParam("profilename") String profilename,
    		@PathParam("username") String username) {
    	Profile profile = this.profileDAO.findByUserAndProfileName(username, profilename);
    	if ( profile == null) {
            throw new WebApplicationException(404);
    	}
    	
    	return getImageResponseFromProfile(profile);
        }

	@GET
	@Produces("image/*")
	@Path("/profiles/{profileid}/picture")
	public Response getProfilePictureFromID(
			@PathParam("profileid") String profileid) {
		try {
			int id = Integer.parseInt(profileid);
			Profile profile = this.profileDAO.findById(id);
			if (profile == null) {
				throw new WebApplicationException(404);
			}
			return getImageResponseFromProfile(profile);
		} catch (NumberFormatException e) {
			throw new WebApplicationException(e, 404);
		}

	}

	private Response getImageResponseFromProfile(Profile profile) {
		byte[] image = profile.getImage();
		if (image.length == 0) {
			throw new WebApplicationException(404);
		}

		String mt = new MimetypesFileTypeMap().getContentType(profile
				.getImagePath());
		return Response.ok(image, mt).build();
	}

    @GET 
    @Produces("application/json")
    @Path("/list")
    public String listUsers() {
    	List<User> users = this.hibernateDAO.find(User.class);
    	StringBuffer sb = new StringBuffer();
    	sb.append("[\n");
	int first = 0;
    	for (User user : users) {
			first++;
			if ( first > 1 ) {
				sb.append(",");
			}
			sb.append("{\n");
			sb.append("\"username\" : \""+ user.getUserName() + "\",\n");
			sb.append("\"fullname\" : \""+ user.getFullName() + "\",\n");
			sb.append("\"email\" : \""+ user.getEmail() + "\",\n");
			sb.append("\"id\" : \""+ user.getId() + "\"\n");
	    		sb.append("}\n");
		}
    	sb.append("]\n");
    	return sb.toString();
    }
    
    @GET 
    @Produces("application/json")
    @Path("/{username}/profiles/list")
	public String listUserProfiles(@PathParam("username") String username) {
		User user = this.hibernateDAO.findByUserName(username);
		if (user == null) {
			throw new WebApplicationException(404);
		}
		List<Profile> profiles = user.getProfiles();
		return getProfilesJSON(profiles);
	}

    @GET 
    @Produces("application/json")
    @Path("/profiles/online/list")
	public String listOnlineProfiles() {
		List<Profile> profiles = profileDAO.findOnlineProfiles();
		return getProfilesJSON(profiles);
	}
    
	private String getProfilesJSON(List<Profile> profiles) {
		StringBuffer sb = new StringBuffer();
		sb.append("[\n");
		int first = 0;
		for (Profile profile : profiles) {
			first++;
			if (first > 1) {
				sb.append(",");
			}
			sb.append("{\n");
			sb.append("\"name\" : \"" + profile.getName() + "\",\n");
			sb.append("\"id\" : \"" + profile.getId() + "\",\n");
			sb.append("\"bio\" : \"" + profile.getBio() + "\",\n");
			sb.append("\"lastSeenLat\" : \"" + (profile.getLastSeenLat() == null ? -1 : profile.getLastSeenLat()) + "\",\n");
			sb.append("\"lastSeenLong\" : \"" + (profile.getLastSeenLong() == null ? -1 : profile.getLastSeenLong()) + "\",\n");
			sb.append("\"interests\" : \"" + profile.getInterestsAsString() + "\",\n");
			sb.append("\"interestsList\" : [");
			List<String> interests = profile.getInterests();
			int firstInterest = 0;
			for (String interest : interests) {
				firstInterest++;
				if (firstInterest > 1) {
					sb.append(", ");
				}
				sb.append("\"" + interest + "\"");
			}
			sb.append("]\n");
			sb.append("}\n");
		}
		sb.append("]\n");
		return sb.toString();
	}

	// NTH Esto deberia ser PUT, pero iOS no manda put comodamente, so...
	// VER http://iphonedevelopment.blogspot.com/2008/06/http-put-and-nsmutableurlrequest.html
	/*
	 curl -i http://localhost:8080/uw-ws/uwservices/users/notifications/bart/bartman/online/321.4/123.4
	 */
    @GET 
    @Produces("application/json")
    @Path("/notifications/{username}/{profilename}/online/{lati}/{longi}")
    @Transactional
    public String setUserProfileOnline(@PathParam("profilename") String profilename,
    		@PathParam("username") String username, @PathParam("lati") String lati, @PathParam("longi") String longi) {
    	Profile profile = this.profileDAO.findByUserAndProfileName(username, profilename);
    	if ( profile == null) {
            throw new WebApplicationException(404);
    	}
		User user = this.hibernateDAO.findByUserName(username);
		List<Profile> userProfiles = user.getProfiles();
		for (Profile each : userProfiles) {
			each.beOffline();
			this.profileDAO.saveProfile(each);
		}
    	profile.beOnline();
    	profile.setLastSeenLat(Double.valueOf(lati));
    	profile.setLastSeenLong(Double.valueOf(longi));
    	
    	this.profileDAO.saveProfile(profile);
    	return listOnlineProfiles();
    }

	// NTH Esto deberia ser PUT, pero iOS no manda put comodamente, so...
    @GET 
    @Produces("application/json")
    @Path("/notifications/{username}/{profilename}/offline")
    @Transactional
    public String setUserProfileOffline(@PathParam("profilename") String profilename,
    		@PathParam("username") String username) {
    	Profile profile = this.profileDAO.findByUserAndProfileName(username, profilename);
    	if ( profile == null) {
            throw new WebApplicationException(404);
    	}
    	profile.beOffline();
    	this.profileDAO.saveProfile(profile);    	
    	return "{\"Resultado\": \"OK\"}";
    }

    @POST 
    @Path("/sendmessage/form/")
    @Consumes("application/x-www-form-urlencoded")
    public String sendMessageFORM(@FormParam("from") String from,
    		@FormParam("to") String to, @FormParam("text") String text) {
		try {
			int fromProfileId = Integer.parseInt(from);
			Profile fromProfile = this.profileDAO.findById(fromProfileId);
			if (fromProfile == null) {
				throw new WebApplicationException(404);
			}
			int toProfileId = Integer.parseInt(to);
			Profile toProfile = this.profileDAO.findById(toProfileId);
			if (toProfile == null) {
				throw new WebApplicationException(404);
			}

			String fromName = fromProfile.getName();
			String toName = toProfile.getName();
			String toMail = toProfile.getUser().getEmail();
			// FIXME Esto deberia tenerlo directamente el MessageSender, pero no lo pude hacer andar con el autowire,
			// porque no reconoce al MessageSender como bean. Posible parche es ponerle anotations hasta que sea un 
			// componente de Jersey,pero medio feo.
			MessageSender ms = new MessageSender();
			ms.setMailSender(mailSender);
			ms.setMailMessage(mailMessage);
			try {
				ms.sendMessage(fromName, toName, toMail, text);
			} catch (RuntimeException e) {
				throw new WebApplicationException(e, 500);
			}
		} catch (NumberFormatException e) {
			throw new WebApplicationException(e, 500);
		}
    	
    	return "OK";
    }

    /**
     * Fechas en formato "{\"date\":\"23-11-2010 10:00:00\"}";
     * @param json
     * @return
     * 	HTTP STATUS CODE 422	Si no puede decodificar el JSON a un Snapshot
     * 							o si lo que vino no es válido como para crear un bean
     * 							 ( faltan properties etc )
     * TODO automatizar 
      curl -i -X POST -H 'Content-Type: application/json' \
      -d '{"name": "S_6", "isPrivate": "false", "ownerId" : "13", "date" : "24-12-2010 10:00:00", "profilesIds": [12, 11, 13, 14, 10], "lati": "123.9", "longi":"321.9"}' \
      http://localhost:8080/uw-ws/uwservices/users/savesnapshot/json
      
     */
    @POST 
    @Path("/savesnapshot/json/")
    @Consumes("application/json")
    @Transactional
    public String saveSnapshotJSON(String json) {
    	logger.debug("/savesnapshot/json. json = " + json);
        Gson gson = createGSON();    	
    	// Sin el builder usaba Gson gson = new Gson();
    	try {
			SnapshotDTO dto = gson.fromJson(json, SnapshotDTO.class);
			//Debug
			Gson gsonOutputter = new GsonBuilder().setPrettyPrinting().create();
			String jsonOutput = gsonOutputter.toJson(dto);
			logger.debug("dto = " + jsonOutput);

			if ( dto == null ) 
				throw new WebApplicationException(new Throwable("No se pudo crear el Snapshot"),422);
			if ( !dto.isValidForCreate())
				throw new WebApplicationException(new Throwable("No se pudo crear el Snapshot, datos inválidos"),422);
			
			
			Snapshot newSnapshot = new Snapshot();
			newSnapshot.setLati(dto.lati);
			newSnapshot.setLongi(dto.longi);
			newSnapshot.setFecha(dto.date);
			newSnapshot.setNombre(dto.name);
			newSnapshot.setPrivateSnapshot(dto.isPrivate);
			// Participantes
			for (Integer id : dto.profilesIds) {
				Profile p = hibernateDAO.load(Profile.class, id);
				newSnapshot.getProfiles().add(p);
				p.getOtherSnapshots().add(newSnapshot);
				logger.debug("Agregando snapshot a profile con id " + id);
			}
			
			Profile profile = hibernateDAO.load(Profile.class, dto.ownerId);
			newSnapshot.setOwnerProfile(profile);

			Set<Tag> tags = new HashSet<Tag>();
			for (Integer id : dto.tagIds) {
				Tag t = hibernateDAO.load(Tag.class, id);
				tags.add(t);
			}
			newSnapshot.setTags(tags);
			
			hibernateDAO.persist(newSnapshot);
			
			
			return "{\"resultado\": \"OK\"}";
		} catch (JsonSyntaxException e) {
			logger.error("Error al procesar un json", e);
			throw new WebApplicationException(new Throwable("Error al procesar un json"),422);
		}
    }

    @GET
    @Path("/profiles/{profileid}")
    @Produces("application/json")
    public String getProfileJSON(@PathParam("profileid") String profileid) {
    	logger.debug("/profiles/{profileid}. profileid = " + profileid );

		try {
			int id = Integer.parseInt(profileid);
			Profile profile = this.profileDAO.findById(id);
			if (profile == null) {
				throw new WebApplicationException(404);
			}
			// si quiero prettyprint: 
	    	// GsonBuilder builder = createGsonBuilder();
	    	// Gson gsonOutputter = builder.setPrettyPrinting().create();
			Gson gsonOutputter = createGSON();

	    	String jsonOutput = gsonOutputter.toJson(profile.getDTO());
	    	
	    	// Para loguear, no muestro la imagen porque es un choclo enorme
	    	ProfileDTO debugDTO = profile.getDTO();
	    	debugDTO.image = null;
	    	logger.debug("profile ( sin la image ) = " + gsonOutputter.toJson(debugDTO));

	    	return jsonOutput;
		} catch (NumberFormatException e) {
			throw new WebApplicationException(e, 404);
		}
    }
    
    
    
    /**
     * Fechas en formato "{\"date\":\"23-11-2010 10:00:00\"}";
     * @param json
     * @return
     * 	HTTP STATUS CODE 422	Si no puede decodificar el JSON a un Snapshot
     * 							o si lo que vino no es válido como para crear un bean 
     * 							( faltan properties etc )
     * 
     *  
       curl -i -X POST -H 'Content-Type: application/json' -d '{"bio": "Test from command line", \
       "interestsAsString" : "futbol,rugby,tenis", "id":13}'  \
       http://localhost:8080/uw-ws/uwservices/users/profiles/13/edit
     */
    @POST 
    @Path("/profiles/{profileid}/edit")
    @Consumes("application/json")
    @Transactional
    @CommitAfter
    public String editProfileJSON(@PathParam("profileid") String profileid, String json) {
    	logger.debug("/profiles/{profileid}/edit. profileid = " + profileid);
        Gson gson = createGSON();    	
    	ProfileDTO dto;
		try {
			dto = gson.fromJson(json, ProfileDTO.class);
	    	if ( dto == null ) 
	    		throw new WebApplicationException(new Throwable("No se pudo editar el Profile"),422);
	    	if ( dto.id != null && dto.id!=Integer.parseInt(profileid) ) 
	    		throw new WebApplicationException(new Throwable("Error en parámetros de id"),422);
	    	if ( dto.id == null) dto.id = Integer.parseInt(profileid);
	    	if ( !dto.isValidForEdit())
	    		throw new WebApplicationException(new Throwable("No se pudo editar el Profile, datos invalidos"),422);

	    	//Debug, uso un dto sin la imagen
	    	ProfileDTO debugDTO = gson.fromJson(json, ProfileDTO.class);
	    	debugDTO.image = null;
	    	Gson gsonOutputter = new GsonBuilder().setPrettyPrinting().create();
	    	String jsonOutput = gsonOutputter.toJson(debugDTO);
	    	logger.debug("dto ( sin la image ) = " + jsonOutput);

	    	
	    	int profileId = dto.id;
	    	Profile editProfile = profileDAO.findById(profileId);
	    	if ( editProfile == null ) 
	    		throw new WebApplicationException(new Throwable("No se pudo editar el Profile"),404);
	    	editProfile.updateFrom(dto);
	    	profileDAO.saveProfile(editProfile);
	    	
	    	return "{\"resultado\": \"OK\"}";
		} catch (JsonSyntaxException e) {
			logger.error("Error al procesar un json", e);
			throw new WebApplicationException(new Throwable("Error al procesar un json"),422);
		}
    }
    
	private Gson createGSON() {
		GsonBuilder builder = createGsonBuilder();
        Gson gson = builder.create();
		return gson;
	}

	private GsonBuilder createGsonBuilder() {
		GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {

            @Override
            public Date deserialize(JsonElement json, java.lang.reflect.Type typeOfT, JsonDeserializationContext context)
                    throws JsonParseException {

                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                String date = json.getAsJsonPrimitive().getAsString();
                try {
                    return format.parse(date);
                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }
            }
        });
		return builder;
	}
    
    
}
