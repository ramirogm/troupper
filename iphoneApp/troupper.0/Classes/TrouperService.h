//
//  TrouperService.h
//  troupper.0
//
//  Created by Betiana Darderes on 1/28/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ServiceDelegate

- (void)didFinishLoadingData:(NSData *)responseData; 

@end


@interface TrouperService : NSObject {
	
id <ServiceDelegate> delegate;	
NSMutableData *responseData;

}

@property (nonatomic, assign) id <ServiceDelegate> delegate;

@end

