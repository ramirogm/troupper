/**
 * 
 */
package com.urbawave.persistence.dao;

import com.urbawave.domain.User;

/**
 * @author ftroya
 * 
 */
public class UserDAO extends HibernateDAO {

	public User findByUserName(String userName) {
		// return User.findByUserName(userName);
		return (User) this.find("from User where name='" + userName + "'").get(0);
	}

}
