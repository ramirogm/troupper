package com.urbawave.components;

import org.apache.tapestry5.BindingConstants;
import org.apache.tapestry5.Block;
import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.IncludeStylesheet;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;

import com.urbawave.pages.ShowContact;

/**
 * Layout component for pages of application uw-t5-prototype.
 */
@IncludeStylesheet("context:css/style.css")
public class Layout {
	/** The page title, for the <title> element and the <h1>element. */
	@Property
	@Parameter(required = true, defaultPrefix = BindingConstants.LITERAL)
	private String title;

	@Property
	private String pageName;

	@Parameter
	private Block navBarBlock;

	@Component
	private NavBar navBarContent;

	@Inject
	private ComponentResources resources;

	public String getClassForPageName() {
		return resources.getPageName().equalsIgnoreCase(pageName) ? "current_page_item"
				: null;
	}

	public String[] getPageNames() {
		return new String[] { "Index", "About", "ShowContact" };
	}

	Object onSuccess() {
		return ShowContact.class;
	}

	public Object getNavBarContentMethod() {
		return navBarBlock == null ? navBarContent : navBarBlock;
	}
}
