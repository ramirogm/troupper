//
//  MapController.h
//  troupper.0
//
//  Created by Betiana Darderes on 1/12/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>


@interface MapController : UIViewController <CLLocationManagerDelegate, MKMapViewDelegate> {
	
	IBOutlet MKMapView *mapView;
	CLLocationManager *locationManager;
	IBOutlet UINavigationController *navController;

}

@property (retain, nonatomic) MKMapView *mapView;
@property (retain, nonatomic) CLLocationManager *locationManager;
@property (retain, nonatomic) UINavigationController *navController;



@end
