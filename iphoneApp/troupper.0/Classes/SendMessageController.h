//
//  SendMessageController.h
//  troupper.0
//
//  Created by Betiana Darderes on 1/28/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"


@interface SendMessageController : UIViewController <UITextViewDelegate>{
	
	IBOutlet UITextView * meesageTextView;
	User * toUser;
	
	
}

@property (nonatomic,retain) UITextView * meesageTextView;
@property (nonatomic,retain) User * toUser;


- (IBAction)send:(id)sender;

@end
