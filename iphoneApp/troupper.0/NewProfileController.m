    //
//  NewProfileController.m
//  troupper.0
//
//  Created by Betiana Darderes on 1/26/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "NewProfileController.h"
#import "User.h"
#import "Components.h"
#import "TrouperService.h"
#import "ServiceResponseReader.h"

@implementation NewProfileController


@synthesize nameLabel;
@synthesize bioTV;
@synthesize imageView;
@synthesize image;
@synthesize  profileImage;
@synthesize  newUser;
@synthesize  editedUser;

- (id)initNewUser{
    self = [super initWithNibName:@"NewProfile" bundle:nil];
    if (self) {
		
		 newUser = [[User alloc] init];
		 
          }
    return self;
}

- (id)initWithUser: aUser{
    self = [super initWithNibName:@"NewProfile" bundle:nil];
    if (self) {
		
		
		editedUser = aUser;
		
		
	}
    return self;
}
/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


- (void)didFinishLoadingData:(NSData *)responseData string: (NSString *) connString{	
	
	ServiceResponseReader * reader = [[ServiceResponseReader alloc] initWith:nil];
	NSString * responseString = [reader resultForm: responseData];
	
	//{"resultado": "OK"}
	
	NSRange textRange;
	textRange =[responseString rangeOfString:@"OK"];
	
	
	NSLog(responseString);
	
	if (textRange.location != NSNotFound) {
		
		{
			UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"User Value Saved" message: @"User Saved" 
															delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] autorelease];
			
			[alert show];
			
			
		}
	
	}	
}


- (IBAction)uploadFoto:(id)sender{

	[self presentModalViewController: imagePicker animated: YES];
	


}


-(void)imagePickerController: (UIImagePickerController *) picker didFinishPickingMediaWithInfo: (NSDictionary*) info{
	
	
	NSURL *mediaUrl;
	mediaUrl = (NSURL *) [info valueForKey: UIImagePickerControllerMediaURL];
	if (mediaUrl == nil) {
		image = (UIImage*) [info valueForKey:UIImagePickerControllerEditedImage];
		if (image == nil) {
			image = (UIImage*) [info valueForKey:UIImagePickerControllerOriginalImage];
			//imageView.image = image;
		}
		else {
			CGRect rect= [[info valueForKey: UIImagePickerControllerCropRect] CGRectValue];
			//imageView.image = image;
		}
		
		profileImage = [image imageByScalingAndCroppingForSize: imageView.frame.size];
		
		if (editedUser) {
			//[editedUser setUserName: nameTF.text];
			//[editedUser.interests addObjectsFromArray:[bioTV.text componentsSeparatedByString:@", "]];
			[editedUser setNewCellImage: profileImage];
		} else {
			if (newUser) {
			//	[newUser setUserName: nameTF.text];
			//	[newUser setBio: bioTV.text];
			//	[newUser setNewCellImage: profileImage];
				
				
			}}
		
		
        imageView.image = [image imageByScalingAndCroppingForSize: imageView.frame.size];
	}
	
	[picker dismissModalViewControllerAnimated: YES];
}

-(void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
[picker dismissModalViewControllerAnimated: YES];

}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	
	UIBarButtonItem *okButton = [[UIBarButtonItem alloc] 
								   initWithTitle:@"Ok" 
								   style:UIBarButtonItemStylePlain 
								   target:self 
								   action:@selector(saveUser)];
	
	self.navigationItem.rightBarButtonItem = okButton;
	[okButton release];
	
	
	
	bioTV.delegate = self;
	imagePicker = [[UIImagePickerController alloc] init];
	imagePicker.delegate = self;
	imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
	
	if (editedUser) {
	
	NSString *csv = [[editedUser interests] componentsJoinedByString: @", "];
	
	[bioTV setText: csv];
		self.navigationItem.title = [editedUser userName];
	//[nameTF setText: editedUser.userName];
	imageView.image = [[editedUser fullImage] imageByScalingAndCroppingForSize: imageView.frame.size];
	//[nameLabel setText: [editedUser userName]];
	}
	
    [super viewDidLoad];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)saveUser {
	
	TrouperService * service = [[TrouperService alloc] initWith: nil delegate: self];
	
	
	
	
	if (editedUser) {
		//[editedUser setUserName: nameTF.text];
		editedUser.interests = [bioTV.text componentsSeparatedByString:@", "];
		[editedUser setNewCellImage: profileImage];
		//NSLog([editedUser jsonRepresentation]);
		[service postEditedProfile: [editedUser jsonRepresentation] profileId: [editedUser tag]];
		
	} else {
		if (newUser) {
			//[newUser setUserName: nameTF.text];
			//[newUser setBio: bioTV.text];
			//[newUser setNewCellImage: profileImage];
			
			[[[Components sharedPlacesInstance] userProfiles] addObject: newUser];
			//[service postNewProfile: [newUser jsonRepresentation]];
			
		}}	
	


}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
{
	if ( [ text isEqualToString: @"\n" ] ) {
		[ textView resignFirstResponder ];
		return NO;
	}
	return YES;
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	
	[nameLabel release];
	[bioTV release];
	[image release];
	[profileImage release];
	
	//if (editedUser) [editedUser release];
	
	[imagePicker release];
	[imageView release];
    [super dealloc];
}


@end
