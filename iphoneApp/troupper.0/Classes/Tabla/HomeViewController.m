//
//  HomeViewController.m
//  KClark
//
//  Created by Betiana Darderes on 7/21/11.
//

#import "HomeViewController.h"
#import "KClarkAppDelegate.h"
#import "PlaceViewController.h"
#import "HomePlaceDataSource.h"

@implementation HomeViewController



- (void)viewDidLoad {
	
	UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
	self.navigationItem.backBarButtonItem = backButton;
	[backButton release];
	
    [super viewDidLoad];
	
}


- (IBAction)selectOffice:(id)sender
{
	HomePlaceDataSource *dataSource = [[HomePlaceDataSource alloc] init];
	dataSource.name = @"Office";
	dataSource.navigationBarName = @"@Office";
	[self selectPlace: dataSource];
	[dataSource release];	
}


- (IBAction)selectSchool:(id)sender
{
	HomePlaceDataSource *dataSource = [[HomePlaceDataSource alloc] init];
	dataSource.name = @"School";
	dataSource.navigationBarName = @"@School";
	[self selectPlace: dataSource];
	[dataSource release];
	
}



- (IBAction)selectGym:(id)sender
{
	
	HomePlaceDataSource *dataSource = [[HomePlaceDataSource alloc] init];
	dataSource.name = @"Gym";
	dataSource.navigationBarName = @"@Gym";
	[self selectPlace: dataSource];
	[dataSource release];}




- (IBAction)selectRestaurant:(id)sender
{
	HomePlaceDataSource *dataSource = [[HomePlaceDataSource alloc] init];
	dataSource.name = @"Restaurant";
	dataSource.navigationBarName = @"@Restaurant";
	[self selectPlace: dataSource];
	[dataSource release];
	
}



- (IBAction)selectHome:(id)sender
{
	HomePlaceDataSource *dataSource = [[HomePlaceDataSource alloc] init];
	dataSource.name = @"Home";
	dataSource.navigationBarName = @"@Home";
	[self selectPlace: dataSource];
	[dataSource release];

}


- (IBAction)selectPublic:(id)sender
{
	HomePlaceDataSource *dataSource = [[HomePlaceDataSource alloc] init];
	dataSource.name = @"Public Transport";
	dataSource.navigationBarName = @"@Public Transport";
	[self selectPlace: dataSource];
	[dataSource release];
	
}



- (IBAction)selectRetail:(id)sender
{
	
	HomePlaceDataSource *dataSource = [[HomePlaceDataSource alloc] init];
	dataSource.name = @"Retail";
	dataSource.navigationBarName = @"@Retail";
	[self selectPlace: dataSource];
	[dataSource release];
	
}



- (IBAction)selectHotel:(id)sender
{
	HomePlaceDataSource *dataSource = [[HomePlaceDataSource alloc] init];
	dataSource.name = @"Hotel";
	dataSource.navigationBarName = @"@Hotel";
	[self selectPlace: dataSource];
	[dataSource release];

	
}



//Opens the place view
- (void)selectPlace: (HomePlaceDataSource *) dataSource {
	
	KClarkAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	PlaceViewController *placeController = [[PlaceViewController alloc] initWithDataSource: dataSource]; 
    [delegate.navController pushViewController: placeController animated:YES];
	[placeController release];
	
}


- (void)viewWillAppear: (BOOL) animated {
	
	KClarkAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	delegate.navController.navigationBar.hidden = YES;
	[super viewWillAppear: animated ];
 }
 


@end
