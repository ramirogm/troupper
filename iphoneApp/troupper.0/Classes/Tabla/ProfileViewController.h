//
//  SurfaceViewController.h
//  KClark
//
//  Created by Betiana Darderes on 7/21/11.
//

#import <UIKit/UIKit.h>
#import "User.h"



@interface ProfileViewController : UIViewController <UIScrollViewDelegate> {

	UIImageView * imageView;
//	UILabel * name;
	UILabel * bio;
	User * user;
	UINavigationController * navController;
		
}


@property (nonatomic, retain) IBOutlet UIImageView * imageView;
//@property (nonatomic, retain) IBOutlet UILabel * name;
@property (nonatomic, retain) IBOutlet UILabel * bio;
@property (nonatomic, retain) IBOutlet UINavigationController * navController;
@property (nonatomic, retain)  User * user;


- (IBAction)saveContact:(id)sender;
- (IBAction)sendMessage:(id)sender;

- (IBAction)editProfile:(id)sender;
- (IBAction)publishProfile:(id)sender;


@end
