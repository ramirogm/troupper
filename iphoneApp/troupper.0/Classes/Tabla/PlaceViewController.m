//
//  PlaceViewController.m
//  KClark
//
//  Created by Betiana Darderes on 7/21/11.
//

#import "PlaceViewController.h"
#import "ProfileViewController.h"
#import "ShadowedTableView.h"
#import "TroupperAppDelegate.h"
#import "HomePlaceDataSource.h"
#import "Components.h"
#import "User.h"
#import "GradientView.h"
#import "ClearLabelsCellView.h"
#import "SendSnapshotController.h"
#import "TrouperService.h"
#import "ServiceResponseReader.h"
 


@implementation PlaceViewController

@synthesize navController;
@synthesize theTableView;
@synthesize dataSource;
@synthesize activityIndicator;
@synthesize selectedUsers;
 
BOOL online = NO;
TrouperService* service;


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	
		
	self.title = @"Friends";
	self.navigationItem.title= @"Friends";
	
	// retain the data source
	
	selectedUsers = [[NSMutableArray alloc]init];
	
	// create a new table using the full application frame
	// we'll ask the datasource which type of table to use (plain or grouped)
	ShadowedTableView *tableView = [[ShadowedTableView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame] 
																	  style:[dataSource tableViewStyle]];
	
	// set the autoresizing mask so that the table will always fill the view
	tableView.autoresizingMask = (UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight);
	
	// set the cell separator to a single straight line.
	tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	
	tableView.backgroundColor = UIColor.groupTableViewBackgroundColor; 
	
	// set the tableview delegate to this object and the datasource to the datasource which has already been set
	tableView.delegate = self;
	tableView.dataSource = self;
	
	tableView.sectionIndexMinimumDisplayRowCount=2;
	
	// set the tableview as the controller view
    self.theTableView = tableView;
	self.view = tableView;
	[tableView release];
	
	self.navigationItem.hidesBackButton = YES;	
	[self addButtons];
		
		
    [super viewDidLoad];
	 
}

//Sets the table style
- (void)addButtons  {
	
	NSString* firstTitle;
	NSString* secondTitle;
	
	if (online) {
		firstTitle = @"Unpublish me";
		secondTitle = @"Save selected";
		
		// Create cancel button
		UIBarButtonItem *saveContact = [[UIBarButtonItem alloc] 
										initWithTitle:secondTitle 
										style:UIBarButtonItemStylePlain 
										target:self 
										action:@selector(saveUserList)];
		
		self.navigationItem.rightBarButtonItem = saveContact;
		[saveContact release];
		
	} else {
		firstTitle = @"Publish me";
		
		self.navigationItem.rightBarButtonItem = nil;
	}

	
	
	
	// Create cancel button
	UIBarButtonItem *syncContact = [[UIBarButtonItem alloc] 
									initWithTitle: firstTitle
									style:UIBarButtonItemStylePlain 
									target:self 
									action:@selector(publish)];
	
	self.navigationItem.leftBarButtonItem = syncContact;
	
	[syncContact release];
	
	

	
}
//Sets the table style
- (UITableViewStyle)tableViewStyle  {
	return UITableViewStylePlain;
}


// return the surface element at the index 
- (User *)userForIndexPath:(NSIndexPath *)indexPath {
	return [[[Components sharedPlacesInstance] connUserForPublicProfile] objectAtIndex:indexPath.row];
}


- (void)didFinishLoadingData:(NSData *)responseData string: (NSString *) connString{	

	User* user;
	//NSRange textRange = [connString rangeOfString:@"online"];
	
	
	ServiceResponseReader * reader = [[ServiceResponseReader alloc] initWith:nil];
	
	//if(textRange.location != NSNotFound)
	
	if(online)
	{
	NSArray * connUsers = [reader userProfilesFrom: responseData];
		NSMutableArray * connUsersFriends = [NSMutableArray arrayWithCapacity: [connUsers count]];
	
	int i;
	User* publicProfile =[[Components sharedPlacesInstance] publicUserProfile];
	
	for (i = 0; i < [connUsers count]; i++) {
		
		User* profile = [connUsers objectAtIndex:i];
		
		if (publicProfile.tag != profile.tag) {
			[connUsersFriends addObject: profile];
			//Pide los perfiles del usuario
			//http://192.168.41.100:8080/uw-ws/uwservices/users/bart/Bart%20Simpson/picture
			NSString * imageProfilesString =  [NSString stringWithFormat: @"users/profiles/%i/picture",[profile tag]];
			UIImage *img = [service imageForProfile: imageProfilesString];
			[profile setDownloadedImage: img];
		}
		
	}
	
	
	[[Components sharedPlacesInstance] setConnUserArray: connUsersFriends];
	[theTableView reloadData];
	[activityIndicator stopAnimating];
	[activityIndicator setHidden: YES];
	
	}
}


- (void) publish {
	
	[activityIndicator setHidden: NO];
	[activityIndicator startAnimating];
	
	User* user = [[Components sharedPlacesInstance] loggedUser];
	
	User* profile = [[Components sharedPlacesInstance] publicUserProfile];
	
	service = [[TrouperService alloc] initWith: nil delegate: self];
	
	NSLog(@"users/notifications/%@/%@", [user userName], [profile userName]);
	if (online) {
		NSString* onlineServiceString = [NSString stringWithFormat: @"users/notifications/%@/%@/offline", [user userName], [profile userName] ];
		[service startWithString:onlineServiceString];
		
		[user setIsPublished: NO];
		online = NO;
		
	}else {
		NSString* offlineServiceString = [NSString stringWithFormat: @"users/notifications/%@/%@/online", [user userName], [profile userName] ];
		[service startWithString:offlineServiceString];
		[user setIsPublished: YES];
		online = YES;
	}
	[self addButtons];
	
	[theTableView reloadData];
	
	
}
// UITableViewDataSource methods

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	//Gets the surface element for the received indexPath 
	User * user = [self userForIndexPath: indexPath];
	
	//Creates the cell with the surface
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[ClearLabelsCellView alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
		cell.backgroundView = [[[GradientView alloc] init] autorelease];
    }
	
	if ( [selectedUsers containsObject:user])
	
	cell.accessoryType = UITableViewCellAccessoryCheckmark;
	
	else {
		cell.accessoryType = UITableViewCellAccessoryNone;
	}

	
	//Sets the surface name
	//cell.textLabel.text = [user userName];
	
	//Creates the surface image
	
	
	
    [cell.imageView setImage: [user thumbnail] ];
	
	
	UIButton *myButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    myButton.frame = CGRectMake(100, 25 , 150, 40); // position in the parent view and set the size of the button
    [myButton setTitle:[user userName] forState:UIControlStateNormal];
    [myButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
	myButton.tag = [user tag];
	
	[cell.contentView addSubview:myButton];
	[myButton retain];
	
    return cell;
	
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView  {
	// this table has only one section
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView  numberOfRowsInSection:(NSInteger)section {
	
	if (online) {
		return [[[Components sharedPlacesInstance] connUserForPublicProfile]count];
	} else return 0;
	
}


//The user send the form
- (IBAction)buttonClicked:(id)sender {
	
	// we know the sender is a UIButton object, so cast it
	
	if ([sender isKindOfClass:[UIButton class]]) {
	
	UIButton *aButton = (UIButton *)sender; 
	int tag =  aButton.tag;
	
	User * selectedUser = [[Components sharedPlacesInstance] connUserForTag: tag];	
	
	
	ProfileViewController * profileController = [[ProfileViewController alloc] initUser: selectedUser navController: navController];
	[navController pushViewController:profileController animated:YES];
	[profileController release];
	}
	
}



//UITableViewDataSource protocol
- (id)init {
	
	
	if ([super init]) {
		
		
	}
	return self;
}



- (void)saveUserList {
	
	//SERVICIO: que guarda los usuarios seleccionados, mas el tag, para un perfil, y un usuario.
	
	if ([selectedUsers count]>0) {
		
		SendSnapshotController * controller = [[SendSnapshotController alloc] initWithProfiles: [NSArray arrayWithArray:selectedUsers]];
		[navController pushViewController:controller animated:YES];
		[controller release];
		
	} else {
		UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Profile Selection" message: @"Select the profiles to include in the snapshot" 
														delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] autorelease];
		[alert show];
	}

	

 
}

- (void)dealloc {
	
	
	
	//[navController release];
	//[selectedUsers release];
	//[theTableView release];
	//[dataSource release];
	[super dealloc];
}





-(void)viewWillAppear:(BOOL)animated
{
	[selectedUsers removeAllObjects];
	
	
	[theTableView reloadData];
}

-(void)viewWillLoad:(BOOL)animated
{
	// force the tableview to load
	
	
	[theTableView reloadData];
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
return 85;

}

// the user selected a row in the table.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)newIndexPath {
	
	
	UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:newIndexPath];
	
	if ([selectedCell accessoryType] == UITableViewCellAccessoryNone) {
        [selectedCell setAccessoryType:UITableViewCellAccessoryCheckmark];
        [selectedUsers addObject:[self userForIndexPath: newIndexPath]];
		
    } 
    else {
        [selectedCell setAccessoryType:UITableViewCellAccessoryNone];
		[selectedUsers removeObject:[self userForIndexPath: newIndexPath]];
		
    }
	
    [tableView deselectRowAtIndexPath:newIndexPath animated:NO];
   	
}


@end
