    //
//  UILoginController.m
//  troupper.0
//
//  Created by Betiana Darderes on 5/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "UILoginController.h"
#import "UIFriendsController.h"
#import "PlaceViewController.h"
#import "HomePlaceDataSource.h"
#import "TroupperAppDelegate.h"
#import "UIUserStatusController.h"
#import "UISignupController.h"
#import "TrouperService.h"
#import "JSON.h"
#import "ServiceResponseReader.h"
#import "Components.h"


@implementation UILoginController

//@synthesize loginTableController;
@synthesize userNameField;
@synthesize passwordField;

NSString * loginString = @"users/login";

NSString * userName;
NSString * pass;


TrouperService* service;

//The user logins
- login{
	
	
	userName = [userNameField text];
	pass = [passwordField text];
    //http://192.168.41.100:8080/uw-ws/uwservices/users/login/bart?password=bart
	NSString * fullLoginString =  [NSString stringWithFormat: @"%@/%@?password=%@",loginString,userName,pass]; 
	
	service = [[TrouperService alloc] initWith: activityIndicator delegate: self];
	[activityIndicator startAnimating];
	[activityIndicator setHidden: NO];
	[service startWithString:fullLoginString];
	
	/*
	TroupperAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	[delegate startTabBarController];
	*/
	
	
}
	
-(BOOL)textFieldShouldReturn:(UITextField *)theTextField {
	[theTextField resignFirstResponder];
	return YES;
}

- (void)didFinishLoadingData:(NSData *)responseData string: (NSString *) connString{	
	
	User* user;
	NSRange textRange;
	textRange =[connString rangeOfString:@"login"];
	
	ServiceResponseReader * reader = [[ServiceResponseReader alloc] initWith:activityIndicator];
	
	if(textRange.location != NSNotFound)
	{
	//Lee el resultado del login	
	
	
		//analiza si hubo error de login
	NSString * responseString = [reader errorLoginFrom: responseData];
	
	if (responseString) {
		
		NSLog(responseString);
		
		if ([responseString isEqualToString: @"NOT FOUND"]) {
			[activityIndicator setHidden:  YES];
			[activityIndicator stopAnimating];
			UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Login Error" message: @"Wrong Username" 
															delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] autorelease];
			[alert show];
		}
        else {
			[activityIndicator setHidden:  YES];
			[activityIndicator stopAnimating];
			UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Login Error" message: @"Wrong Password" 
															delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] autorelease];
			[alert show];
			
		}
		
		
		
		
		
		
	} else {
		//No hubo error y lee el usuario logeado
		user = [reader loggedUserFrom: responseData];
		[user setUserName: userName];
		[user setIsPublished: NO];
		[[Components sharedPlacesInstance] setLoggedUser: user];
		
		//Pide los perfiles del usuario
		NSString * userProfilesString =  [NSString stringWithFormat: @"users/%@/profiles/list", userName];
		[service startWithString: userProfilesString];
		
	}
	}
	// No es el string de login, entonces es el string de los perfiles 
	else {
		
		NSArray * userProfiles = [reader userProfilesFrom: responseData];
					
			int i;
			
			for (i = 0; i < [userProfiles count]; i++) {
				
				User* profile = [userProfiles objectAtIndex:i];
				
				//Pide los perfiles del usuario
				//http://192.168.41.100:8080/uw-ws/uwservices/users/bart/Bart%20Simpson/picture
				NSString * imageProfilesString =  [NSString stringWithFormat: @"users/%@/%@/picture", userName, [profile userName]];
				UIImage *img = [service imageForProfile: imageProfilesString];
				[profile setDownloadedImage: img];
			
					
			}
			
	
		    [[Components sharedPlacesInstance] setProfileUserArray: userProfiles];
		
		     [[Components sharedPlacesInstance] publicUserProfile];
		
		[[Components sharedPlacesInstance] setConnUserArray:[NSMutableArray arrayWithCapacity:3]];
		
		     [activityIndicator stopAnimating];
	 
	 
		     TroupperAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
		     [delegate startTabBarController];
		
		
		
		
		}
		
		
	}


	
		


//The user logins
- signup{
	
	
	UISignupController *signupController = [[UISignupController alloc] initWithNibName:@"signup" bundle:nil];
	
	TroupperAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
	[delegate.iniNavController pushViewController:signupController animated:YES];
	[signupController release];
	
	
	
}



/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	// Create cancel button

	// Create cancel button
	UIBarButtonItem *loginButton = [[UIBarButtonItem alloc] 
								   initWithTitle:@"Log In" 
								   style:UIBarButtonItemStylePlain 
								   target:self 
								   action:@selector(login)];
	
	self.navigationItem.rightBarButtonItem = loginButton;
	[loginButton release];
	
	
	self.navigationItem.hidesBackButton = YES;	
	
	userNameField.delegate = self;
	passwordField.delegate = self;
	
    [super viewDidLoad];
}



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
