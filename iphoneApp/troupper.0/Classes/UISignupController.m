    //
//  UISignupController.m
//  troupper.0
//
//  Created by Betiana Darderes on 6/7/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "UISignupController.h"
#import "TroupperAppDelegate.h"


@implementation UISignupController

@synthesize signupTableController;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

//The user send the form
- (IBAction)join:(id)sender {
	
	
	
		
}
//The user cancels
- (IBAction)cancel:(id)sender {
	
		
	
}

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
		
	// Create cancel button
	UIBarButtonItem *joinButton = [[UIBarButtonItem alloc] 
								   initWithTitle:@"Join" 
								   style:UIBarButtonItemStylePlain 
								   target:self 
								   action:@selector(join:)];
	
	self.navigationItem.rightBarButtonItem = joinButton;
	[joinButton release];
	
	self.navigationItem.hidesBackButton = YES;	
	
	
    [super viewDidLoad];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
